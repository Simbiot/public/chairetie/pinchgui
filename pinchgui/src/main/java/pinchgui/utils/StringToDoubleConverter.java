package pinchgui.utils;

import org.slf4j.LoggerFactory;

import javafx.util.StringConverter;

/**
 * Simple implementation of {@link StringConverter} for double.
 *
 * @author tparis
 *
 */
public class StringToDoubleConverter extends StringConverter<Double> {

	@Override
	public String toString(Double object) {
		if (object != null) {
			return object.toString();
		}
		return "";
	}

	@Override
	public Double fromString(String string) {
		if (string != null) {
			try {
				return Double.parseDouble(string);
			} catch (NumberFormatException e) {
				LoggerFactory.getLogger("StringToDoubleConverter")
						.error("Cannot convert " + string + " to double. Default value 0.0 will be used.");
			}

		}
		return 0.;

	}

}
