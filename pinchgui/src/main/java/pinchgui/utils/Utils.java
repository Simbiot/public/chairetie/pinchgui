package pinchgui.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

/**
 * Utility class with comparison method.
 */
public final class Utils {

	/**
	 * Name used for logging.
	 */
	private static final String CONCEPT_ID = "pinchgui.utils.Utils";

	/**
	 * Java logger to debug tests.
	 */
	private static final Logger ERROR_LOGGER = (Logger) LoggerFactory.getLogger(CONCEPT_ID);

	/**
	 * Avoid constructor.
	 */
	private Utils() {
	}

	/**
	 * Compare two files.
	 *
	 * @param filePath1
	 * @param filePath2
	 * @return False if the file paths are the same, of if the files are not
	 *         identical. True if they are equal line by line.
	 */
	public static boolean compare(String filePath1, String filePath2) {
		boolean ret = false;
		File file1 = new File(filePath1);
		File file2 = new File(filePath2);
		// If they don't exist or they are the same, return false.
		if (!file1.exists()) {
			ERROR_LOGGER.error("Path leads to no file! " + filePath1);
		} else if (!file2.exists()) {
			ERROR_LOGGER.error("Path leads to no file! " + filePath2);
		} else if (file1.compareTo(file2) == 0) {
			ERROR_LOGGER.error("The path are identical! The method must compare two distinct files!\n" + filePath1
					+ "\n" + filePath2);
		} else {
			// Comparison
			try {
				FileReader stream1 = new FileReader(file1);
				FileReader stream2 = new FileReader(file2);
				BufferedReader b1 = new BufferedReader(stream1);
				BufferedReader b2 = new BufferedReader(stream2);
				String line1 = "";
				String line2 = "";
				boolean difference = false;
				// Single "&" because we need line1 and line2, don't stop after 1st part.
				while ((line1 = b1.readLine()) != null & (line2 = b2.readLine()) != null) {
					if (!line1.equals(line2)) {
						ERROR_LOGGER.error("Lines doesn't match!\n" + line1 + "\n" + line2);
						difference = true;
						break;
					}
				}
				// If there is no difference and they are both fully read, then they are
				// similar.
				ret = !difference && (line1 == null && line2 == null);
				stream1.close();
				stream2.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace(); // Shouldn't happen!
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	/**
	 * Extract the resource inside the jar into a temporary file which will be
	 * deleted at the end of the JVM.
	 *
	 * @param relativePath Relative path to the resource, e.g.
	 *                     "language/fr/strings.xml"
	 * @param resourceName Name of the resource, will be used as suffix for the
	 *                     temporary file.
	 * @param suffix       Type of the resource, e.g. ".txt".
	 * @return Path to the temporary file.
	 */
	public static String extractResourcesIntoTemporaryFile(String relativePath, String resourceName, String suffix) {
		ClassLoader cl = Utils.class.getClassLoader();
		try {
			InputStream inputStream = cl.getResourceAsStream(relativePath);
			if (inputStream != null) {

				File tempFile = File.createTempFile(resourceName, suffix);
				tempFile.deleteOnExit();
				final int bufferSize = 1024;
				byte[] buffer = new byte[bufferSize];
				int length;
				OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tempFile));
				while ((length = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, length);
				}
				inputStream.close();
				outputStream.close();
				return tempFile.getAbsolutePath();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ERROR_LOGGER.error("Cannot access resource in JAR! " + relativePath);
		return null;
	}

}
