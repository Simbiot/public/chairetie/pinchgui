package pinchgui.chart;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple to handle JFreechart graphic. Not much options.
 *
 */
public class SimpleGraphXY {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.chart.simplegraphxy";

	/**
	 * The containing frame to display.
	 */
	private final JFrame frame;

	/**
	 * Set of data displayed on the graph.
	 */
	private final XYSeriesCollection dataset;

	/**
	 * The chart on which data are displayed.
	 */
	private final JFreeChart chart;

	/**
	 * Logger to debug code.
	 */
	private Logger logger;

	/**
	 *
	 * @param title Title of the frame
	 * @param xAxis Name of the x axis
	 * @param yAxis Name of the y axis
	 */
	public SimpleGraphXY(String title, String xAxis, String yAxis) {

		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + title);

		// Contains the data to display
		dataset = new XYSeriesCollection();

		// The chart itself
		chart = ChartFactory.createXYLineChart(
				title, // chart title
				xAxis, // x axis label
				yAxis, // y axis label
				dataset, // data
				PlotOrientation.VERTICAL,
				true, // include legend
				true, // tooltips
				false // urls
		);

		// Plot linked to the chart
		XYPlot plot = (XYPlot) chart.getPlot();
		final int fontSize = 16;
		final int defaultWidth = 2;
		plot.setRenderer(configRenderer(fontSize, defaultWidth));

		plot.setBackgroundPaint(Color.white);
		plot.setRangeGridlinePaint(Color.black);
		plot.setDomainGridlinePaint(Color.black);

		// Frame which will contains the chart
		frame = new JFrame(title);
		// Closing the window doesn't stop the program.
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setLocation(0, 0);
		// Panel to place the chart within the frame.
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setLayout(new BorderLayout());
		panel.add(new ChartPanel(chart));
		frame.add(panel);
		frame.pack(); // Resize the frame to the dimension of the content.
		frame.setResizable(true);

	}

	/**
	 * Add a point to the specified serie, update the plot.
	 *
	 * @param serieNumber Name of the curb to create/update with this point.
	 * @param x
	 * @param y
	 */
	public void add(String serieNumber, double x, double y) {
		logger.trace("Add (" + x + ", " + y + ") to " + serieNumber + ").");
		if (dataset.getSeriesIndex(serieNumber) >= 0) {
			dataset.getSeries(serieNumber).add(x, y);
		} else {
			// Auto sort = false.
			dataset.addSeries(new XYSeries(serieNumber, false));
			dataset.getSeries(serieNumber).add(x, y);
		}
	}

	/**
	 * Display the frame
	 */
	public void display() {
		frame.setVisible(true);
	}

	/**
	 * Update the thickness of lines.
	 *
	 * @param serie
	 * @param width
	 */
	public void setWidth(final String serie, float width) {
		int serieIndex = dataset.getSeriesIndex(serie);
		if (serieIndex > -1) {
			((XYPlot) chart.getPlot()).getRenderer().setSeriesStroke(serieIndex, new BasicStroke(width));
		} else {
			logger.error("The serie " + serie + " does not exist!");
		}
	}

	/**
	 * Change the color of a particular serie.
	 *
	 * @param serie
	 * @param newColor
	 */
	public void setColor(final String serie, Color newColor) {
		int serieIndex = dataset.getSeriesIndex(serie);
		if (serieIndex > -1) {
			((XYPlot) chart.getPlot()).getRenderer().setSeriesPaint(serieIndex, newColor);
		} else {
			logger.error("The serie " + serie + " does not exist!");
		}

	}

	private AbstractXYItemRenderer configRenderer(final int fontSize, final float lineWidth) {
		// Renderer step to see changes in values.
		XYLineAndShapeRenderer lineRenderer = new XYLineAndShapeRenderer();
		lineRenderer.setDefaultLegendTextFont(new Font(
				XYLineAndShapeRenderer.DEFAULT_VALUE_LABEL_FONT.getName(),
				XYLineAndShapeRenderer.DEFAULT_VALUE_LABEL_FONT.getStyle(),
				fontSize));
		lineRenderer.setDefaultShapesVisible(false);
		lineRenderer.setDefaultStroke(new BasicStroke(lineWidth));
		lineRenderer.setAutoPopulateSeriesStroke(false);
		return lineRenderer;
	}

}
