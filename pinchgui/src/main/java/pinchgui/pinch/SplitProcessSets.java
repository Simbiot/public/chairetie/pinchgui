package pinchgui.pinch;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents the two subsets when splitting the energy process set of a
 * {@link EnergyTargetingAnalysis}.
 *
 * @author tparis
 *
 */
public final class SplitProcessSets {

	/**
	 * Set above of processes above the temperature.
	 */
	private Set<EnergyProcess> above;

	/**
	 * Set of processes below the temperature.
	 */
	private Set<EnergyProcess> below;

	/**
	 *
	 * @param belowSet
	 * @param aboveSet
	 */
	private SplitProcessSets(Set<EnergyProcess> belowSet, Set<EnergyProcess> aboveSet) {
		above = aboveSet;
		below = belowSet;
	}

	/**
	 *
	 * @return {@link #above}.
	 */
	public Set<EnergyProcess> getAbove() {
		return above;
	}

	/**
	 *
	 * @return {@link #below}.
	 */
	public Set<EnergyProcess> getBelow() {
		return below;
	}

	/**
	 * Split a set of energy processes into two subsets. Only consider processes
	 * that are completely above or below the temperature (no crossing). Method used
	 * on the offset temperature process set from the {@link EnergyTargetingAnalysis}
	 * instance.
	 *
	 * @param pinch The {@link EnergyTargetingAnalysis} instance from which we want the
	 *              subsets.
	 * @return A {@link SplitProcessSets} composed of the process set below the
	 *         pinch point temperature and the process set above. Careful we
	 *         consider offset processes.
	 */
	public static SplitProcessSets splitProcesses(EnergyTargetingAnalysis pinch) {
		Set<EnergyProcess> processSetOffsetTemperatures = pinch.getProcessSetOffsetTemperatures();
		final double temperature = pinch.getPinchPointTemperature();
		Set<EnergyProcess> above = new HashSet<EnergyProcess>();
		Set<EnergyProcess> below = new HashSet<EnergyProcess>();
		for (EnergyProcess process : processSetOffsetTemperatures) {
			if (process.getInletT() <= temperature && process.getOutletT() <= temperature) {
				below.add(process);
			} else if (process.getInletT() >= temperature && process.getOutletT() >= temperature) {
				above.add(process);
			} else {
				if (process.isCold()) {
					// Split the process into its two parts (above and below the temperature
					// pinch). As it is a cold process the outlet temperature is higher.
					EnergyProcess pBelow = process.clone();
					pBelow.setOutletT(temperature);
					below.add(pBelow);

					EnergyProcess pAbove = process.clone();
					pAbove.setInletT(temperature);
					above.add(pAbove);
				} else {
					// Split the process into its two parts (above and below the temperature
					// (pinch). As it is a hot process the outlet temperature is smaller.
					EnergyProcess pAbove = process.clone();
					pAbove.setOutletT(temperature);
					above.add(pAbove);

					EnergyProcess pBelow = process.clone();
					pBelow.setInletT(temperature);
					below.add(pBelow);
				}
			}
		}
		return new SplitProcessSets(below, above);
	}
}
