package pinchgui.pinch;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pinchgui.app_ui.model.EnergyProcessData;

/**
 * Abstract process, we consider its inlet and outlet temperatures (common to
 * each process definition) and getter for the inlet and outlet enthalpies
 * (computation of these enthalpies may differ between processes).
 *
 * @author tparis
 *
 */
public abstract class EnergyProcess implements Serializable, Comparable<EnergyProcess>, Cloneable {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.pinch.process";

	/**
	 * Default serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the process.
	 */
	private String name;

	/**
	 * Fluid name for this process.
	 */
	private String fluidName;

	/**
	 * Temperature of the fluid at the inlet of this process.
	 */
	private double inletT;

	/**
	 * Temperature of the fluid at the outlet of this process.
	 */
	private double outletT;

	/**
	 * Mass flow of the fluid in this process.
	 */
	private double massFlow;

	/**
	 * Logger to debug code.
	 */
	private Logger logger;

	/**
	 * Empty constructor intended to be used with
	 * {@link #importFromCsvLine(String, String)}.
	 *
	 */
	public EnergyProcess() {
		super();
		// Default valid values.
		name = "Unknown";
		fluidName = "Unknown";
		massFlow = Double.MAX_VALUE;
		inletT = Double.MIN_VALUE;
		outletT = Double.MAX_VALUE;

		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + name);
	}

	/**
	 * Process constructor with only name, mass flow, inlet and outlet temperatures.
	 *
	 * @param aName
	 * @param aFluidName
	 * @param mflow
	 * @param inT
	 * @param outT
	 */
	public EnergyProcess(String aName, String aFluidName, double mflow, double inT, double outT) {
		this();
		setName(aName); // Add also logger.
		setFluidName(aFluidName);
		massFlow = mflow;
		inletT = inT;
		outletT = outT;
	}

	/**
	 *
	 * @param offsetTemperature Add this value to the existing inlet and outlet
	 *                          temperature.
	 * @return The same subclass of {@link EnergyProcess}, but add offset to inlet
	 *         and outlet temperatures.
	 */
	public abstract EnergyProcess getOffsetProcess(double offsetTemperature);

	/**
	 *
	 * @return Name of the process.
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @return Name of the fluid for this process.
	 */
	public String getFluidName() {
		return fluidName;
	}

	/**
	 *
	 * @return Mass flow of the fluid in the process, kg/s.
	 */
	public double getMassFlow() {
		return massFlow;
	}

	/**
	 *
	 * @return Inlet temperature.
	 */
	public double getInletT() {
		return inletT;
	}

	/**
	 *
	 * @return Outlet temperature.
	 */
	public double getOutletT() {
		return outletT;
	}

	/**
	 *
	 * @param temperature
	 * @return True if the temperature belongs the interval of temperature of this
	 *         process.
	 */
	public boolean isInInterval(double temperature) {
		if (isHot() && temperature >= outletT && temperature <= inletT) {
			return true;
		} else if (isCold() && temperature >= inletT && temperature <= outletT) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @return logger.
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 *
	 * @param aName {@link #name}.
	 */
	protected void setName(String aName) {
		assert aName != null : "The name of the process mustn't be null!";
		name = aName;
		// Change logger name to be consistent.
		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + name);
	}

	/**
	 *
	 * @param aFluidName {@link #fluidName}.
	 */
	protected void setFluidName(String aFluidName) {
		assert aFluidName != null : "The name of the fluid mustn't be null!";
		fluidName = aFluidName;
	}

	/**
	 *
	 * @param inT {@link #inletT}.
	 */
	protected void setInletT(double inT) {
		assert inT != outletT : "The temperatures at the inlet and at the outlet must be different!";
		inletT = inT;
	}

	/**
	 *
	 * @param outT {@link #outletT}.
	 */
	protected void setOutletT(double outT) {
		assert inletT != outT : "The temperatures at the inlet and at the outlet must be different!";
		outletT = outT;
	}

	/**
	 *
	 * @param mFlow {@link #massFlow}.
	 */
	protected void setMassFlow(double mFlow) {
		assert mFlow > 0 : "The mass flow must be strictly positive!";
		massFlow = mFlow;
	}

	/**
	 *
	 * @return Inlet enthalpy in kJ.
	 */
	public abstract double getInletEnthalpy();

	/**
	 *
	 * @return Outlet enthalpy in kJ.
	 */
	public abstract double getOutletEnthalpy();

	/**
	 * Return the specific enthalpy of the fluid for a given temperature. Assume a
	 * linear relation between the inlet and outlet enthalpies.
	 *
	 * @param temperature
	 * @return Specific enthalpy.
	 */
	public double getH(double temperature) {
		// h = a * temperature + b;
		final double inH = getInletEnthalpy();
		final double outH = getOutletEnthalpy();
		double a = (outH - inH) / (getOutletT() - getInletT());
		double b = inH - (outH - inH) * getInletT() / (getOutletT() - getInletT());
		return a * temperature + b;
	}

	/**
	 *
	 * @return Thermal power for the fluid, difference of energy between inlet and
	 *         outlet, in kW.
	 */
	public double getDeltaQ() {
		return (getInletEnthalpy() - getOutletEnthalpy()) * getMassFlow();
	}

	/**
	 *
	 * @param minT
	 * @param maxT
	 * @return Thermal power for the fluid, absolute difference of energy between
	 *         the two temperatures, in kW. It represents the part of the thermal
	 *         power within these temperatures, the sign depends on the type of
	 *         fluid (cold or hot).
	 */
	public double getDeltaQ(double minT, double maxT) {
		return (getH(Math.max(minT, maxT)) - getH(Math.min(minT, maxT))) * getMassFlow() * (isHot() ? 1 : -1);
	}

	/**
	 * @param temperature Temperature as the specific heat capacity can be not
	 *                    constant.
	 * @return Aggregated value forged from the specific heat capacity and its mass
	 *         flow in the process. May depends on temperature.
	 */
	public abstract double getC(double temperature);

	/**
	 * @return Aggregated value forged from the specific heat capacity and its mass
	 *         flow in the process. If the specific heat capacity depends on
	 *         temperature, the mean value is used.
	 */
	public abstract double getC();

	/**
	 *
	 * @return True if it is a cold process which needs to be heated, false if it is
	 *         a hot process which needs to be cooled down.
	 */
	public boolean isCold() {
		return inletT < outletT;
	}

	/**
	 *
	 * @return False if it is a cold process which needs to be heated, true if it is
	 *         a hot process which needs to be cooled down.
	 */
	public boolean isHot() {
		return inletT > outletT;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof EnergyProcess) {
			EnergyProcess p = (EnergyProcess) obj;
			return inletT == p.getInletT() && outletT == p.getOutletT() && getInletEnthalpy() == p.getInletEnthalpy()
					&& getOutletEnthalpy() == p.getOutletEnthalpy() && massFlow == p.getMassFlow()
					&& name.equals(p.getName()) && fluidName.equals(p.getFluidName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return name.hashCode() + fluidName.hashCode() + Double.hashCode(inletT) + Double.hashCode(outletT)
				+ Double.hashCode(getInletEnthalpy()) + Double.hashCode(getOutletEnthalpy())
				+ Double.hashCode(massFlow);
	}

	@Override
	public int compareTo(EnergyProcess p) {
		return getName().compareTo(p.getName());
	}

	/**
	 * Each subclass will be responsible for implementing this method to export
	 * itself as a CSV line.
	 *
	 * @param csvSeparator The CSV separator to use.
	 * @return A String representing this process as a CSV line.
	 */
	public abstract String exportCsvLine(String csvSeparator);

	/**
	 * Each subclass will be responsible for implementing this method to export
	 * itself as a CSV line.
	 *
	 * @param csvSeparator The CSV separator to use.
	 * @return A String to be used as a title column, it is a description of each
	 *         column.
	 */
	public abstract String exportCsvFirstLine(String csvSeparator);

	/**
	 *
	 * @param csvLine
	 * @param csvSeparator The CSV separator to use.
	 */
	public abstract void importFromCsvLine(String csvLine, String csvSeparator);

	/**
	 *
	 * @return New {@link EnergyProcessData} to be used in the GUI part.
	 */
	public abstract EnergyProcessData getProcessData();

	/**
	 * Override clone method to have deep copy of this object.
	 */
	@Override
	public abstract EnergyProcess clone();

	@Override
	public String toString() {
		final String fluidPart = getFluidName() != null && !getFluidName().equals("") ? " of fluid " + getFluidName()
				: "";
		return "Process " + getName() + fluidPart + ", CP=" + getC() + " kW/K, Inlet temperature " + getInletT()
				+ " °C, outlet temperature " + getOutletT() + " °C.";
	}

}
