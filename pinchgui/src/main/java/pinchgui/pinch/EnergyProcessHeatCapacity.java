package pinchgui.pinch;

import pinchgui.app_ui.model.EnergyProcessData;
import pinchgui.app_ui.model.EnergyProcessHeatCapacityData;

/**
 * Represent a process, i.e. a thermal process which involves a fluid whose
 * temperature changes between the inlet and outlet, but the state and the heat
 * capacity does not change.
 *
 */
public class EnergyProcessHeatCapacity extends EnergyProcess {

	/**
	 * Default serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Specific heat capacity of the fluid, J/Kg/K, assumed constant during the
	 * process here.
	 */
	private double cp;

	/**
	 * Constructor to be used with the {@link #importFromCsvLine(String, String)}
	 * method.
	 */
	protected EnergyProcessHeatCapacity() {
		super();
	}

	/**
	 *
	 * @param aName
	 * @param fluidName
	 * @param fluidCp
	 * @param inletTemp
	 * @param outletTemp
	 * @param aMassFlow
	 */
	public EnergyProcessHeatCapacity(String aName, String fluidName, double fluidCp, double inletTemp,
			double outletTemp,
			double aMassFlow) {
		super(aName, fluidName, aMassFlow, inletTemp, outletTemp);
		setCp(fluidCp);
	}

	@Override
	public double getC() {
		return getMassFlow() * getCp();
	}

	@Override
	public double getC(double temperature) {
		return getMassFlow() * getCp();
	}

	/**
	 *
	 * @return Specific heat capacity of the fluid, constant here.
	 */
	public double getCp() {
		return cp;
	}

	/**
	 * Set the specific heat capacity.
	 *
	 * @param aCp
	 */
	protected void setCp(double aCp) {
		assert aCp > 0 : "The specific heat capacity must be strictly positive!";
		cp = aCp;
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			if (obj instanceof EnergyProcessHeatCapacity) {
				// Just check same type, fluid and mass flow, the rest in checked in Process.
				EnergyProcessHeatCapacity p = (EnergyProcessHeatCapacity) obj;
				return getCp() == p.getCp();
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode() + Double.hashCode(cp);
	}

	@Override
	public double getInletEnthalpy() {
		return getInletT() * getCp();
	}

	@Override
	public double getOutletEnthalpy() {
		return getOutletT() * getCp();
	}

	@Override
	public EnergyProcess getOffsetProcess(double offset) {
		return new EnergyProcessHeatCapacity(getName(), getFluidName(), cp, getInletT() + offset, getOutletT() + offset,
				getMassFlow());

	}

	@Override
	public String exportCsvLine(String csvSeparator) {
		return getName() + csvSeparator + getFluidName() + csvSeparator + getCp() + csvSeparator
				+ getInletT() + csvSeparator + getOutletT() + csvSeparator + getMassFlow();
	}

	@Override
	public String exportCsvFirstLine(String csvSeparator) {
		return "Process id" + csvSeparator + " Fluid id" + csvSeparator + " Specific Heat capacity "
				+ csvSeparator
				+ " inlet temperature (Celsius)" + csvSeparator + " outlet temperature (Celsius)" + csvSeparator
				+ "Mass flow (m/s)";
	}

	@Override
	public void importFromCsvLine(String csvLine, String csvSeparator) {
		final int indexProcessName = 0;
		final int indexFluidName = 1;
		final int indexCp = 2;
		final int indexInletTemperature = 3;
		final int indexOutletTemperature = 4;
		final int indexMassFlow = 5;
		String[] items = csvLine.split(csvSeparator);
		if (items.length > indexMassFlow) {
			setName(items[indexProcessName]);
			setFluidName(items[indexFluidName]);
			setCp(Double.parseDouble(items[indexCp]));
			setInletT(Double.parseDouble(items[indexInletTemperature]));
			setOutletT(Double.parseDouble(items[indexOutletTemperature]));
			setMassFlow(Double.parseDouble(items[indexMassFlow]));
		}
	}

	@Override
	public EnergyProcessData getProcessData() {
		return new EnergyProcessHeatCapacityData(getName(), getCp(),
				getInletT(), getOutletT(), getMassFlow());
	}

	@Override
	public EnergyProcess clone() {
		return new EnergyProcessHeatCapacity(getName(), getFluidName(), getCp(), getInletT(), getOutletT(),
				getMassFlow());
	}

}
