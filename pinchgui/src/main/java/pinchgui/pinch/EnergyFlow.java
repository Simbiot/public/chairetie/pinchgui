package pinchgui.pinch;

/**
 * An energy flow is a point of an energy process identified by the temperature,
 * available power, and total heat capacity.
 *
 * @author tparis
 *
 */
public class EnergyFlow {

	/**
	 * Identification of the flow.
	 */
	private String id;

	/**
	 * Temperature at the beginning if this flow in K or Celsius.
	 */
	private double inletTemperature;

	/**
	 * Temperature at the end if this flow in K or Celsius.
	 */
	private double outletTemperature;

	/**
	 * Heat capacity flow in kW/K.
	 */
	private double heatCapacityFlow;

	// To keep a trace of the parent.
	/**
	 * Owner of this energy flow. Each EnergyFlow has one {@link EnergyProcess}
	 * owner.
	 */
	private EnergyProcess owner;

	/**
	 *
	 * @param myOwner             Origin of this energy flow.
	 * @param anId                Identify this specific flow.
	 * @param anInletTemperature
	 * @param anOutletTemperature
	 * @param aCP                 Heat capacity flow, {@link #heatCapacityFlow}.
	 */
	public EnergyFlow(EnergyProcess myOwner, String anId, double anInletTemperature, double anOutletTemperature,
			double aCP) {
		super();
		id = anId;
		inletTemperature = anInletTemperature;
		outletTemperature = anOutletTemperature;
		heatCapacityFlow = aCP;
		owner = myOwner;
	}

	/**
	 *
	 * @return Id of the energy flow.
	 */
	public String getId() {
		return id;
	}

	/**
	 *
	 * @return {@link #inletTemperature}
	 */
	public double getInletTemperature() {
		return inletTemperature;
	}

	/**
	 *
	 * @return {@link #outletTemperature}
	 */
	public double getOutletTemperature() {
		return outletTemperature;
	}

	/**
	 *
	 * @return Power exchanged during this process.
	 */
	public double getPower() {
		return (inletTemperature - outletTemperature) * heatCapacityFlow;
	}

	/**
	 *
	 * @param inT
	 * @param outT
	 * @return Power exchangeable by his flow between this two temperatures.
	 */
	public double getPower(double inT, double outT) {
		if (!isInInterval(inT) || !isInInterval(outT)) {
			return 0.;
		}
		return (inT - outT) * heatCapacityFlow;
	}

	/**
	 *
	 * @param temperature
	 * @return True if the temperature belongs the interval of temperature of this
	 *         flow.
	 */
	public boolean isInInterval(double temperature) {
		if (getOwner().isHot() && temperature >= outletTemperature && temperature <= inletTemperature) {
			return true;
		} else if (getOwner().isCold() && temperature >= inletTemperature && temperature <= outletTemperature) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @return {@link #owner}
	 */
	public EnergyProcess getOwner() {
		return owner;
	}

	/**
	 *
	 * @return {@link #heatCapacityFlow}
	 */
	public double getC() {
		return heatCapacityFlow;
	}

	/**
	 *
	 * @return The mass flow of this {@link EnergyFlow}. Computed by the ratio
	 *         between the process heat capacity flow and the current heat capacity
	 *         flow.
	 */
	public double getMassFlow() {
		return owner.getMassFlow() * heatCapacityFlow / owner.getC();
	}

	@Override
	public String toString() {
		return "Energy flow " + id + " from " + owner.getName() + " Tin=" + inletTemperature + " Tout="
				+ outletTemperature + " CP=" + heatCapacityFlow + " power=" + getPower() + ".";
	}

}
