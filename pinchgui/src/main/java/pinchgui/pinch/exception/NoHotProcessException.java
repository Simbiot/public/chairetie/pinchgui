package pinchgui.pinch.exception;

import pinchgui.app_ui.internationalisation.I18n;

/**
 * Exception when there is not enough processes to conduct the Pinch analysis.
 *
 */
public class NoHotProcessException extends NotEnoughProcessException {

	/**
	 * Default serial number.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID to get the message of this exception.
	 */
	private static final String ID = "noHotProcess";

	@Override
	public String getId() {
		return ID;
	}

	/**
	 * Generate an exception with the default message.
	 */
	public NoHotProcessException() {
		super(I18n.getInstance().get("exception", ID));
	}

	/**
	 * Generate an exception which starts with the default message.
	 *
	 * @param s A String to add to the original default message.
	 */
	public NoHotProcessException(String s) {
		super(I18n.getInstance().get("exception", ID) + s);
	}
}
