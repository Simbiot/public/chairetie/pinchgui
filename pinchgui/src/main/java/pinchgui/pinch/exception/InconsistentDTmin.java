package pinchgui.pinch.exception;

import pinchgui.app_ui.internationalisation.I18n;

/**
 * Exception raised when we cannot find a good minimal delta of temperature
 * between the hot and cold original composite curbs.
 *
 */
public class InconsistentDTmin extends Exception implements ExceptionId {

	/**
	 * Default serial number.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID to get the message of this exception.
	 */
	private static final String ID = "inconsistentDTmin";

	@Override
	public String getId() {
		return ID;
	}

	/**
	 * Generate an exception with the default message.
	 */
	public InconsistentDTmin() {
		super(I18n.getInstance().get("exception", ID));
	}

	/**
	 * Generate an exception which starts with the default message.
	 *
	 * @param s A String to add to the original default message.
	 */
	public InconsistentDTmin(String s) {
		super(I18n.getInstance().get("exception", ID) + s);
	}

}
