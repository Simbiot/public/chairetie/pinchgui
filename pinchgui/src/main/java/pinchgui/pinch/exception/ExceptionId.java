package pinchgui.pinch.exception;

/**
 * Specify an Id to identify exceptions.
 *
 */
public interface ExceptionId {

	/**
	 * @return ID to get the message of this exception.
	 *
	 */
	String getId();

}
