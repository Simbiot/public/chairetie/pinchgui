package pinchgui.pinch.exception;

import pinchgui.app_ui.internationalisation.I18n;

/**
 * Exception raised when no Pinch point are found.
 *
 */
public class NoPinchPointException extends Exception implements ExceptionId {

	/**
	 * Default serial number.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID to get the message of this exception.
	 */
	private static final String ID = "noPinchPoint";

	@Override
	public String getId() {
		return ID;
	}

	/**
	 * Generate an exception with the default message.
	 */
	public NoPinchPointException() {
		super(I18n.getInstance().get("exception", ID));
	}

	/**
	 * Generate an exception which starts with the default message.
	 *
	 * @param s A String to add to the original default message.
	 */
	public NoPinchPointException(String s) {
		super(I18n.getInstance().get("exception", ID) + s);
	}

}
