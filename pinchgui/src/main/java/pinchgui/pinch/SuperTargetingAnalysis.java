package pinchgui.pinch;

import static pinchgui.pinch.util.EnergyUtilitySet.getColdUtilities;
import static pinchgui.pinch.util.EnergyUtilitySet.getHotUtilities;
import static pinchgui.pinch.util.EnergyUtilitySet.getMaxCoolingCost;
import static pinchgui.pinch.util.EnergyUtilitySet.getMaxHeatingCost;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;
import pinchgui.pinch.util.EnergyProcessSet;

/**
 * Object to conduct the super targeting analysis (methods to compute costs
 * depending on the DTmin).
 *
 * @author tparis
 *
 */
public class SuperTargetingAnalysis implements Serializable {

	/**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.pinch.supertargeting";

	/**
	 * Energy targeting instance which contains the target system.
	 */
	private EnergyTargetingAnalysis energyTargeting;

	/**
	 * Set of utilities.
	 */
	private final Set<EnergyUtility> utilitySet;

	/**
	 * Default interest, 8%.
	 */
	public static final double DEFAULT_INTEREST_OF_CAPITAL = 0.08;

	/**
	 * Capital annual interest (-), percentage.
	 */
	private double capitalAnnualInterest;

	/**
	 * Pay Off Period (year).
	 */
	public static final double DEFAULT_PAY_OFF_PERIOD = 10;

	/**
	 * Pay Off Period (year).
	 */
	private double payOffPeriod;

	/**
	 * Fixed cost of a heat exchanger (euros).
	 */
	private double heatExchangerFixedCost;

	/**
	 * Reference surface of a heat exchanger (square meters) for the cost
	 * {@link #heatExchangerReferenceCost}.
	 */
	private double heatExchangerReferenceSurface;

	/**
	 * Default cost function exponent (-), 0.71.
	 */
	public static final double DEFAULT_COST_FUNCTION_EXPONENT = 0.71;

	/**
	 * Cost function exponent (-).
	 */
	private double costFunctionExponent;

	/**
	 * Number of exploitation hours (h).
	 */
	private double nbAnnualExploitationHours;

	/**
	 * Model factor (euros), variable part of the price.
	 */
	private double modelFactor;

	/**
	 * Default actualization factor (index factor, e.g. given by Marshall & Swift
	 * Equipment Cost Index), 1 means cost doesn't depend on time.
	 */
	public static final double DEFAULT_ACTUALIZATION_FACTOR = 1;

	/**
	 * Actualization factor (index factor, e.g. given by Marshall & Swift Equipment
	 * Cost Index), default 1.
	 */
	private double actualizationFactor;

	/**
	 * Default installation factor, 3.
	 */
	public static final double DEFAULT_INSTALLATION_FACTOR = 3.;

	/**
	 * Factor for transportation and installation, default
	 * {@link #DEFAULT_INSTALLATION_FACTOR}.
	 */
	private double transportationAndInstallationFactor;

	/**
	 * Default value in kW/m²/K, coefficient for the convection heat transfer (used
	 * to estimate heat exchanger surface).
	 */
	public static final double DEFAULT_CONVECTION_COEFFICIENT = 1.;

	/**
	 * Coefficient for the convection heat transfer (used to estimate heat exchanger
	 * surface), default 1.
	 */
	private double convectionHeatTransferCoefficient;

	/**
	 * List of supertargeting results (minimal difference of temperature associated
	 * to costs and energy targeting values).
	 */
	private List<SuperTargetingData> superTargetingResults;

	/**
	 * Whether of not the computation has been done.
	 */
	private boolean hasComputed;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger;

	/**
	 * Instance of utility object which contains the values to compute costs.
	 *
	 * @param pinch                Energy targeting instance.
	 * @param heFixedCost          {@link #heatExchangerFixedCost}.
	 * @param heRefSurface         {@link #heatExchangerReferenceSurface}.
	 * @param aModelFactor         {@link #modelFactor}.
	 * @param nbAnnualExploitHours {@link #nbAnnualExploitationHours}.
	 */
	public SuperTargetingAnalysis(EnergyTargetingAnalysis pinch,
			double heFixedCost, double heRefSurface, double aModelFactor,
			double nbAnnualExploitHours) {
		energyTargeting = pinch;
		heatExchangerFixedCost = heFixedCost;
		heatExchangerReferenceSurface = heRefSurface;
		modelFactor = aModelFactor;
		nbAnnualExploitationHours = nbAnnualExploitHours;
		utilitySet = new HashSet<EnergyUtility>();
		superTargetingResults = new ArrayList<SuperTargetingData>();

		capitalAnnualInterest = DEFAULT_INTEREST_OF_CAPITAL;
		payOffPeriod = DEFAULT_PAY_OFF_PERIOD;
		costFunctionExponent = DEFAULT_COST_FUNCTION_EXPONENT;
		actualizationFactor = DEFAULT_ACTUALIZATION_FACTOR;
		transportationAndInstallationFactor = DEFAULT_INSTALLATION_FACTOR;
		convectionHeatTransferCoefficient = DEFAULT_CONVECTION_COEFFICIENT;
		hasComputed = false;
		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + energyTargeting.getName());
	}

	/**
	 * An energy utility (hot or cold) to be used to balance the heat cascade.
	 *
	 * @param utility
	 */
	public void addUtility(EnergyUtility utility) {
		utilitySet.add(utility);
	}

	/**
	 * Utilities (hot or cold) to be used to balance the heat cascade.
	 *
	 * @param utilities
	 */
	public void addUtility(Set<EnergyUtility> utilities) {
		utilitySet.addAll(utilities);
	}

	/**
	 *
	 * @return Annuity factor (formula (i*(1+i)^n)/((1+i)^n - 1) ). With i is
	 *         {@link #capitalAnnualInterest}, and n is {@link #payOffPeriod}.
	 *         Formula is from formula 6.2 of OFEN manual.
	 */
	public double getAnnuityFactor() {
		return (capitalAnnualInterest * Math.pow(1 + capitalAnnualInterest, payOffPeriod))
				/ (Math.pow(1 + capitalAnnualInterest, payOffPeriod) - 1);
	}

	/**
	 *
	 * @return Formula 6.4 of OFEN manual.
	 */
	public double getHeatExchangerReferenceInvestmentCost() {
		return modelFactor * actualizationFactor * transportationAndInstallationFactor;
	}

	/**
	 * Estimated cost of the heat exchanger network depending of the estimated
	 * minimal number of heat exchangers and the estimated heat exchanger surface.
	 * Formula 6.5 on OFEN manual.
	 *
	 * @param targetMinimalNumberOfHeatExchanger Estimated minimal number of heat
	 *                                           exchanger requested.
	 * @param targetHeatExchangerSurface         Estimated requested surface of all
	 *                                           heat exchangers.
	 * @return Estimated cost of the heat exchanger network (euros).
	 */
	public double getHeatExchangerNetworkEstimatedInvestmentCost(double targetMinimalNumberOfHeatExchanger,
			double targetHeatExchangerSurface) {
		return targetMinimalNumberOfHeatExchanger
				* (heatExchangerFixedCost + getHeatExchangerReferenceInvestmentCost() * Math.pow(
						(targetHeatExchangerSurface)
								/ (heatExchangerReferenceSurface * targetMinimalNumberOfHeatExchanger),
						costFunctionExponent));
	}

	/**
	 *
	 * @param targetMinimalHeatingEnergy Targeted minimal energy used to heat.
	 * @param costHotUtility             Cost (per kW) of the hot utility.
	 * @param targetMinimalCoolingEnergy Targeted minimal energy used to cool down.
	 * @param costColdUtility            Cost (per kW) of the hot utility.
	 * @return Annual operation cost (euros/year).
	 */
	public double getEstimatedAnnualOperationCost(double targetMinimalHeatingEnergy, double costHotUtility,
			double targetMinimalCoolingEnergy, double costColdUtility) {
		return nbAnnualExploitationHours
				* (targetMinimalHeatingEnergy * costHotUtility + targetMinimalCoolingEnergy * costColdUtility);
	}

	/**
	 * Formula 6.1 of the OFEN manual (annuity factor * investment cost +
	 * operational cost).
	 *
	 * @param heatExchangerEstimatedInvestmentCost
	 * @param annualOperationalCost
	 * @return Total estimated annual costs for the heat exchanger network
	 *         (euros/year).
	 */
	public double getEstimatedAnnualTotalCost(double heatExchangerEstimatedInvestmentCost,
			double annualOperationalCost) {
		return getAnnuityFactor() * heatExchangerEstimatedInvestmentCost + annualOperationalCost;
	}

	/**
	 *
	 * @return The annual operating costs if no heat exchange is performed.
	 */
	public double getWorstAnnnualOperatingCosts() {
		double totalEnergyRequiredToHeatWithoutExchange = EnergyProcessSet
				.sumColdProcessRequiredEnergy(energyTargeting.getProcessSet());
		double totalEnergyRequiredToCoolDownWithoutExchange = EnergyProcessSet
				.sumHotProcessRequiredEnergy(energyTargeting.getProcessSet());
		double worseAnnualOperatingCost = nbAnnualExploitationHours
				* (totalEnergyRequiredToHeatWithoutExchange * getMaxHeatingCost(utilitySet)
						+ totalEnergyRequiredToCoolDownWithoutExchange * getMaxCoolingCost(utilitySet));
		return worseAnnualOperatingCost;
	}

	/**
	 *
	 * @param investmentCost
	 * @param annualOperatingCost
	 * @param worseAnnualOperatingCost
	 * @return Pay back time obtained by dividing the investment cost, by the
	 *         monthly economy performed on operating costs (difference between the
	 *         estimated operating cost and the worse operating cost).
	 */
	public double getEstimatedPayBackTime(final double investmentCost,
			final double annualOperatingCost, final double worseAnnualOperatingCost) {
		final double nbMonthPerYear = 12;
		double annualOperatingEconomy = worseAnnualOperatingCost - annualOperatingCost;
		double monthlyOperatingEconomy = annualOperatingEconomy / nbMonthPerYear;
		double payBackTime = investmentCost / monthlyOperatingEconomy;
		return payBackTime;
	}

	/**
	 * Test the evolution of several values depending on the minimal difference of
	 * temperature. Compute {@link #superTargetingResults}.
	 *
	 * @param minDtMin
	 * @param step
	 * @param maxDtMin
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public void compute(double minDtMin, double step, double maxDtMin)
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		if (!checkMinSetMaxDtmin(minDtMin, step, maxDtMin)) {
			getLogger().error(
					"Wrong parameters for the supertargeting analysis "
							+ "(mininal delta of temperature and step must be strictly positive, "
							+ "the maximal must be greater than the minimal)!");
			return;
		}
		if (!checkEnergyTargeting(energyTargeting)) {
			getLogger().error("No energy targeting analysis run yet!");
			return;
		}
		superTargetingResults.clear();
		for (double dtmin = minDtMin; dtmin <= maxDtMin; dtmin += step) {
			EnergyTargetingAnalysis newEnergyTargeting = new EnergyTargetingAnalysis(
					energyTargeting.getName() + "DTmin" + dtmin);
			newEnergyTargeting.addProcess(energyTargeting.getProcessSet());
			newEnergyTargeting.setChosenDeltaTmin(dtmin);
			newEnergyTargeting.initialize();
			if (!checkUtilities(newEnergyTargeting, utilitySet)) {
				getLogger().error(
						"Wrong utility set, the supertargeting cannot be set "
								+ "(either cold utility not cold enough "
								+ "or hot utility not hot enough)!");
			}
			final int aEstimatedNumberHe = estimateMinimalNumberOfHeatExchangers(newEnergyTargeting);
			final double aEstimatedTotalSurfaceHen = estimateTotalHeatExchangerSurface(
					newEnergyTargeting,
					utilitySet,
					convectionHeatTransferCoefficient);
			final double aHenInvestmentCost = getHeatExchangerNetworkEstimatedInvestmentCost(aEstimatedNumberHe,
					aEstimatedTotalSurfaceHen);
			final double aHenAnnualOperationCost = getEstimatedAnnualOperationCost(
					newEnergyTargeting.getMinimalHeatingEnergyRequested(), getMaxHeatingCost(utilitySet),
					newEnergyTargeting.getMinimalCoolingEnergyRequested(), getMaxCoolingCost(utilitySet));
			final double aHenAnnualTotalCost = getEstimatedAnnualTotalCost(aHenInvestmentCost,
					aHenAnnualOperationCost);
			final double worseAnnualOperatingCost = getWorstAnnnualOperatingCosts();
			final double estimatedPayBackTime = getEstimatedPayBackTime(aHenInvestmentCost, aHenAnnualOperationCost,
					worseAnnualOperatingCost);
			superTargetingResults
					.add(new SuperTargetingData(
							dtmin, newEnergyTargeting.getMinimalHeatingEnergyRequested(),
							newEnergyTargeting.getMinimalCoolingEnergyRequested(),
							aEstimatedNumberHe, aEstimatedTotalSurfaceHen,
							aHenInvestmentCost, aHenAnnualOperationCost, aHenAnnualTotalCost,
							worseAnnualOperatingCost, estimatedPayBackTime));
		}
		for (SuperTargetingData data : superTargetingResults) {
			getLogger().info(data.toString());
		}
		hasComputed = true;
	}

	/**
	 *
	 * @param minDtMin
	 * @param step
	 * @param maxDtMin
	 * @return True if a for loop can be set up with these doubles.
	 */
	public boolean checkMinSetMaxDtmin(double minDtMin, double step, double maxDtMin) {
		return minDtMin > 0 && step > 0 && maxDtMin > minDtMin;
	}

	/**
	 * @param anEnergyTargeting {@link EnergyTargetingAnalysis} to check.
	 *
	 * @return Check if the {@link #EnergyTargetingAnalysis} is not null and is
	 *         initialized (if just not initialized, try to run it).
	 */
	public static boolean checkEnergyTargeting(EnergyTargetingAnalysis anEnergyTargeting) {
		if (anEnergyTargeting != null) {
			if (anEnergyTargeting.isInitialized()) {
				return true;
			} else {
				try {
					anEnergyTargeting.initialize();
					return true;
				} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
					e.printStackTrace();
				}
			}

		}
		return false;
	}

	/**
	 * Check that there are at least one cold and one hot utilities and that these
	 * utilities can be used to cool down a heat the energy system (minimal and
	 * maximal temperature are consistent).
	 *
	 * @param anEnergyTargeting {@link EnergyTargetingAnalysis} to check.
	 * @param utilities         Set of {@link EnergyUtility} to consider.
	 *
	 * @return True if so, false if not (in this case the computation of costs will
	 *         be inconsistent).
	 */
	public static boolean checkUtilities(EnergyTargetingAnalysis anEnergyTargeting, Set<EnergyUtility> utilities) {
		return checkColdUtilities(anEnergyTargeting, utilities) && checkHotUtilities(anEnergyTargeting, utilities);
	}

	/**
	 *
	 * @param anEnergyTargeting {@link EnergyTargetingAnalysis} to check.
	 * @param utilities         Set of {@link EnergyUtility} to consider.
	 *
	 * @return True if at least one utility enables to heat all cold processes.
	 */
	public static boolean checkColdUtilities(EnergyTargetingAnalysis anEnergyTargeting, Set<EnergyUtility> utilities) {
		boolean coldCheck = false;
		final double minTemperatureToReach = EnergyProcessSet
				.minimalHotProcessTemperature(anEnergyTargeting.getProcessSet()) - anEnergyTargeting.getDeltaTmin() / 2;
		for (EnergyUtility utility : utilities) {
			if (utility.isCold() && utility.getOutletT() < minTemperatureToReach) {
				coldCheck = true;
			}
		}
		return coldCheck;
	}

	/**
	 *
	 * @param anEnergyTargeting {@link EnergyTargetingAnalysis} to check.
	 * @param utilities         Set of {@link EnergyUtility} to consider.
	 *
	 * @return True if at least one utility enables to cool down all hot processes.
	 */
	public static boolean checkHotUtilities(EnergyTargetingAnalysis anEnergyTargeting, Set<EnergyUtility> utilities) {
		final double maxTemperatureToReach = EnergyProcessSet
				.maximalColdProcessTemperature(anEnergyTargeting.getProcessSet())
				+ anEnergyTargeting.getDeltaTmin() / 2;
		boolean hotCheck = false;
		for (EnergyUtility utility : utilities) {
			if (utility.isHot() && utility.getOutletT() > maxTemperatureToReach) {
				hotCheck = true;
			}
		}
		return hotCheck;
	}

	/**
	 * @param aEnergyTargeting {@link EnergyTargetingAnalysis} object to use.
	 *
	 * @return The minimal number of heat exchangers for this set of processes. Use
	 *         formula (N = Nabove - 1 + Nbelow -1) and take into account utilities.
	 */
	public static int estimateMinimalNumberOfHeatExchangers(EnergyTargetingAnalysis aEnergyTargeting) {
		if (!aEnergyTargeting.isInitialized()) {
			LoggerFactory.getLogger(CONCEPT_ID)
					.error("Error computing the minimal number of heat exchangers, is the pinch analysis initialized?");
		} else {
			final SplitProcessSets l = SplitProcessSets.splitProcesses(aEnergyTargeting);
			final int nbColdUtilities = 1;
			final int nbHotUtilities = 1;
			/*
			 * Number of fluids/utilities below the pinch point less one, more number of
			 * fluids/utilities above the pinch points less one.
			 */
			return l.getBelow().size() + nbColdUtilities - 1 + l.getAbove().size() + nbHotUtilities - 1;
		}
		return 0;
	}

	/**
	 *
	 * @return The minimal number of heat exchangers for this set of processes. Use
	 *         formula (N = Nabove - 1 + Nbelow -1) and take into account utilities.
	 */
	public int estimateMinimalNumberOfHeatExchangers() {
		return estimateMinimalNumberOfHeatExchangers(energyTargeting);
	}

	/**
	 * @param processSet
	 *
	 * @return The minimal number of internal heat exchangers for this set of
	 *         processes. Use formula (N = S -1) where S is the number of processes.
	 */
	public static int estimateMinimalNumberOfInternalHeatExchangers(Set<EnergyProcess> processSet) {
		return processSet.size() - 1;
	}

	/**
	 *
	 * @return The minimal number of internal heat exchangers for this set of
	 *         processes. Use formula (N = S -1) where S is the number of processes.
	 */
	public int estimateMinimalNumberOfInternalHeatExchangers() {
		return energyTargeting.getProcessSet().size() - 1;
	}

	/**
	 * @param aEnergyTargeting        {@link EnergyTargetingAnalysis} object to use.
	 *
	 * @param utilitySet              Set of energy utilities.
	 *
	 * @param convectionTransferCoeff The convection transfer coefficient,
	 *                                considered the same for all fluid (e.g. 3
	 *                                kW/(m2.K) for liquid water).
	 * @return Estimation of the heat exchanger surface.
	 */
	public static double estimateTotalHeatExchangerSurface(EnergyTargetingAnalysis aEnergyTargeting,
			Set<EnergyUtility> utilitySet,
			double convectionTransferCoeff) {
		NavigableMap<Double, Double> mapHot = aEnergyTargeting.getEnthalpyFlowIntervalsLinkedToTemperaturesHot();
		NavigableMap<Double, Double> mapCold = aEnergyTargeting.getEnthalpyFlowIntervalsLinkedToTemperaturesCold();
		List<Double> enthalpyFlows = aEnergyTargeting.getEnthalpyFlowValues();
		double estimatedArea = 0.;
		LoggerFactory.getLogger(CONCEPT_ID).info("Start estimation of total heat exchanger area.");
		// Cold utility part
		for (int i = 0; i < enthalpyFlows.size() - 1; i++) {
			// Check this is an interval where hot and cold composite indicated a heat
			// exchange is feasible.
			double theQdot = enthalpyFlows.get(i + 1) - enthalpyFlows.get(i);
			if (mapHot.containsKey(enthalpyFlows.get(i)) && mapCold.containsKey(enthalpyFlows.get(i))
					&& mapHot.containsKey(enthalpyFlows.get(i + 1)) && mapCold.containsKey(enthalpyFlows.get(i + 1))) {
				final double hotOutletT = mapHot.get(enthalpyFlows.get(i));
				final double hotInletT = mapHot.get(enthalpyFlows.get(i + 1));
				final double coldOutletT = mapCold.get(enthalpyFlows.get(i + 1));
				final double coldInletT = mapCold.get(enthalpyFlows.get(i));
				// Compute the contribution of all process on this interval.

				final double dTx = hotOutletT - coldInletT;
				final double dTy = hotInletT - coldOutletT;
				final double meanLogDeltaT = dTx != dTy ? (dTx - dTy) / Math.log(dTx / dTy)
						: dTx;
				double partialArea = theQdot / convectionTransferCoeff / meanLogDeltaT;
				estimatedArea += partialArea;
				LoggerFactory.getLogger(CONCEPT_ID)
						.info("Interval [" + enthalpyFlows.get(i) + ", " + enthalpyFlows.get(i + 1) + "] "
								+ "Mean logarithmic delta of temperature=" + meanLogDeltaT + ", "
								+ "Whole absolute power contribution " + theQdot + " kW"
								+ " partial area " + partialArea + " m2.");
			} else if (mapHot.containsKey(enthalpyFlows.get(i)) && mapHot.containsKey(enthalpyFlows.get(i + 1))) {
				// Case we use the hot utility
				final double hotOutletT = mapHot.get(enthalpyFlows.get(i));
				final double hotInletT = mapHot.get(enthalpyFlows.get(i + 1));
				Set<EnergyUtility> coldUtilities = getColdUtilities(utilitySet);
				EnergyUtility coldUtility = null;
				double coldOutletT = Double.MIN_VALUE;
				// Get the closest utility to the hot outlet stream (cooling at the highest
				// temperature feasible).
				for (EnergyUtility utility : coldUtilities) {
					// Check condition DT >= DTmin
					if (utility.getOutletT() <= hotInletT - aEnergyTargeting.getDeltaTmin()
							&& utility.getInletT() <= hotOutletT - aEnergyTargeting.getDeltaTmin()) {
						if (coldOutletT < utility.getOutletT()) {
							coldUtility = utility;
							coldOutletT = utility.getOutletT();
						}
					}
				}
				if (coldUtility != null) {
					final double dTx = hotOutletT - coldUtility.getInletT();
					final double dTy = hotInletT - coldOutletT;
					final double meanLogDeltaT = dTx != dTy ? (dTx - dTy) / Math.log(dTx / dTy)
							: dTx;
					double partialArea = theQdot / convectionTransferCoeff / meanLogDeltaT;
					estimatedArea += partialArea;
					LoggerFactory.getLogger(CONCEPT_ID)
							.info("Interval [" + enthalpyFlows.get(i) + ", " + enthalpyFlows.get(i + 1) + "] "
									+ "Mean logarithmic delta of temperature=" + meanLogDeltaT + ", "
									+ " power exchange with cold utility " + theQdot + " kW"
									+ " partial area " + partialArea + " m2.");
				} else {
					LoggerFactory.getLogger(CONCEPT_ID)
							.info("Interval [" + enthalpyFlows.get(i) + ", " + enthalpyFlows.get(i + 1) + "] "
									+ "does not consider any cold utility.");
				}

			} else if (mapCold.containsKey(enthalpyFlows.get(i)) && mapCold.containsKey(enthalpyFlows.get(i + 1))) {
				// Case we use the cold utility

				final double coldOutletT = mapCold.get(enthalpyFlows.get(i + 1));
				final double coldInletT = mapCold.get(enthalpyFlows.get(i));
				Set<EnergyUtility> hotUtilities = getHotUtilities(utilitySet);
				EnergyUtility hotUtility = null;
				double hotOutletT = Double.MAX_VALUE;
				// Get the closest utility to the cold outlet stream (cooling at the highest
				// temperature feasible).
				for (EnergyUtility utility : hotUtilities) {
					// Check condition DT >= DTmin
					if (utility.getOutletT() >= coldInletT + aEnergyTargeting.getDeltaTmin()
							&& utility.getInletT() >= coldOutletT + aEnergyTargeting.getDeltaTmin()) {
						if (hotOutletT > utility.getOutletT()) {
							hotUtility = utility;
							hotOutletT = hotUtility.getOutletT();
						}
					}
				}
				if (hotUtility != null) {
					final double dTx = hotOutletT - coldInletT;
					final double dTy = hotUtility.getInletT() - coldOutletT;
					final double meanLogDeltaT = dTx != dTy ? (dTx - dTy) / Math.log(dTx / dTy)
							: dTx;
					double partialArea = theQdot / convectionTransferCoeff / meanLogDeltaT;
					estimatedArea += partialArea;
					LoggerFactory.getLogger(CONCEPT_ID)
							.info("Interval [" + enthalpyFlows.get(i) + ", " + enthalpyFlows.get(i + 1) + "] "
									+ "Mean logarithmic delta of temperature=" + meanLogDeltaT + ", "
									+ " power exchange with hot utility " + theQdot + " kW"
									+ " partial area " + partialArea + " m2.");
				} else {
					LoggerFactory.getLogger(CONCEPT_ID)
							.info("Interval [" + enthalpyFlows.get(i) + ", " + enthalpyFlows.get(i + 1) + "] "
									+ "does not consider any hot utility.");
				}

			}

		}
		LoggerFactory.getLogger(CONCEPT_ID)
				.info("End estimation of total heat exchanger area. Get " + estimatedArea + " m2.");
		return estimatedArea;
	}

	/**
	 * @param convectionTransferCoeff The convection transfer coefficient,
	 *                                considered the same for all fluid (e.g. 3
	 *                                kW/(m2.K) for liquid water).
	 * @return Estimation of the heat exchanger surface.
	 */
	public double estimateTotalHeatExchangerSurface(double convectionTransferCoeff) {
		return estimateTotalHeatExchangerSurface(energyTargeting, utilitySet, convectionTransferCoeff);
	}

	/**
	 * @return the {@link #energyTargeting}
	 */
	public EnergyTargetingAnalysis getEnergyTargeting() {
		return energyTargeting;
	}

	/**
	 * @return the {@link #capitalAnnualInterest}
	 */
	public double getCapitalAnnualInterest() {
		return capitalAnnualInterest;
	}

	/**
	 * @return the {@link #payOffPeriod}
	 */
	public double getPayOffPeriod() {
		return payOffPeriod;
	}

	/**
	 * @return the {@link #heatExchangerFixedCost}
	 */
	public double getHeatExchangerFixedCost() {
		return heatExchangerFixedCost;
	}

	/**
	 * @return the {@link #heatExchangerReferenceSurface}
	 */
	public double getHeatExchangerReferenceSurface() {
		return heatExchangerReferenceSurface;
	}

	/**
	 * @return the {@link #costFunctionExponent}
	 */
	public double getCostFunctionExponent() {
		return costFunctionExponent;
	}

	/**
	 * @return the {@link #nbAnnualExploitationHours}
	 */
	public double getNbAnnualExploitationHours() {
		return nbAnnualExploitationHours;
	}

	/**
	 * @return the {@link #modelFactor}
	 */
	public double getModelFactor() {
		return modelFactor;
	}

	/**
	 * @return the {@link #actualizationFactor}
	 */
	public double getActualizationFactor() {
		return actualizationFactor;
	}

	/**
	 * @return the {@link #transportationAndInstallationFactor}
	 */
	public double getTransportationAndInstallationFactor() {
		return transportationAndInstallationFactor;
	}

	/**
	 * @return the {@link #convectionHeatTransferCoefficient}
	 */
	public double getConvectionHeatTransferCoefficient() {
		return convectionHeatTransferCoefficient;
	}

	/**
	 * @param newEnergyTargeting the {@link #energyTargeting} to set
	 */
	public void setEnergyTargeting(EnergyTargetingAnalysis newEnergyTargeting) {
		energyTargeting = newEnergyTargeting;
	}

	/**
	 * @param newCapitalAnnualInterest the {@link #capitalAnnualInterest} to set
	 */
	public void setCapitalAnnualInterest(double newCapitalAnnualInterest) {
		capitalAnnualInterest = newCapitalAnnualInterest;
	}

	/**
	 * @param newPayOffPeriod the {@link #payOffPeriod} to set
	 */
	public void setPayOffPeriod(double newPayOffPeriod) {
		payOffPeriod = newPayOffPeriod;
	}

	/**
	 * @param newHeatExchangerFixedCost the {@link #heatExchangerFixedCost} to set
	 */
	public void setHeatExchangerFixedCost(double newHeatExchangerFixedCost) {
		heatExchangerFixedCost = newHeatExchangerFixedCost;
	}

	/**
	 * @param newHeatExchangerReferenceSurface the
	 *                                         {@link #heatExchangerReferenceSurface}
	 *                                         to set
	 */
	public void setHeatExchangerReferenceSurface(double newHeatExchangerReferenceSurface) {
		heatExchangerReferenceSurface = newHeatExchangerReferenceSurface;
	}

	/**
	 * @param newCostFunctionExponent the {@link #costFunctionExponent} to set
	 */
	public void setCostFunctionExponent(double newCostFunctionExponent) {
		costFunctionExponent = newCostFunctionExponent;
	}

	/**
	 * @param newNbAnnualExploitationHours the {@link #nbAnnualExploitationHours} to
	 *                                     set
	 */
	public void setNbAnnualExploitationHours(double newNbAnnualExploitationHours) {
		nbAnnualExploitationHours = newNbAnnualExploitationHours;
	}

	/**
	 * @param newModelFactor the {@link #modelFactor} to set
	 */
	public void setModelFactor(double newModelFactor) {
		modelFactor = newModelFactor;
	}

	/**
	 * @param newActualizationFactor the {@link #actualizationFactor} to set
	 */
	public void setActualizationFactor(double newActualizationFactor) {
		actualizationFactor = newActualizationFactor;
	}

	/**
	 * @param newTransportationAndInstallationFactor the
	 *                                               {@link #transportationAndInstallationFactor}
	 *                                               to set
	 */
	public void setTransportationAndInstallationFactor(double newTransportationAndInstallationFactor) {
		this.transportationAndInstallationFactor = newTransportationAndInstallationFactor;
	}

	/**
	 * @param newConvectionHeatTransferCoefficient the
	 *                                             {@link #convectionHeatTransferCoefficient}
	 *                                             to set
	 */
	public void setConvectionHeatTransferCoefficient(double newConvectionHeatTransferCoefficient) {
		this.convectionHeatTransferCoefficient = newConvectionHeatTransferCoefficient;
	}

	/**
	 *
	 * @return True if the analysis has been done and results are accessible.
	 */
	public boolean hasComputed() {
		return hasComputed;
	}

	/**
	 *
	 * @return {@link #superTargetingResults}.
	 */
	public List<SuperTargetingData> getSuperTargetingResults() {
		return superTargetingResults;
	}

	/**
	 *
	 * @return {@link #logger}.
	 */
	private Logger getLogger() {
		return logger;
	}

}
