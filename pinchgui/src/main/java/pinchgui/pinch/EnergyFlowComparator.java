package pinchgui.pinch;

import java.util.Comparator;

/**
 * Compare primarily from the distance from a reference.
 *
 * @author tparis
 *
 */
public abstract class EnergyFlowComparator implements Comparator<EnergyFlow> {

	/**
	 * Reference temperature, pinch temperature.
	 */
	private final double reference;

	/**
	 *
	 * @param pinchT Temperature at pinch point.
	 */
	public EnergyFlowComparator(double pinchT) {
		reference = pinchT;
	}

	/**
	 *
	 * @return The reference to compare the temperature.
	 */
	public double getReference() {
		return reference;
	}

}
