package pinchgui.pinch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoColdProcessException;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoHotProcessException;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Represent a Pinch analysis for a given list of {@link EnergyProcess}.
 *
 */
public class EnergyTargetingAnalysis implements Serializable {

	/**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.pinch.energytargeting";

	/**
	 * Identifier of the analysis.
	 */
	private final String name;

	/**
	 * Set of the {@link EnergyProcess} to consider.
	 */
	private final Set<EnergyProcess> processSet;

	/**
	 * Set of the cold {@link EnergyProcess} to consider. The ones which need to be
	 * heated.
	 */
	private final Set<EnergyProcess> coldProcessSet;

	/**
	 * Set of the {@link EnergyProcess} to consider. The ones which need to be cool
	 * down.
	 */
	private final Set<EnergyProcess> hotProcessSet;

	/**
	 * Minimal absolute temperature difference between hot fluids and cold fluids.
	 */
	private Double deltaTmin;

	/**
	 * Value to test the algorithm on a chosen value of minimal absolute temperature
	 * difference between hot fluids and cold fluids
	 */
	private Double chosenDeltaTmin;

	/**
	 * Set of the {@link EnergyProcess} to consider.
	 */
	private Set<EnergyProcess> processSetOffsetTemperatures;

	/**
	 * Map of temperatures of fluids in processes, sorted in an increasing order. We
	 * keep the link to their process.
	 */
	private List<Entry<Double[], List<EnergyProcess>>> sortedOffsetIntervalsToProcess;

	/**
	 * Data to draw the great composite curb, minimal energy is added.
	 */
	private final NavigableMap<Double, Double> heatCascade;

	/**
	 * Data to draw the cold composite curb.
	 */
	private NavigableMap<Double, Double> coldCompositeCurb;

	/**
	 * Data for the heat cascade without addition of energy, map (temperature,
	 * energy).
	 */
	private NavigableMap<Double, Double> hotCompositeCurb;

	/**
	 * Data to draw the cold composite curb.
	 */
	private NavigableMap<Double, Double> originalColdCompositeCurb;

	/**
	 * Data for the heat cascade without addition of energy, map (temperature,
	 * energy).
	 */
	private NavigableMap<Double, Double> originalHotCompositeCurb;

	/**
	 * Minimal energy to put in the system (energy to add to have 0 in the heat
	 * cascade at the pinch point).
	 */
	private double minimalEnergy;

	/**
	 * Temperature of the pinch point.
	 */
	private Double pinchPointTemperature;

	/**
	 * Minimal energy requested to heat the cold fluids.
	 */
	private double minimalHeatingEnergyRequested;

	/**
	 * Minimal energy requested to cool down the hot fluids.
	 */
	private double minimalCoolingEnergyRequested;

	/**
	 * Minimal energy requested to cool down the hot fluids.
	 */
	private double energyAddedHotComposite;

	/**
	 * Separator used when exporting or importing CSV.
	 */
	private String csvSeparator = ";";

	/**
	 * Accuracy when comparing double.
	 */
	public static final double EPSILON = 1e-3;

	/**
	 * Just indicate that the algorithm has already run or not.
	 */
	private boolean isInitialized;

	/**
	 * Max number of loop for the algorithm that looks for the Delta T min.
	 */
	private int tryLimit;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger;

	/**
	 *
	 * @param aName Identifier of the analysis.
	 */
	public EnergyTargetingAnalysis(String aName) {
		name = aName;
		processSet = new HashSet<EnergyProcess>();
		processSetOffsetTemperatures = new HashSet<EnergyProcess>();
		coldProcessSet = new HashSet<EnergyProcess>();
		hotProcessSet = new HashSet<EnergyProcess>();
		sortedOffsetIntervalsToProcess = new ArrayList<Entry<Double[], List<EnergyProcess>>>();
		heatCascade = new TreeMap<Double, Double>();
		coldCompositeCurb = new TreeMap<Double, Double>();
		hotCompositeCurb = new TreeMap<Double, Double>();
		originalColdCompositeCurb = new TreeMap<Double, Double>();
		originalHotCompositeCurb = new TreeMap<Double, Double>();
		isInitialized = false;
		final int defaultTryLimit = 10;
		tryLimit = defaultTryLimit;
		energyAddedHotComposite = 0.;

		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + name);
	}

	/**
	 * Add a {@link EnergyProcess} to the process set (or replace it if already
	 * present) to consider for the analysis.
	 *
	 * @param aProcess {@link EnergyProcess}
	 */
	public void addProcess(EnergyProcess aProcess) {
		processSet.add(aProcess);
	}

	/**
	 * Add all {@link EnergyProcess} to the process set (or replace it if already
	 * present) to consider for the analysis.
	 *
	 * @param aProcessSet {@link EnergyProcess}
	 */
	public void addProcess(Set<EnergyProcess> aProcessSet) {
		processSet.addAll(aProcessSet);
	}

	/**
	 *
	 * @return Identifier of the analysis.
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @return A copy of the set of {@link EnergyProcess} to consider for the
	 *         analysis.
	 */
	public Set<EnergyProcess> getProcessSet() {
		return new HashSet<EnergyProcess>(processSet);
	}

	/**
	 *
	 * @return A copy of the set of {@link EnergyProcess} sorted to have the
	 *         original order of the processes.
	 */
	public SortedSet<EnergyProcess> getSortedProcessSet() {
		SortedSet<EnergyProcess> s = new TreeSet<EnergyProcess>();
		for (EnergyProcess p : processSet) {
			s.add(p);
		}
		return s;
	}

	/**
	 *
	 * @return Copy of the heat cascade map with addition of minimal energy.
	 */
	public NavigableMap<Double, Double> getHeatCascade() {
		return new TreeMap<Double, Double>(heatCascade);
	}

	/**
	 *
	 * @return Set of cold process.
	 */
	public Set<EnergyProcess> getColdProcessSet() {
		return coldProcessSet;
	}

	/**
	 *
	 * @return Set of hot process.
	 */
	public Set<EnergyProcess> getHotProcessSet() {
		return hotProcessSet;
	}

	/**
	 *
	 * @return Minimal difference of temperature between the two composite curbs
	 *         (without offset).
	 */
	public Double getDeltaTmin() {
		return deltaTmin;
	}

	/**
	 * Impose the minimal difference of temperature between the two composite curbs.
	 *
	 * @param aDeltaTmin
	 */
	public void setChosenDeltaTmin(Double aDeltaTmin) {
		chosenDeltaTmin = aDeltaTmin;
	}

	/**
	 * Add an offset to the composite curves energy. It will impact both hot and
	 * cold composite curves. By default hot composite curve starts at 0, after the
	 * modification it will start at the value put in parameter.
	 *
	 * @param offsetHotComposite
	 */
	public void addEnergyToSystem(double offsetHotComposite) {
		energyAddedHotComposite = Math.max(0.0, offsetHotComposite);
	}

	/**
	 *
	 * @return Energy added to process through method
	 *         {@link #addEnergyToSystem(double)}.
	 */
	public double getAddedEnergyToSystem() {
		return energyAddedHotComposite;
	}

	/**
	 *
	 * @return Set of process, with the temperature with the deltaT offset.
	 */
	public Set<EnergyProcess> getProcessSetOffsetTemperatures() {
		return processSetOffsetTemperatures;
	}

	/**
	 *
	 * @return List couple, the key is the interval of temperature, from the hottest
	 *         to the coolest. The value is the process involved in this interval.
	 */
	public List<Entry<Double[], List<EnergyProcess>>> getSortedIntervalsToProcess() {
		return sortedOffsetIntervalsToProcess;
	}

	/**
	 *
	 * @return Data for the cold composite curb (temperature, enthalpy flow),
	 *         without offset.
	 */
	public NavigableMap<Double, Double> getOriginalColdCompositeCurb() {
		return originalColdCompositeCurb;
	}

	/**
	 *
	 * @return Data for the hot composite curb (temperature, enthalpy flow), without
	 *         offset.
	 */
	public NavigableMap<Double, Double> getOriginalHotCompositeCurb() {
		return originalHotCompositeCurb;
	}

	/**
	 *
	 * @return Minimal energy to add to the process.
	 */
	public double getMinimalEnergy() {
		return minimalEnergy;
	}

	/**
	 *
	 * @return Temperature of the pinch point.
	 */
	public Double getPinchPointTemperature() {
		return pinchPointTemperature;
	}

	/**
	 *
	 * @return Minimal energy requested to heat.
	 */
	public double getMinimalHeatingEnergyRequested() {
		return minimalHeatingEnergyRequested;
	}

	/**
	 *
	 * @return Minimal energy requested to cool down.
	 */
	public double getMinimalCoolingEnergyRequested() {
		return minimalCoolingEnergyRequested;
	}

	/**
	 *
	 * @return Map of point for the cold composite curb (temperature, enthalpy
	 *         flow).
	 */
	public NavigableMap<Double, Double> getColdCompositeCurb() {
		return new TreeMap<Double, Double>(coldCompositeCurb);
	}

	/**
	 *
	 * @return Map of point for the hot composite curb (temperature, enthalpy flow).
	 */
	public NavigableMap<Double, Double> getHotCompositeCurb() {
		return new TreeMap<Double, Double>(hotCompositeCurb);
	}

	/**
	 * Initialize and compute the data for the analysis.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 */
	public void initialize() throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		final int minimumNumberOfFluids = 2;
		if (processSet.size() < minimumNumberOfFluids) {
			throw new NotEnoughProcessException(name);
		}
		sortProcess();
		if (coldProcessSet.size() < 1) {
			throw new NoHotProcessException(name);
		}
		if (hotProcessSet.size() < 1) {
			throw new NoColdProcessException(name);
		}
		// deltaTmin can be imposed, if so, we let the user choose one for the
		// computation.
		if (chosenDeltaTmin == null) {
			runWithoutDTmin(tryLimit);
		} else {
			runWithChosenDTmin();
		}
		// Raise an exception if the Pinch point is not set, it indicates that the pinch
		// analysis is not relevant.
		if (pinchPointTemperature == null) {
			throw new NoPinchPointException(name);
		}
		isInitialized = true;
	}

	/**
	 * Run the algorithm with the chosen value of delta T min.
	 *
	 * @throws InconsistentDTmin
	 */
	private void runWithChosenDTmin() throws InconsistentDTmin {
		logger.info("Run Pinch with a chosen DTmin of " + chosenDeltaTmin);
		processSetOffsetTemperatures = setProcessSetOfssetTemperatures(processSet, chosenDeltaTmin);
		sortedOffsetIntervalsToProcess = sortTemperatures(processSetOffsetTemperatures);
		initHeatCascade();
		coldCompositeCurb = getCompositeCurb(coldProcessSet, chosenDeltaTmin / 2,
				energyAddedHotComposite + minimalCoolingEnergyRequested);
		hotCompositeCurb = getCompositeCurb(hotProcessSet, -chosenDeltaTmin / 2, energyAddedHotComposite);
		originalColdCompositeCurb = getCompositeCurb(coldProcessSet, 0.0,
				energyAddedHotComposite + minimalCoolingEnergyRequested);
		originalHotCompositeCurb = getCompositeCurb(hotProcessSet, 0.0, energyAddedHotComposite);

		// Set real deltaTmin obtained with the computation (different if pinch point
		// not found). Rounded to the EPSILON accuracy.
		deltaTmin = (Math.rint(computeDeltaTmin(originalColdCompositeCurb, originalHotCompositeCurb) / EPSILON))
				* EPSILON;
		logger.info("Found DTmin " + deltaTmin + " and the chosen one is " + chosenDeltaTmin);
		if (Math.abs(deltaTmin - chosenDeltaTmin) > EPSILON) {
			// Case DTmin isn't consistent.
			throw new InconsistentDTmin(name + " " + chosenDeltaTmin + " " + deltaTmin);
		}
	}

	/**
	 * Run the algorithm without a chosen delta T min, try to guess one. If the
	 * algorithm doesn't find the pinch point, the DTmin read on the original
	 * composite curbs is used to try again. The method loop until a pinch point is
	 * found. This is a heuristic method.
	 *
	 * @param nbTry Max number of loops to have a constant DTmin.
	 * @throws NoDTminFound
	 */
	private void runWithoutDTmin(final int nbTry) throws NoDTminFound {
		deltaTmin = 0.;
		int i = 0;
		while (i < nbTry) {
			processSetOffsetTemperatures = setProcessSetOfssetTemperatures(processSet, deltaTmin);
			sortedOffsetIntervalsToProcess = sortTemperatures(processSetOffsetTemperatures);
			initHeatCascade();
			coldCompositeCurb = getCompositeCurb(coldProcessSet, deltaTmin / 2,
					energyAddedHotComposite + minimalCoolingEnergyRequested);
			hotCompositeCurb = getCompositeCurb(hotProcessSet, -deltaTmin / 2, energyAddedHotComposite);
			originalColdCompositeCurb = getCompositeCurb(coldProcessSet, 0.0,
					energyAddedHotComposite + minimalCoolingEnergyRequested);
			originalHotCompositeCurb = getCompositeCurb(hotProcessSet, 0.0, energyAddedHotComposite);
			// Compute real DTmin, if not equal to guessed one, we do the computation again.
			double newDeltaTmin = computeDeltaTmin(originalColdCompositeCurb, originalHotCompositeCurb);
			if (Math.abs(deltaTmin - newDeltaTmin) < EPSILON) {
				return;
			}
			clearComputedData();

			// Update DTmin for the next try.
			// try to round the lower value, if already greater than that (or most likely
			// equal), round to upper value).
			if (deltaTmin < Math.floor(newDeltaTmin)) {
				deltaTmin = Math.rint(newDeltaTmin);
			} else {
				// Round to the upper int value to avoid getting infinitely the same DTmin.
				deltaTmin = Math.ceil(newDeltaTmin);
			}
			i++;
		}
		// Case DTmin isn't found after the number of loops.
		throw new NoDTminFound(name);
	}

	/**
	 * Clear all computed data, except {@link #deltaTmin} to prepare a new run of
	 * the algorithm.
	 */
	private void clearComputedData() {
		processSetOffsetTemperatures.clear();
		sortedOffsetIntervalsToProcess.clear();
		heatCascade.clear();
		coldCompositeCurb.clear();
		hotCompositeCurb.clear();
		originalColdCompositeCurb.clear();
		originalHotCompositeCurb.clear();
		minimalCoolingEnergyRequested = 0.0;
		minimalEnergy = 0.0;
		minimalHeatingEnergyRequested = 0.0;
	}

	/**
	 * Separate the initial {@link EnergyProcess} between the cold process set
	 * (Tin&lt;Tout) and the hot process set (Tin&gt;Tout).
	 */
	private void sortProcess() {
		for (EnergyProcess process : processSet) {
			if (process.getInletT() < process.getOutletT()) {
				// Fluid is heated during the process, it is a cold fluid.
				coldProcessSet.add(process);
			} else {
				// Fluid is cool down during the process, it is a hot fluid.
				hotProcessSet.add(process);
			}
		}
	}

	/**
	 *
	 * @param theProcessSet
	 * @param theDeltaTmin
	 * @return The set of process with their offset temperatures, to ease
	 *         computation.
	 */
	private static Set<EnergyProcess> setProcessSetOfssetTemperatures(Set<EnergyProcess> theProcessSet,
			double theDeltaTmin) {
		Set<EnergyProcess> processSetOffsetTemperatures = new HashSet<EnergyProcess>();
		for (EnergyProcess p : theProcessSet) {
			double offset = 0.0;
			if (p.isCold()) {
				offset = theDeltaTmin / 2;
			} else {
				offset = -theDeltaTmin / 2;
			}
			processSetOffsetTemperatures.add(p.getOffsetProcess(offset));

		}
		return processSetOffsetTemperatures;
	}

	/**
	 *
	 * @param aProcessSet Set of processes to treat.
	 * @return A list which will contain a tuple of the interval of temperatures
	 *         linked to the process involved. This function is used to compute
	 *         {@link #sortedOffsetIntervalsToProcess}, the list of sorted offset
	 *         temperatures.
	 */
	public static List<Entry<Double[], List<EnergyProcess>>> sortTemperatures(
			Set<EnergyProcess> aProcessSet) {
		NavigableSet<Double> sortedTemperatures = new TreeSet<Double>();
		for (EnergyProcess p : aProcessSet) {
			sortedTemperatures.add(p.getInletT());
			sortedTemperatures.add(p.getOutletT());
		}
		/*
		 * Browse the temperature list in the increasing order, we decompose it in
		 * intervals and for each interval we search for the linked processes (if the
		 * temperature interval is within the temperature range of the process).
		 *
		 */
		List<Entry<Double[], List<EnergyProcess>>> aSortedIntervalsToProcess = new ArrayList<>();
		Iterator<Double> ite = sortedTemperatures.descendingIterator();
		double rightTemperature = ite.next();
		while (ite.hasNext()) {
			double leftTemperature = ite.next();
			Double[] interval = new Double[] {leftTemperature, rightTemperature };
			Entry<Double[], List<EnergyProcess>> entry = new SimpleEntry<Double[], List<EnergyProcess>>(interval,
					new ArrayList<EnergyProcess>());
			aSortedIntervalsToProcess.add(entry);
			for (EnergyProcess p : aProcessSet) {
				if (p.isCold() && p.getInletT() <= leftTemperature && p.getOutletT() >= rightTemperature) {
					entry.getValue().add(p);
				} else if (p.isHot() && p.getOutletT() <= leftTemperature && p.getInletT() >= rightTemperature) {
					entry.getValue().add(p);
				}
			}
			rightTemperature = leftTemperature;
		}
		return aSortedIntervalsToProcess;
	}

	/**
	 * Compute the heat cascade with addition of {@link #minimalEnergy}. Compute
	 * {@link #minimalEnergy} at the same time to add it at the end. And set the
	 * pinch point temperature. Request {@link #sortedOffsetIntervalsToProcess}
	 */
	private void initHeatCascade() {
		// First, we compute the heat cascade
		// At the same time we get the minimal energy at the pinch point.
		Double cumul = 0.;
		minimalEnergy = cumul;
		heatCascade.put(sortedOffsetIntervalsToProcess.get(0).getKey()[1], cumul);
		// Default Pinch point temperature.
		pinchPointTemperature = sortedOffsetIntervalsToProcess.get(0).getKey()[1];
		for (int i = 0; i < sortedOffsetIntervalsToProcess.size(); i++) {
			Entry<Double[], List<EnergyProcess>> currentInterval = sortedOffsetIntervalsToProcess.get(i);
			for (EnergyProcess p : currentInterval.getValue()) {
				// Compute the amount of energy in this interval, for this process.
				final double power = p.getH(currentInterval.getKey()[1]) * p.getMassFlow()
						- p.getH(currentInterval.getKey()[0]) * p.getMassFlow();
				if (p.isHot()) {
					cumul += power;
				} else {
					cumul -= power;
				}
			}
			if (minimalEnergy > cumul) {
				minimalEnergy = cumul;
				pinchPointTemperature = currentInterval.getKey()[0];
			}
			heatCascade.put(currentInterval.getKey()[0], cumul);
		}
		heatCascade.forEach((k, v) -> heatCascade.put(k, v - minimalEnergy));

		// Search minimal heating energy requested. Energy at Tmax
		minimalHeatingEnergyRequested = heatCascade.lastEntry().getValue();
		// Search minimal heating energy requested. Energy at Tmin
		minimalCoolingEnergyRequested = heatCascade.firstEntry().getValue();
	}

	/**
	 *
	 * @param processSet   Set of processes to consider.
	 * @param deltaT       Delta of temperature to add if an offset of temperature
	 *                     is requested. deltaT is removed if hot fluid and added if
	 *                     cold fluid.
	 * @param energyOffset Offset on the energy abscissa.
	 * @return A navigable map to draw the composite curb.
	 */
	private static NavigableMap<Double, Double> getCompositeCurb(Set<EnergyProcess> processSet, double deltaT,
			double energyOffset) {
		NavigableSet<Double> sortedTemperatures = new TreeSet<Double>();
		for (EnergyProcess p : processSet) {
			double offset = p.isCold() ? Math.abs(deltaT) : -Math.abs(deltaT);
			sortedTemperatures.add(p.getInletT() + offset);
			sortedTemperatures.add(p.getOutletT() + offset);
		}

		Iterator<Double> ite = sortedTemperatures.iterator();
		Double cumul = energyOffset;
		double leftTemp = ite.next();
		NavigableMap<Double, Double> compositeCurb = new TreeMap<Double, Double>();
		compositeCurb.put(leftTemp, cumul);
		while (ite.hasNext()) {
			double rightTemp = ite.next();
			// If the process belongs to this temperature window, we add it (either it is
			// cold on hot)
			for (EnergyProcess p : processSet) {
				if (p.getOutletT() + deltaT <= leftTemp && p.getInletT() + deltaT >= rightTemp) {
					cumul += (p.getH(rightTemp) - p.getH(leftTemp)) * p.getMassFlow();
				} else if (p.getInletT() + deltaT <= leftTemp && p.getOutletT() + deltaT >= rightTemp) {
					cumul += (p.getH(rightTemp) - p.getH(leftTemp)) * p.getMassFlow();
				}
			}
			compositeCurb.put(rightTemp, cumul);
			leftTemp = rightTemp;
		}
		return compositeCurb;

	}

	/**
	 * Search real DeltaTmin from the two composite curbs, do interpolation if
	 * needed.
	 *
	 * @param coldComppositeNoOffset
	 * @param hotComppositeNoOffset
	 * @return DeltaTmin
	 */
	public static double computeDeltaTmin(NavigableMap<Double, Double> coldComppositeNoOffset,
			NavigableMap<Double, Double> hotComppositeNoOffset) {
		double deltaTmin2 = Double.MAX_VALUE;

		// For each energy point, we look the difference between cold and hot
		// temperatures.
		NavigableMap<Double, Double> coldEnergyTemperature = new TreeMap<Double, Double>();
		coldComppositeNoOffset.forEach((temperature, energy) -> coldEnergyTemperature.put(energy, temperature));
		NavigableMap<Double, Double> hotEnergyTemperature = new TreeMap<Double, Double>();
		hotComppositeNoOffset.forEach((temperature, energy) -> hotEnergyTemperature.put(energy, temperature));

		// The problem is to find interpolated value of temperature for hot curb when we
		// browse cold curb abscissa.
		for (Double energy : coldEnergyTemperature.navigableKeySet()) {
			double dT = Double.MAX_VALUE;
			// At the same energy abscissa, hot and cold curb have a point, so we just
			// compare. No need to interpolate.
			if (hotEnergyTemperature.get(energy) != null) {
				dT = Math.abs(hotEnergyTemperature.get(energy) - coldEnergyTemperature.get(energy));
			} else if (!(hotEnergyTemperature.firstKey() > energy || hotEnergyTemperature.lastKey() < energy)) {
				// We can interpolate the value of the fluid (vertical line from our energy to
				// find
				// the hot temperature).
				double energyLeft = hotEnergyTemperature.firstKey();
				double energyRight = hotEnergyTemperature.lastKey();
				Iterator<Double> ite = hotEnergyTemperature.navigableKeySet().iterator();
				while (ite.hasNext()) {
					double nrj = ite.next();
					// while the nrg < energy we update the left side, as soon as nrj > energy we
					// have the right side.
					if (nrj < energy) {
						energyLeft = nrj;
					} else {
						energyRight = nrj;
						break;
					}
				}
				double leftT = hotEnergyTemperature.get(energyLeft);
				double rightT = hotEnergyTemperature.get(energyRight);
				double interpolatedT = leftT + (rightT - leftT) / (energyRight - energyLeft) * (energy - energyLeft);
				dT = Math.abs(interpolatedT - coldEnergyTemperature.get(energy));
			}
			if (dT < deltaTmin2) {
				deltaTmin2 = dT;
			}
		}
		// We do the same browsing the hot curb.
		for (Double energy : hotEnergyTemperature.navigableKeySet()) {
			double dT = Double.MAX_VALUE;
			if (coldEnergyTemperature.get(energy) != null) {
				dT = Math.abs(hotEnergyTemperature.get(energy) - coldEnergyTemperature.get(energy));
			} else if (!(coldEnergyTemperature.firstKey() > energy || coldEnergyTemperature.lastKey() < energy)) {
				// We can interpolate the value of the fluid (vertical line from our energy to
				// find
				// the hot temperature).
				double energyLeft = coldEnergyTemperature.firstKey();
				double energyRight = coldEnergyTemperature.lastKey();
				Iterator<Double> ite = coldEnergyTemperature.navigableKeySet().iterator();
				while (ite.hasNext()) {
					double nrj = ite.next();
					// while the nrg < energy we update the left side, as soon as nrj > energy we
					// have the right side.
					if (nrj < energy) {
						energyLeft = nrj;
					} else {
						energyRight = nrj;
						break;
					}
				}
				double leftT = coldEnergyTemperature.get(energyLeft);
				double rightT = coldEnergyTemperature.get(energyRight);
				double interpolatedT = leftT + (rightT - leftT) / (energyRight - energyLeft) * (energy - energyLeft);
				dT = Math.abs(interpolatedT - hotEnergyTemperature.get(energy));
			}
			if (dT < deltaTmin2) {
				deltaTmin2 = dT;
			}
		}
		return deltaTmin2;
	}

	@Override
	public String toString() {
		String s = "Pinch analysis " + name + "\n";
		s += "Cold processes: name(fluid name, cp, intel temperature, outlet temperature)\n";
		for (EnergyProcess p : coldProcessSet) {
			s += p + "\n";
		}
		s += "Hot processes: name(fluid name, cp, intel temperature, outlet temperature)\n";
		for (EnergyProcess p : hotProcessSet) {
			s += p + "\n";
		}
		s += "Heat Cascade\n";
		s += "Temperature, Energy (kJ) \n";
		s += "T (K),\tH (kJ)\n";
		for (Double temperature : heatCascade.keySet()) {
			s += temperature + ",\t" + heatCascade.get(temperature) + "\n";
		}
		return s;
	}

	/**
	 *
	 * @return {@link #csvSeparator}
	 */
	public String getCsvSeparator() {
		return csvSeparator;
	}

	/**
	 *
	 * @param newCsvSeparator New value for {@link #csvSeparator}.
	 */
	public void setCsvSeparatpr(String newCsvSeparator) {
		csvSeparator = newCsvSeparator;
	}

	/**
	 * Export the heat cascade tab in a CSV file.
	 *
	 * @param filePath Path of the file to be exported.
	 * @throws FileNotFoundException
	 */
	public void exportHeatCascade(String filePath) throws FileNotFoundException {
		File file = new File(filePath);
		PrintWriter out = new PrintWriter(file);
		out.print("Temperature (Celsius)" + csvSeparator + "Energy (kW)\n");
		for (Double key : heatCascade.navigableKeySet()) {
			out.print(key + csvSeparator + heatCascade.get(key) + "\n");
		}
		out.close();
	}

	/**
	 * Export the fluids tab in a CSV file.
	 *
	 * @param filePath Path of the file to be exported.
	 * @throws FileNotFoundException
	 */
	public void exportProcessesTab(String filePath) throws FileNotFoundException {
		// Order the process by name to have always the same order.
		NavigableMap<String, EnergyProcess> processMap = new TreeMap<String, EnergyProcess>();
		for (EnergyProcess p : processSet) {
			processMap.put(p.getName(), p);
		}
		File file = new File(filePath);
		PrintWriter out = new PrintWriter(file);
		out.print(processMap.firstEntry().getValue().exportCsvFirstLine(csvSeparator) + "\n");
		for (EnergyProcess p : processMap.values()) {
			out.print(p.exportCsvLine(csvSeparator) + "\n");
		}
		out.close();
	}

	/**
	 *
	 * @param filePath Path to the file to read. First line contains titles, others
	 *                 lines contains (Fluid id; Specific Heat capacity; inlet
	 *                 temperature (Celsius); outlet temperature (Celsius); mass
	 *                 flow (m/s)).
	 * @throws IOException
	 */
	public void importFluidTab(String filePath) throws IOException {
		processSet.clear();
		clearComputedData();
		File file = new File(filePath);
		if (file.exists()) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			final int processType = line.split(csvSeparator).length;
			// First line contains only titles, we use it to determine the kind of tables.
			final int caseCp = 6;
			final int caseH = 7;
			if (line != null) {
				while ((line = br.readLine()) != null) {
					EnergyProcess p;
					switch (processType) {
					case caseCp:
						p = new EnergyProcessHeatCapacity();
						break;
					case caseH:
						p = new EnergyProcessEnthalpy();
						break;
					default:
						p = null;
						logger.error("Cannot parse the line " + line + " to create a Process!");
						break;
					}
					p.importFromCsvLine(line, csvSeparator);
					processSet.add(p);
				}
			}
			br.close();
		}
	}

	/**
	 *
	 * @return {@link #tryLimit}.
	 */
	public int getTryLimit() {
		return tryLimit;
	}

	/**
	 *
	 * @param aTryLimit {@link #tryLimit}.
	 */
	public void setTryLimit(int aTryLimit) {
		assert aTryLimit > 0;
		tryLimit = aTryLimit;
	}

	/**
	 *
	 * @return {@link #isInitialized}.
	 */
	public boolean isInitialized() {
		return isInitialized;
	}

	/**
	 *
	 * @return List of all enthalpy flow values from original composite curves.
	 */
	public List<Double> getEnthalpyFlowValues() {
		NavigableSet<Double> enthalpyIntervals = new TreeSet<Double>();
		enthalpyIntervals.addAll(originalColdCompositeCurb.values());
		enthalpyIntervals.addAll(originalHotCompositeCurb.values());
		return new ArrayList<Double>(enthalpyIntervals);
	}

	/**
	 * @param mapSuchAsCompositeCurve
	 * @return Inverse key and value of a (Double, Double) map..
	 */
	public static NavigableMap<Double, Double> inverseKeyValue(NavigableMap<Double, Double> mapSuchAsCompositeCurve) {
		NavigableMap<Double, Double> enthalpyIntervals = new TreeMap<Double, Double>();
		for (Entry<Double, Double> entry : mapSuchAsCompositeCurve.entrySet()) {
			enthalpyIntervals.put(entry.getValue(), entry.getKey());
		}
		return enthalpyIntervals;
	}

	private NavigableMap<Double, Double> getEnthalpyFlowIntervalsLinkedToTemperatures(
			NavigableMap<Double, Double> compositeCurveHotOrCold) {
		NavigableMap<Double, Double> hotEnthalpyFlowTemperature = inverseKeyValue(compositeCurveHotOrCold);
		NavigableMap<Double, Double> results = new TreeMap<Double, Double>();
		List<Double> enthalpyFlowsIntervals = getEnthalpyFlowValues();
		Iterator<Double> ite = hotEnthalpyFlowTemperature.navigableKeySet().iterator();
		double leftKnownEnergy = ite.next();
		double rightKnownEnergy = ite.next();
		for (double enthalpyFlow : enthalpyFlowsIntervals) {
			// If we go further the current enthalpy interval, we move it forward
			if (enthalpyFlow > rightKnownEnergy) {
				while (ite.hasNext() && enthalpyFlow > rightKnownEnergy) {
					leftKnownEnergy = rightKnownEnergy;
					rightKnownEnergy = ite.next();
				}
			}
			// If we are within the bounds of the interval, we add the value.
			if (enthalpyFlow >= leftKnownEnergy && enthalpyFlow <= rightKnownEnergy) {
				double leftT = hotEnthalpyFlowTemperature.get(leftKnownEnergy);
				double rightT = hotEnthalpyFlowTemperature.get(rightKnownEnergy);
				double interpolatedT = leftT + (rightT - leftT) / (rightKnownEnergy - leftKnownEnergy)
						* (enthalpyFlow - leftKnownEnergy);
				results.put(enthalpyFlow, interpolatedT);
			} // else nothing
		}
		return results;
	}

	/**
	 *
	 * @return A map whose keys are all enthalpy flows and the temperature on the
	 *         hot composite curve (interpolate if needed).
	 */
	public NavigableMap<Double, Double> getEnthalpyFlowIntervalsLinkedToTemperaturesHot() {
		return getEnthalpyFlowIntervalsLinkedToTemperatures(originalHotCompositeCurb);
	}

	/**
	 *
	 * @return A map which whose keys are all enthalpy flows and the temperature on
	 *         the hot composite curve (interpolate if needed).
	 */
	public NavigableMap<Double, Double> getEnthalpyFlowIntervalsLinkedToTemperaturesCold() {
		return getEnthalpyFlowIntervalsLinkedToTemperatures(originalColdCompositeCurb);
	}

}
