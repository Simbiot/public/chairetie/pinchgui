package pinchgui.pinch;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents an energy utility (heater or cooler).
 *
 * @author tparis
 *
 */
public class EnergyUtility implements Serializable {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.pinch.utility";

	/**
	 * Default serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the process.
	 */
	private String name;

	/**
	 * Temperature of the fluid at the inlet of this process.
	 */
	private double inletT;

	/**
	 * Temperature of the fluid at the outlet of this process.
	 */
	private double outletT;

	/**
	 * Cost of the utility by kW (euros/kW)
	 */
	private double cost;

	/**
	 * Logger to debug code.
	 */
	private Logger logger;

	/**
	 * Process constructor with only name, inlet and outlet temperatures, and cost.
	 *
	 * @param aName
	 * @param inT
	 * @param outT
	 * @param c
	 */
	public EnergyUtility(String aName, double inT, double outT, double c) {
		name = aName;
		inletT = inT;
		outletT = outT;
		cost = c;
		logger = LoggerFactory.getLogger(CONCEPT_ID + "." + name);
	}

	/**
	 *
	 * @return Name of the process.
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @return Mass flow of the fluid in the process, kg/s.
	 */
	public double getCost() {
		return cost;
	}

	/**
	 *
	 * @return Inlet temperature.
	 */
	public double getInletT() {
		return inletT;
	}

	/**
	 *
	 * @return Outlet temperature.
	 */
	public double getOutletT() {
		return outletT;
	}

	/**
	 *
	 * @return Utility is used to cool down another fluid, its outlet temperature is
	 *         greater than its inlet temperature (or equal if it is a state change
	 *         that request energy).
	 */
	public boolean isCold() {
		return outletT >= inletT;
	}

	/**
	 *
	 * @return Utility is used to heat another fluid, its outlet temperature is
	 *         strictly lower than its inlet temperature.
	 */
	public boolean isHot() {
		return outletT < inletT;
	}

	/**
	 *
	 * @return logger.
	 */
	public Logger getLogger() {
		return logger;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof EnergyUtility) {
			EnergyUtility p = (EnergyUtility) obj;
			return inletT == p.getInletT() && outletT == p.getOutletT() && cost == p.getCost()
					&& name.equals(p.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return name.hashCode() + Double.hashCode(inletT) + Double.hashCode(outletT)
				+ Double.hashCode(cost);
	}

	@Override
	public String toString() {
		return getName() + ": " + (isCold() ? "Cold" : "Hot") + " energy utility " + getInletT() + "->" + getOutletT();
	}

}
