package pinchgui.pinch;

import static pinchgui.pinch.HeatExchanger.isCpRuleRespected;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Take a {@link EnergyTargetingAnalysis} object and provide methods for the HEN
 * analysis.
 *
 * @author tparis
 *
 */
public class HenAnalysis {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.pinch.henanalysis";

	/**
	 * Pinch object on which the analysis must be done.
	 */
	private final EnergyTargetingAnalysis energyTargeting;

	/**
	 * Set of utilities.
	 */
	private final Set<EnergyUtility> utilitySet;

	/**
	 * List of heat exchangers between internal flow proposed for this system.
	 */
	private final List<HeatExchanger> internalHeatExchangerList;

	/**
	 * Minimal power of a heat exchanger that can be proposed.
	 */
	private double minimalHeatExchangerPower;

	/**
	 * Boolean to indicate that the algorithm has run.
	 */
	private boolean hasComputed;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger;

	/**
	 * Instantiate an {@link HenAnalysis} with
	 *
	 * @param aPinch
	 */
	public HenAnalysis(final EnergyTargetingAnalysis aPinch, Set<EnergyUtility> aUtilitySet) {
		energyTargeting = aPinch;
		utilitySet = new HashSet<EnergyUtility>(aUtilitySet);
		internalHeatExchangerList = new ArrayList<HeatExchanger>();
		logger = LoggerFactory.getLogger(CONCEPT_ID + energyTargeting.getName());
		minimalHeatExchangerPower = 0;
		hasComputed = false;
	}

	/**
	 *
	 * @return {@link #minimalHeatExchangerPower}.
	 */
	public double getMinimalHeatExchangerPower() {
		return minimalHeatExchangerPower;
	}

	/**
	 *
	 * @param newMinimalHeatExchangerPower New {@link #minimalHeatExchangerPower},
	 *                                     must be positive.
	 */
	public void setMinimalHeatExchangerPower(double newMinimalHeatExchangerPower) {
		minimalHeatExchangerPower = newMinimalHeatExchangerPower >= 0 ? newMinimalHeatExchangerPower
				: minimalHeatExchangerPower;
	}

	/**
	 * Run the computation of the HEN design algorithm to find placement of heat
	 * exchangers.
	 *
	 * @throws InconsistentDTmin
	 * @throws NoDTminFound
	 * @throws NoPinchPointException
	 * @throws NotEnoughProcessException
	 */
	public void initialize() throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		// First we ensure the pinch analysis energetic part has been performed.
		if (!energyTargeting.isInitialized()) {
			energyTargeting.initialize();
		}
		internalHenDesign(energyTargeting);
		hasComputed = true;
	}

	/**
	 * Identify the different subsets using {@link EnergyTargetingAnalysis} and
	 * apply itself recursively to the subsystem to figure out a heat exchanger
	 * network feasible.
	 *
	 * @param currentPinchAnalysis The current system on which we try to figure out
	 *                             the HEN. If it can be split, the method will
	 *                             split it into two subsystems and apply itself
	 *                             recursively on them.
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	private void internalHenDesign(EnergyTargetingAnalysis currentPinchAnalysis)
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {

		// Split the processes
		SplitProcessSets l = SplitProcessSets.splitProcesses(currentPinchAnalysis);

		if (l.getBelow().size() == 0 || l.getAbove().size() == 0) {
			// Stopping case, we have one system and the pinch temperature is on one of the
			// boundaries, i.e. we are solving a subsystem below or above the pinch of a
			// parent system. We just return the list given by the stream splitting
			// algorithm.
			logger.info("Internal HEN Design: No more subset for " + currentPinchAnalysis.getProcessSet());
			streamSplittingAlgorithm(
					getProcessesEnteringPinch(currentPinchAnalysis.getProcessSetOffsetTemperatures(),
							currentPinchAnalysis.getPinchPointTemperature()),
					getProcessesFlowingAwayFromPinch(currentPinchAnalysis.getProcessSetOffsetTemperatures(),
							currentPinchAnalysis.getPinchPointTemperature()),
					currentPinchAnalysis.getPinchPointTemperature());
		} else {
			/*
			 * Case, several subsystems are detected, i.e. a pinch point has been found and
			 * we have split the system in two sub-systems. We apply recursively the hen
			 * method on the subsystem. Note that as we work on offset temperature, no need
			 * to specify again the DTmin of the first pinch (the main system).
			 */
			if (isHotAndColdStream(l.getBelow())) {
				logger.info("Internal HEN Design: Study the subset below " + l.getBelow() + " from "
						+ currentPinchAnalysis.getProcessSet() + "Pinch Point "
						+ currentPinchAnalysis.getPinchPointTemperature());
				EnergyTargetingAnalysis subPinchAnalysis = new EnergyTargetingAnalysis(
						currentPinchAnalysis.getName() + ".below");
				subPinchAnalysis.addProcess(l.getBelow());
				subPinchAnalysis.initialize();
				HenAnalysis subHen = new HenAnalysis(subPinchAnalysis, utilitySet);
				subHen.setMinimalHeatExchangerPower(getMinimalHeatExchangerPower());
				subHen.internalHenDesign(subPinchAnalysis);
				internalHeatExchangerList.addAll(subHen.getHeatExchangerList());
			} else {
				logger.info(
						"Internal HEN Design: No hot and cold streams in the subset below "
								+ l.getBelow() + " from " + currentPinchAnalysis.getProcessSet()
								+ "Pinch Point " + currentPinchAnalysis.getPinchPointTemperature());
			}
			if (isHotAndColdStream(l.getAbove())) {
				logger.info("Internal HEN Design: Study the subset above " + l.getAbove() + " from "
						+ currentPinchAnalysis.getProcessSet());
				EnergyTargetingAnalysis subPinchAnalysis = new EnergyTargetingAnalysis(
						currentPinchAnalysis.getName() + ".above");
				subPinchAnalysis.addProcess(l.getAbove());
				subPinchAnalysis.initialize();
				HenAnalysis subHen = new HenAnalysis(subPinchAnalysis, utilitySet);
				subHen.setMinimalHeatExchangerPower(getMinimalHeatExchangerPower());
				subHen.internalHenDesign(subPinchAnalysis);
				internalHeatExchangerList.addAll(subHen.getHeatExchangerList());
			} else {
				logger.info(
						"Internal HEN Design: No hot and cold streams in the subset above "
								+ l.getAbove() + " from " + currentPinchAnalysis.getProcessSet()
								+ "Pinch Point " + currentPinchAnalysis.getPinchPointTemperature());
			}
		}
	}

	/**
	 * Check if there is at least one cold fluid and one hot fluid in a set of
	 * energy processes.
	 *
	 * @param setProcesses
	 * @return True if we can find at least one cold and one hot stream.
	 */
	public static boolean isHotAndColdStream(Set<EnergyProcess> setProcesses) {
		Iterator<EnergyProcess> ite = setProcesses.iterator();
		boolean is1Cold = false;
		boolean is1Hot = false;
		while (ite.hasNext() && (!is1Cold || !is1Hot)) {
			EnergyProcess p = ite.next();
			if (p.isCold()) {
				is1Cold = true;
			} else {
				is1Hot = true;
			}
		}
		return is1Cold && is1Hot;
	}

	/**
	 *
	 * @param leavingFlow  {@link #EnergyFlow} of a process that leaves the pinch.
	 * @param enteringFlow {@link #EnergyFlow} of a process that enters the pinch.
	 * @return The heat exchanger that fits the two {@link #EnergyFlow} in parameter
	 *         and which maximize the heat exchange considering a minimal difference
	 *         of temperature of 0.
	 */
	private static HeatExchanger getHeatExchanger(EnergyFlow leavingFlow,
			EnergyFlow enteringFlow) {
		if (leavingFlow.getOwner().isHot()) {
			return HeatExchanger.getHeatExchanger(leavingFlow, enteringFlow);
		} else {
			return HeatExchanger.getHeatExchanger(enteringFlow, leavingFlow);
		}
	}

	/**
	 * Split the current entering process into two parts to respect the CPout >=
	 * CPin rule. The process is split in a way that one of its part has a CP equal
	 * to CPout.
	 *
	 * @param theCPout            The CPout
	 * @param enteringFlowToSplit The entering process to split.
	 * @param lFlowEnteringPinch  The list of entering flows, will be updated by the
	 *                            function.
	 */
	public static void splitEnteringProcess(double theCPout,
			EnergyFlow enteringFlowToSplit, List<EnergyFlow> lFlowEnteringPinch) {
		assert enteringFlowToSplit.getC() > theCPout; // Reason why the flow must be split is ensured.
		double ratio = theCPout / enteringFlowToSplit.getC();
		EnergyFlow newEnergyFlowA = new EnergyFlow(enteringFlowToSplit.getOwner(),
				enteringFlowToSplit.getId() + "_a",
				enteringFlowToSplit.getInletTemperature(),
				enteringFlowToSplit.getOutletTemperature(),
				enteringFlowToSplit.getC() * ratio);
		EnergyFlow newEnergyFlowB = new EnergyFlow(enteringFlowToSplit.getOwner(),
				enteringFlowToSplit.getId() + "_b",
				enteringFlowToSplit.getInletTemperature(),
				enteringFlowToSplit.getOutletTemperature(),
				enteringFlowToSplit.getC() * (1 - ratio));
		lFlowEnteringPinch.remove(enteringFlowToSplit);
		lFlowEnteringPinch.add(newEnergyFlowA);
		lFlowEnteringPinch.add(newEnergyFlowB);
	}

	/**
	 *
	 * @param processes        Set of processes.
	 * @param pinchTemperature Identification of the stream
	 * @return The list of @link{EnergyFlow} that enter the pinch.
	 */
	public static List<EnergyFlow> getProcessesEnteringPinch(Set<EnergyProcess> processes, double pinchTemperature) {
		List<EnergyFlow> lFlowEnteringPinch = new ArrayList<EnergyFlow>();
		for (EnergyProcess p : processes) {
			if (p.isHot()) {
				// Hot enter the stream from above.
				if (p.getInletT() > pinchTemperature && p.getOutletT() <= pinchTemperature) {
					lFlowEnteringPinch.add(
							new EnergyFlow(p, p.getName(), p.getInletT(),
									pinchTemperature, p.getC()));
				}
			} else {
				// Cold enter the stream from below.
				if (p.getInletT() < pinchTemperature && p.getOutletT() >= pinchTemperature) {
					lFlowEnteringPinch.add(
							new EnergyFlow(p, p.getName(), p.getInletT(),
									pinchTemperature, p.getC()));
				}
			}
		}
		return lFlowEnteringPinch;
	}

	/**
	 *
	 * @param processes        Set of processes.
	 * @param pinchTemperature Identification of the stream
	 * @return The list of @link{EnergyFlow} that leave the pinch.
	 */
	public static List<EnergyFlow> getProcessesLeavingPinch(Set<EnergyProcess> processes, double pinchTemperature) {
		List<EnergyFlow> lFlowLeavingPinch = new ArrayList<EnergyFlow>();
		for (EnergyProcess p : processes) {
			if (p.isHot()) {
				// Hot enter the stream from below.
				if (p.getOutletT() < pinchTemperature && p.getInletT() >= pinchTemperature) {
					lFlowLeavingPinch.add(
							new EnergyFlow(p, p.getName(), pinchTemperature,
									p.getOutletT(), p.getC()));
				}
			} else {
				// Cold enter the stream from above.
				if (p.getOutletT() > pinchTemperature && p.getInletT() <= pinchTemperature) {
					lFlowLeavingPinch.add(
							new EnergyFlow(p, p.getName(), pinchTemperature,
									p.getOutletT(), p.getC()));
				}
			}
		}
		return lFlowLeavingPinch;
	}

	/**
	 * Get all the processes that flow away from the pinch (event if they do not
	 * start at the pinch).
	 *
	 * @param processes        Set of processes.
	 * @param pinchTemperature Identification of the stream
	 * @return The list of @link{EnergyFlow} that leave the pinch.
	 */
	public static List<EnergyFlow> getProcessesFlowingAwayFromPinch(Set<EnergyProcess> processes,
			double pinchTemperature) {
		List<EnergyFlow> lFlowFlowingAwayFromPinch = new ArrayList<EnergyFlow>();
		for (EnergyProcess p : processes) {
			if (p.isHot()) {
				// Hot flows are flowing away from the pinch when we are studying the subset
				// below the pinch.
				// All the hot flows that leaves the pinch have an outlet temperature below the
				// pinch.
				if (p.getOutletT() < pinchTemperature) {
					if (p.getInletT() >= pinchTemperature) {
						// Case the hot flow starts above the pinch, we cut it at the pinch.
						lFlowFlowingAwayFromPinch.add(
								new EnergyFlow(p, p.getName(), pinchTemperature,
										p.getOutletT(), p.getC()));
					} else {
						// Case the flow starts below the pinch, we can keep its normal inlet
						// temperature.
						lFlowFlowingAwayFromPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										p.getOutletT(), p.getC()));
					}
				}
			} else {
				// Cold flows are flowing away from the pinch when we are studying the subset
				// above the pinch.
				// All the cold flows that leaves the pinch have an outlet temperature above the
				// pinch.
				if (p.getOutletT() > pinchTemperature) {
					if (p.getInletT() <= pinchTemperature) {
						// Case the flow starts below the pinch and we cut it at the pinch.
						lFlowFlowingAwayFromPinch.add(
								new EnergyFlow(p, p.getName(), pinchTemperature,
										p.getOutletT(), p.getC()));
					} else {
						// Case the flow starts above the pinch, we can keep its normal outlet
						// temperature.
						lFlowFlowingAwayFromPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										p.getOutletT(), p.getC()));
					}
				}
			}
		}
		return lFlowFlowingAwayFromPinch;
	}

	/**
	 * Get all the processes that flow away from the pinch (event if they do not
	 * start at the pinch).
	 *
	 * @param processes        Set of processes.
	 * @param pinchTemperature Identification of the stream
	 * @return The list of @link{EnergyFlow} that leave the pinch.
	 */
	public static List<EnergyFlow> getProcessesFlowingTowardFromPinch(Set<EnergyProcess> processes,
			double pinchTemperature) {
		List<EnergyFlow> lFlowFlowingTowardPinch = new ArrayList<EnergyFlow>();
		for (EnergyProcess p : processes) {
			if (p.isHot()) {
				// Hot flows are flowing toward the pinch when we are studying the subset
				// above the pinch.
				// All the hot flows that enter the pinch have an inlet temperature above the
				// pinch.
				if (p.getInletT() > pinchTemperature) {
					if (p.getOutletT() <= pinchTemperature) {
						// Case the hot flow ends below the pinch, we cut it at the pinch.
						lFlowFlowingTowardPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										pinchTemperature, p.getC()));
					} else {
						// Case the flow ends above the pinch, we can keep its normal inlet
						// temperature.
						lFlowFlowingTowardPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										p.getOutletT(), p.getC()));
					}
				}
			} else {
				// Cold flows are flowing toward the pinch when we are studying the subset
				// below the pinch.
				// All the cold flows that enters the pinch have an inlet temperature below the
				// pinch.
				if (p.getInletT() < pinchTemperature) {
					if (p.getOutletT() >= pinchTemperature) {
						// Case the flow ends above the pinch and we cut it at the pinch.
						lFlowFlowingTowardPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										pinchTemperature, p.getC()));
					} else {
						// Case the flow ends below the pinch, we can keep its normal outlet
						// temperature.
						lFlowFlowingTowardPinch.add(
								new EnergyFlow(p, p.getName(), p.getInletT(),
										p.getOutletT(), p.getC()));
					}
				}
			}
		}
		return lFlowFlowingTowardPinch;
	}

	/**
	 *
	 * @param enteringFlowWithGreaterCPin Current entering flow with the greater
	 *                                    heat capacity flow.
	 * @param lFlowLeavingPinch           Ordered list of leaving flows.
	 * @return The leaving flow to use for the HEN design.
	 */
	private EnergyFlow selectLeavingFlowToExchange(EnergyFlow enteringFlowWithGreaterCPin,
			List<EnergyFlow> lFlowLeavingPinch) {
		EnergyFlow candidate = tickOffRule(enteringFlowWithGreaterCPin, lFlowLeavingPinch);
		if (candidate != null) {
			return candidate;
		}
		// Default return the candidate with the closest temperature to the pinch, of
		// greater heat capacity flow (or power or name).
		return lFlowLeavingPinch.get(lFlowLeavingPinch.size() - 1);
	}

	/**
	 * Return the candidate that maximize the heat exchanged with the entering flow
	 * to consider. Do not check the CP rule.
	 *
	 * @param enteringFlow
	 * @param lFlowLeavingPinch
	 * @return The corresponding candidate or null if no one is found.
	 */
	private EnergyFlow tickOffRule(EnergyFlow enteringFlow, List<EnergyFlow> lFlowLeavingPinch) {
		double heatExchangerPower = 0.;
		EnergyFlow candidate = null;
		for (int i = lFlowLeavingPinch.size() - 1; i >= 0; i--) {
			// If the CP rule is respected, try to maximize the energy exchanged by the
			// potential heat exchanger that will be placed.
			HeatExchanger potentialExchanger = getHeatExchanger(lFlowLeavingPinch.get(i), enteringFlow);
			// >0 to avoid loop with 0kW exchange, and <= heatExchangerPower to have the
			// last fluid that maximize the exchange.
			if (potentialExchanger.getHeatPower() > minimalHeatExchangerPower
					&& potentialExchanger.getHeatPower() >= heatExchangerPower) {
				candidate = lFlowLeavingPinch.get(i);
				heatExchangerPower = potentialExchanger.getHeatPower();
			}
		}
		if (candidate != null) {
			getLogger().info("Tick Off Rule: For the entering fluid " + enteringFlow.getId()
					+ ", the leaving fluid that maximizes the heat exchange is " + candidate.getId() + ": "
					+ heatExchangerPower + " kW");
		}
		return candidate;
	}

	/**
	 * Perform a stream splitting algorithm to propose a first set of heat exchanger
	 * that may have to be optimized later. The purpose is to reach the targeting
	 * energy recovery. This method fill {@link #internalHeatExchangerList}.
	 *
	 * Note that the algorithm works on the offset temperature.
	 *
	 * @param lFlowEnteringPinch Set of flows that enters the pinch. The purpose is
	 *                           to lead them to the pinch only with heat exchanges
	 *                           (the feasibility is a property of the pinch
	 *                           analysis).
	 * @param lFlowLeavingPinch  Set of flows that leaves the pinch.
	 * @param pinchTemperature   Pinch temperature considered.
	 */
	public void streamSplittingAlgorithm(List<EnergyFlow> lFlowEnteringPinch,
			List<EnergyFlow> lFlowLeavingPinch, double pinchTemperature) {

		// All entering process are ordered firstly by their outlet temperature
		// (distance with the pinch), then by the CP, power and name.
		// All leaving flow, by mirror, are ordered by their inlet temperature first.
		Comparator<EnergyFlow> comparatorEnteringProcesses = new EnergyFlowComparatorTout(pinchTemperature);
		Comparator<EnergyFlow> comparatorLeavingProcesses = new EnergyFlowComparatorTin(pinchTemperature);

		getLogger().info("Stream Splitting Algorithm starts!");
		int countLoop = 1;
		boolean errorStop = false;
		while (!errorStop && lFlowEnteringPinch.size() > 0 && lFlowLeavingPinch.size() > 0) {
			// Ensure they are sorted
			lFlowEnteringPinch.sort(comparatorEnteringProcesses);
			lFlowLeavingPinch.sort(comparatorLeavingProcesses);

			getLogger().info("Loop " + countLoop + ", entering flow set: " + lFlowEnteringPinch);
			getLogger().info("Loop " + countLoop + ", leaving flow set: " + lFlowLeavingPinch);

			// Rule N_out >= N_in -> Automatic fit to this rule because leaving fluids are
			// split at each use if they are not fulfilled.
			EnergyFlow enteringFlowWithGreaterCPin = lFlowEnteringPinch.get(lFlowEnteringPinch.size() - 1);
			EnergyFlow leavingFlowWithGreaterCPout = selectLeavingFlowToExchange(enteringFlowWithGreaterCPin,
					lFlowLeavingPinch);
			// Rule CP_out >= CP_in
			if (isCpRuleRespected(leavingFlowWithGreaterCPout, enteringFlowWithGreaterCPin)) {
				// We place a heat exchanger.

				HeatExchanger exchanger = getHeatExchanger(leavingFlowWithGreaterCPout,
						enteringFlowWithGreaterCPin);
				if (exchanger.getHeatPower() > minimalHeatExchangerPower) {
					internalHeatExchangerList.add(exchanger);

					lFlowEnteringPinch.remove(enteringFlowWithGreaterCPin);
					lFlowLeavingPinch.remove(leavingFlowWithGreaterCPout);

					getLogger().info("Loop " + countLoop + ", place exchanger: " + exchanger);

					// We update the leaving process set and the entering process set.
					if (Math.abs(exchanger.getHeatPower() - Math.abs(enteringFlowWithGreaterCPin.getPower())) > 1) {
						EnergyFlow newEnergyFlow = getUpdatedEnteringFlowAfterExchange(enteringFlowWithGreaterCPin,
								exchanger);
						lFlowEnteringPinch.add(newEnergyFlow);
						getLogger().info("Loop " + countLoop + ", rest of entering flow: " + newEnergyFlow);
					}
					if (Math.abs(exchanger.getHeatPower() - Math.abs(leavingFlowWithGreaterCPout.getPower())) > 1) {
						// The leaving process target temperature is not reached, a sub leaving energy
						// flow is created and added to the leaving flow set (we sort it again).
						EnergyFlow newEnergyFlow = getUpdatedLeavingFlowAfterExchange(leavingFlowWithGreaterCPout,
								exchanger);
						getLogger().info("Loop " + countLoop + ", rest of leaving flow: " + newEnergyFlow);
					}
				} else {
					getLogger()
							.warn("Cannot find a proper heat exchanger for, cannot find new consistent "
									+ "heat exchanger following the heuristic! "
									+ "Last try was " + exchanger);
					// Remove the fluid we cannot fulfill correctly.
					lFlowEnteringPinch.remove(enteringFlowWithGreaterCPin);
				}

			} else {
				getLogger().info("Loop " + countLoop + ", rule CP_out >= CP_in not respected for fluids "
						+ enteringFlowWithGreaterCPin.getId()
						+ " and " + leavingFlowWithGreaterCPout.getId() + ". "
						+ enteringFlowWithGreaterCPin.getId() + " is split!");
				splitEnteringProcess(leavingFlowWithGreaterCPout.getC(),
						enteringFlowWithGreaterCPin, lFlowEnteringPinch);
			}
			countLoop++;
		}
	}

	/**
	 *
	 * @param enteringFlow Flow that participate in the exchange.
	 * @param exchanger    The heat exchanger used.
	 * @return The remains of the entering flow.
	 */
	private EnergyFlow getUpdatedEnteringFlowAfterExchange(EnergyFlow enteringFlow, HeatExchanger exchanger) {
		// The entering process target temperature is not reached, a sub entering energy
		// flow is created and added to the entering flow set (we sort it again).
		boolean isHot = enteringFlow.getOwner().isHot();
		EnergyFlow newEnergyFlow = new EnergyFlow(enteringFlow.getOwner(),
				enteringFlow.getId() + "_re",
				isHot ? exchanger.getOutletTemperatureHot()
						: exchanger.getOutletTemperatureCold(),
				enteringFlow.getOutletTemperature(),
				enteringFlow.getC());
		return newEnergyFlow;
	}

	/**
	 *
	 * @param leavingFlow Flow that participate in the exchange.
	 * @param exchanger   The heat exchanger used.
	 * @return The remains of the leaving flow.
	 */
	private EnergyFlow getUpdatedLeavingFlowAfterExchange(EnergyFlow leavingFlow, HeatExchanger exchanger) {
		// The entering process target temperature is not reached, a sub entering energy
		// flow is created and added to the entering flow set (we sort it again).
		boolean isHot = leavingFlow.getOwner().isHot();
		EnergyFlow newEnergyFlow = new EnergyFlow(leavingFlow.getOwner(),
				leavingFlow.getId() + "_rl",
				isHot ? exchanger.getOutletTemperatureHot()
						: exchanger.getOutletTemperatureCold(),
				leavingFlow.getOutletTemperature(),
				leavingFlow.getC());
		return newEnergyFlow;
	}

	/**
	 * @return {@link #energyTargeting}.
	 */
	public EnergyTargetingAnalysis getEnergyTargeting() {
		return energyTargeting;
	}

	/**
	 * @return {@link #utilitySet}.
	 */
	public Set<EnergyUtility> getUtilitySet() {
		return new HashSet<>(utilitySet);
	}

	/**
	 *
	 * @return {@link #internalHeatExchangerList}.
	 */
	public List<HeatExchanger> getHeatExchangerList() {
		return new ArrayList<HeatExchanger>(internalHeatExchangerList);
	}

	/**
	 * @return {@link #hasComputed}.
	 */
	public boolean hasComputed() {
		return hasComputed;
	}

	/**
	 *
	 * @return {@link #logger}.
	 */
	private Logger getLogger() {
		return logger;
	}

}
