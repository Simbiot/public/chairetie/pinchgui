package pinchgui.pinch;

import pinchgui.app_ui.model.EnergyProcessData;
import pinchgui.app_ui.model.EnergyProcessEnthalpyData;

/**
 * Represents a process which is defined by its inlet and outlet enthalpy.
 *
 * @author tparis
 *
 */
public class EnergyProcessEnthalpy extends EnergyProcess {

	/**
	 * Default serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Enthalpy at the inlet of the process, in kJ/kg.
	 */
	private double inletH;

	/**
	 * Enthalpy at the outlet of the process, in kJ/kg.
	 */
	private double outletH;

	/**
	 * Constructor to be used with the {@link #importFromCsvLine(String, String)}
	 * method.
	 */
	protected EnergyProcessEnthalpy() {
		super();
	}

	/**
	 * Instantiate a process whose specific heat capacity depends linearly on the
	 * temperatures. Its value is estimated from the two enthalpies at inlet and
	 * outlet.
	 *
	 * @param aName
	 * @param aFluidName
	 * @param mflow
	 * @param inT
	 * @param outT
	 * @param inH
	 * @param outH
	 */
	public EnergyProcessEnthalpy(String aName, String aFluidName, double mflow, double inT, double outT, double inH,
			double outH) {
		super(aName, aFluidName, mflow, inT, outT);
		inletH = inH;
		outletH = outH;
	}

	@Override
	public double getInletEnthalpy() {
		return inletH;
	}

	@Override
	public double getOutletEnthalpy() {
		return outletH;
	}

	/**
	 *
	 * @param inH {@link #inletH}.
	 */
	protected void setInletEnthalpy(double inH) {
		inletH = inH;
	}

	/**
	 *
	 * @param outH {@link #outletH}.
	 */
	protected void setOutletEnthalpy(double outH) {
		outletH = outH;
	}

	@Override
	public double getC(double temperature) {
		return getMassFlow() * getCp(temperature);
	}

	@Override
	public double getC() {
		final double meanCp = (getOutletEnthalpy() - getInletEnthalpy()) / (getOutletT() - getInletT());
		return getMassFlow() * meanCp;
	}

	/**
	 *
	 * @param temperature Temperature of the fluid.
	 * @return Specific heat capacity computed from inlet and outlet temperatures
	 *         and enthalpies. Assumed to be linearly dependent on temperature.
	 */
	double getCp(double temperature) {
		// cp = a * temperature + b;
		final double inCp = inletH / getInletT();
		final double outCp = outletH / getOutletT();
		double a = (outCp - inCp) / (getOutletT() - getInletT());
		double b = inCp - (outCp - inCp) * getInletT() / (getOutletT() - getInletT());
		return a * temperature + b;
	}

	@Override
	public EnergyProcess getOffsetProcess(double offset) {
		return new EnergyProcessEnthalpy(getName(), getFluidName(), getMassFlow(), getInletT() + offset,
				getOutletT() + offset, getInletEnthalpy(), getOutletEnthalpy());
	}

	@Override
	public String exportCsvLine(String csvSeparator) {
		return getName() + csvSeparator + getFluidName() + csvSeparator + getInletT()
				+ csvSeparator + getOutletT() + csvSeparator + getInletEnthalpy() + csvSeparator
				+ getOutletEnthalpy() + csvSeparator + getMassFlow();
	}

	@Override
	public String exportCsvFirstLine(String csvSeparator) {
		return "Process id" + csvSeparator + " Fluid id" + csvSeparator
				+ " inlet temperature (Celsius)" + csvSeparator + " outlet temperature (Celsius)" + csvSeparator
				+ " inlet enthalpy (kJ/K)" + csvSeparator + " outlet enthalpy (kJ/K)" + csvSeparator
				+ "Mass flow (m/s)";
	}

	@Override
	public void importFromCsvLine(String csvLine, String csvSeparator) {
		final int indexProcessName = 0;
		final int indexFluidName = 1;
		final int indexInletTemperature = 2;
		final int indexOutletTemperature = 3;
		final int indexInletEnthapy = 4;
		final int indexOutletEnthalpy = 5;
		final int indexMassFlow = 6;
		String[] items = csvLine.split(csvSeparator);
		if (items.length > indexMassFlow) {
			setName(items[indexProcessName]);
			setFluidName(items[indexFluidName]);
			setInletT(Double.parseDouble(items[indexInletTemperature]));
			setInletEnthalpy(Double.parseDouble(items[indexInletEnthapy]));
			setOutletT(Double.parseDouble(items[indexOutletTemperature]));
			setOutletEnthalpy(Double.parseDouble(items[indexOutletEnthalpy]));
			setMassFlow(Double.parseDouble(items[indexMassFlow]));
		}
	}

	@Override
	public EnergyProcessData getProcessData() {
		return new EnergyProcessEnthalpyData(getName(), getInletT(), getOutletT(),
				getInletEnthalpy(), getOutletEnthalpy(),
				getMassFlow());
	}

	@Override
	public EnergyProcess clone() {
		return new EnergyProcessEnthalpy(getName(), getFluidName(), getMassFlow(), getInletT(), getOutletT(),
				getInletEnthalpy(), getOutletEnthalpy());
	}

}
