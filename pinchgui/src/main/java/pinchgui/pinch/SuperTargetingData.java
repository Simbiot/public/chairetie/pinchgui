package pinchgui.pinch;

import java.io.Serializable;

/**
 * Data for the super targeting analysis (link the minimal temperature
 * difference to HU, CU, number of heat exchangers, surface and costs).
 *
 * @author tparis
 *
 */
public class SuperTargetingData implements Serializable {

	/**
	 * Default serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal difference of temperature.
	 */
	private final double dTmin;

	/**
	 * Minimal heating energy requested for the system to work.
	 */
	private final double minimalHeatingEnergyRequested;

	/**
	 * Minimal cooling energy requested for the system to work.
	 */
	private final double minimalCoolingEnergyRequested;

	/**
	 * Estimated minimal number of heat exchanger.
	 */
	private final int estimatedNumberHe;

	/**
	 * Estimated total surface of heat exchanger requested.
	 */
	private final double estimatedTotalSurfaceHen;

	/**
	 * Estimated installation cost.
	 */
	private final double henInvestmentCost;

	/**
	 * Estimated operation cost.
	 */
	private final double henAnnualOperationCost;

	/**
	 * Estimated annual total cost.
	 */
	private final double henAnnualTotalCost;

	/**
	 * Worse annual operating cost, operating cost obtained if each process is
	 * handled by an utility without considering heat exchangers.
	 */
	private final double worstAnnualOperationCost;

	/**
	 * Estimated pay back period in month.
	 */
	private final double estimatedPayBackTimeInMonth;

	/**
	 *
	 * @param aDTmin                         {@link #dTmin}
	 * @param aMinimalHeatingEnergyRequested {@link #minimalHeatingEnergyRequested}
	 * @param aMinimalCoolingEnergyRequested {@link #minimalCoolingEnergyRequested}
	 * @param aEstimatedNumberHe             {@link #estimatedNumberHe}
	 * @param aEstimatedTotalSurfaceHen      {@link #estimatedTotalSurfaceHen}
	 * @param aHenInvestmentCost             {@link #henInvestmentCost}
	 * @param aHenAnnualOperationCost        {@link #henAnnualOperationCost}
	 * @param aHenAnnualTotalCost            {@link #henAnnualTotalCost}
	 * @param aWortAnnualOperationCost       {@link #worstAnnualOperationCost}
	 * @param aPayBackTimeInMonth            {@link #estimatedPayBackTimeInMonth}
	 */
	public SuperTargetingData(double aDTmin, double aMinimalHeatingEnergyRequested,
			double aMinimalCoolingEnergyRequested,
			int aEstimatedNumberHe, double aEstimatedTotalSurfaceHen, double aHenInvestmentCost,
			double aHenAnnualOperationCost, double aHenAnnualTotalCost,
			double aWortAnnualOperationCost, double aPayBackTimeInMonth) {
		super();
		dTmin = aDTmin;
		minimalHeatingEnergyRequested = aMinimalHeatingEnergyRequested;
		minimalCoolingEnergyRequested = aMinimalCoolingEnergyRequested;
		estimatedNumberHe = aEstimatedNumberHe;
		estimatedTotalSurfaceHen = aEstimatedTotalSurfaceHen;
		henInvestmentCost = aHenInvestmentCost;
		henAnnualOperationCost = aHenAnnualOperationCost;
		henAnnualTotalCost = aHenAnnualTotalCost;
		worstAnnualOperationCost = aWortAnnualOperationCost;
		estimatedPayBackTimeInMonth = aPayBackTimeInMonth;
	}

	/**
	 * @return the {@link #dTmin}
	 */
	public double getdTmin() {
		return dTmin;
	}

	/**
	 * @return the {@link #minimalHeatingEnergyRequested}
	 */
	public double getMinimalHeatingEnergyRequested() {
		return minimalHeatingEnergyRequested;
	}

	/**
	 * @return the {@link #minimalCoolingEnergyRequested}
	 */
	public double getMinimalCoolingEnergyRequested() {
		return minimalCoolingEnergyRequested;
	}

	/**
	 * @return the {@link #estimatedNumberHe}
	 */
	public int getEstimatedNumberHe() {
		return estimatedNumberHe;
	}

	/**
	 * @return the {@link #estimatedTotalSurfaceHen}
	 */
	public double getEstimatedTotalSurfaceHen() {
		return estimatedTotalSurfaceHen;
	}

	/**
	 * @return the {@link #henInvestmentCost}
	 */
	public double getHenInvestmentCost() {
		return henInvestmentCost;
	}

	/**
	 * @return the {@link #henAnnualOperationCost}
	 */
	public double getHenAnnualOperationCost() {
		return henAnnualOperationCost;
	}

	/**
	 * @return the {@link #henAnnualTotalCost}
	 */
	public double getHenAnnualTotalCost() {
		return henAnnualTotalCost;
	}

	/**
	 * @return the {@link #worstAnnualOperationCost}
	 */
	public double getWorstAnnualOperationCost() {
		return worstAnnualOperationCost;
	}

	/**
	 * @return the {@link #estimatedPayBackTimeInMonth}
	 */
	public double getEstimatedPayBackTimeInMonth() {
		return estimatedPayBackTimeInMonth;
	}

	@Override
	public String toString() {
		return "SuperTargeting data : " + "\n"
				+ "DTmin " + dTmin + "\n"
				+ "Minimal Heating Energy Required " + minimalHeatingEnergyRequested + "\n"
				+ "Minimal Cooling Energy Required " + minimalCoolingEnergyRequested + "\n"
				+ "Estimated number of heat exchangers " + estimatedNumberHe + "\n"
				+ "Estimated total heat exchanger network total surface " + estimatedTotalSurfaceHen + "\n"
				+ "Investment cost " + henInvestmentCost + "\n"
				+ "Annual operationnal cost " + henAnnualOperationCost + "\n"
				+ "Annual total cost " + henAnnualTotalCost + "\n"
				+ "Worst annual operating cost (reference) " + worstAnnualOperationCost + "\n"
				+ "Estimated payback period in month  " + estimatedPayBackTimeInMonth + "\n";
	}

}
