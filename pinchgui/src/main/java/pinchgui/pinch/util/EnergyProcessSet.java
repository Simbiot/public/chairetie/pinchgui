package pinchgui.pinch.util;

import java.util.Set;

import pinchgui.pinch.EnergyProcess;

/**
 * Utility method for set of {@link EnergyProcess}
 *
 * @author tparis
 *
 */
public final class EnergyProcessSet {

	/**
	 *
	 * @param processSet
	 * @return The maximal temperature to reach regarding all cold processes.
	 */
	public static double maximalColdProcessTemperature(Set<EnergyProcess> processSet) {
		double max = Double.NEGATIVE_INFINITY;
		for (EnergyProcess p : processSet) {
			if (p.isCold() && p.getOutletT() > max) {
				max = p.getOutletT();
			}
		}
		return max;
	}

	/**
	 *
	 * @param processSet
	 * @return The minimal temperature to reach regarding all hot processes.
	 */
	public static double minimalHotProcessTemperature(Set<EnergyProcess> processSet) {
		double min = Double.POSITIVE_INFINITY;
		for (EnergyProcess p : processSet) {
			if (p.isHot() && p.getOutletT() < min) {
				min = p.getOutletT();
			}
		}
		return min;
	}

	/**
	 *
	 * @param processSet
	 * @return Sum of energy required for each hot process that needs to be cooled
	 *         down.
	 */
	public static double sumHotProcessRequiredEnergy(Set<EnergyProcess> processSet) {
		double sum = 0.;
		for (EnergyProcess p : processSet) {
			if (p.isHot()) {
				sum += p.getDeltaQ();
			}
		}
		return sum;
	}

	/**
	 *
	 * @param processSet
	 * @return Sum of energy required for each cold process that needs to be heated.
	 */
	public static double sumColdProcessRequiredEnergy(Set<EnergyProcess> processSet) {
		double sum = 0.;
		for (EnergyProcess p : processSet) {
			if (p.isCold()) {
				sum -= p.getDeltaQ();
			}
		}
		return sum;
	}

	private EnergyProcessSet() {
	}

}
