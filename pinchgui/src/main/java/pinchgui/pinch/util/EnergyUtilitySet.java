package pinchgui.pinch.util;

import java.util.HashSet;
import java.util.Set;

import pinchgui.pinch.EnergyUtility;

/**
 * Utility class for energy utility set.
 *
 * @author tparis
 *
 */
public class EnergyUtilitySet {

	/**
	 * @param utilitySet Utility set to browse.
	 *
	 * @return Set of cold utilities.
	 */
	public static Set<EnergyUtility> getColdUtilities(Set<EnergyUtility> utilitySet) {
		Set<EnergyUtility> coldUtilities = new HashSet<>();
		for (EnergyUtility utility : utilitySet) {
			if (utility.isCold()) {
				coldUtilities.add(utility);
			}
		}
		return coldUtilities;
	}

	/**
	 * @param utilitySet Utility set to browse.
	 *
	 * @return Set of hot utilities.
	 */
	public static Set<EnergyUtility> getHotUtilities(Set<EnergyUtility> utilitySet) {
		Set<EnergyUtility> hotUtilities = new HashSet<>();
		for (EnergyUtility utility : utilitySet) {
			if (utility.isHot()) {
				hotUtilities.add(utility);
			}
		}
		return hotUtilities;
	}

	/**
	 * @param utilitySet Utility set to browse.
	 *
	 * @return The maximum cost of heating fluid. Used to estimate annual operation
	 *         cost.
	 */
	public static double getMaxHeatingCost(Set<EnergyUtility> utilitySet) {
		double maxCost = 0.;
		for (EnergyUtility utility : utilitySet) {
			if (utility.isHot() && utility.getCost() > maxCost) {
				maxCost = utility.getCost();
			}
		}
		return maxCost;
	}

	/**
	 * @param utilitySet Utility set to browse.
	 *
	 * @return The maximum cost of cooling fluid. Used to estimate annual operation
	 *         cost.
	 */
	public static double getMaxCoolingCost(Set<EnergyUtility> utilitySet) {
		double maxCost = 0.;
		for (EnergyUtility utility : utilitySet) {
			if (utility.isCold() && utility.getCost() > maxCost) {
				maxCost = utility.getCost();
			}
		}
		return maxCost;
	}

}
