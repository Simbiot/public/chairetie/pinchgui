package pinchgui.pinch;

/**
 * Comparator that compares two {@link EnergyFlow} regarding the distance
 * between their inlet temperature and the reference, then by heat capacity
 * flow, then by id.
 *
 * @author tparis
 *
 */
public class EnergyFlowComparatorTin extends EnergyFlowComparator {

	/**
	 * New comparator that compares two {@link EnergyFlow} regarding the distance
	 * between their outlet temperature and the reference, then by heat capacity
	 * flow, then by absolute power, then by id.
	 *
	 * @param pinchT
	 */
	public EnergyFlowComparatorTin(double pinchT) {
		super(pinchT);
	}

	@Override
	public int compare(EnergyFlow o1, EnergyFlow o2) {
		if (o1.getInletTemperature() != o2.getInletTemperature()) {
			return (Math.abs(o1.getInletTemperature() - getReference())
					- Math.abs(o2.getInletTemperature() - getReference())) > 0
							? -1
							: 1;
		}
		if (o1.getC() != o2.getC()) {
			return (o1.getC() - o2.getC()) > 0 ? 1 : -1;
		}
		if (o1.getPower() != o2.getPower()) {
			return (Math.abs(o1.getPower()) - Math.abs(o2.getPower())) > 0 ? 1 : -1;
		}
		return o1.getId().compareTo(o2.getId());
	}
}
