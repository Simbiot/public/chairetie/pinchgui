package pinchgui.pinch;

/**
 * Represents an exchanger placed between two {@link EnergyProcess} to exchange
 * heat power.
 *
 * @author tparis
 *
 */
public final class HeatExchanger {

	/**
	 * Hot flow involved in the heat exchange.
	 */
	private EnergyFlow hotFlow;

	/**
	 * Temperature at the inlet of the heat exchanger for the hot process.
	 */
	private double inletTemperatureHot;

	/**
	 * Temperature at the inlet of the heat exchanger for the hot process.
	 */
	private double outletTemperatureHot;

	/**
	 * Cold flow involved in the heat exchange.
	 */
	private EnergyFlow coldFlow;

	/**
	 * Temperature at the inlet of the heat exchanger for the cold process.
	 */
	private double inletTemperatureCold;

	/**
	 * Temperature at the inlet of the heat exchanger for the cold process.
	 */
	private double outletTemperatureCold;

	/**
	 * Power exchanged in this heat exchanger (>0) in kW.
	 */
	private double heatPower;

	/**
	 *
	 * @param theHotFlow  {@link #hotFlow}.
	 * @param hotInT      {@link #inletTemperatureHot}.
	 * @param hotOutT     {@link #outletTemperatureHot}.
	 * @param theColdFlow {@link #coldFlow}.
	 * @param coldInT     {@link #inletTemperatureCold}.
	 * @param coldOutT    {@link #outletTemperatureCold}.
	 */
	private HeatExchanger(EnergyFlow theHotFlow, double hotInT, double hotOutT, EnergyFlow theColdFlow,
			double coldInT, double coldOutT) {
		hotFlow = theHotFlow;
		inletTemperatureHot = hotInT;
		outletTemperatureHot = hotOutT;
		coldFlow = theColdFlow;
		inletTemperatureCold = coldInT;
		outletTemperatureCold = coldOutT;
		heatPower = (coldOutT - coldInT) * theColdFlow.getC();
	}

	/**
	 *
	 * @return {@link #hotFlow}.
	 */
	public EnergyFlow getHotFlow() {
		return hotFlow;
	}

	/**
	 *
	 * @return {@link #inletTemperatureHot}.
	 */
	public double getInletTemperatureHot() {
		return inletTemperatureHot;
	}

	/**
	 *
	 * @return {@link #outletTemperatureHot}.
	 */
	public double getOutletTemperatureHot() {
		return outletTemperatureHot;
	}

	/**
	 *
	 * @return {@link #coldFlow}.
	 */
	public EnergyFlow getColdFlow() {
		return coldFlow;
	}

	/**
	 *
	 * @return {@link #inletTemperatureCold}.
	 */
	public double getInletTemperatureCold() {
		return inletTemperatureCold;
	}

	/**
	 *
	 * @return {@link #outletTemperatureCold}.
	 */
	public double getOutletTemperatureCold() {
		return outletTemperatureCold;
	}

	/**
	 *
	 * @return {@link #heatPower}.
	 */
	public double getHeatPower() {
		return heatPower;
	}

	/**
	 *
	 * @param theHotFlow  {@link #hotFlow}.
	 * @param hotInT      {@link #inletTemperatureHot}.
	 * @param hotOutT     {@link #outletTemperatureHot}.
	 * @param theColdFlow {@link #coldFlow}.
	 * @param coldInT     {@link #inletTemperatureCold}.
	 * @return An heat exchanger, compute automatically the outlet temperature of
	 *         the cold process {@link #outletTemperatureCold} from other value.
	 */
	public static HeatExchanger getHeatExchangerComputeColdOut(EnergyFlow theHotFlow, double hotInT, double hotOutT,
			EnergyFlow theColdFlow,
			double coldInT) {
		double hotHeatPower = (hotInT - hotOutT) * theHotFlow.getC();
		double coldOutT = coldInT + hotHeatPower / theColdFlow.getC();
		return new HeatExchanger(theHotFlow, hotInT, hotOutT, theColdFlow,
				coldInT, coldOutT);
	}

	/**
	 *
	 * @param theHotFlow  {@link #hotFlow}.
	 * @param hotInT      {@link #inletTemperatureHot}.
	 * @param hotOutT     {@link #outletTemperatureHot}.
	 * @param theColdFlow {@link #coldFlow}.
	 * @param coldOutT    {@link #outletTemperatureCold}.
	 * @return An heat exchanger, compute automatically the inlet temperature of the
	 *         cold process {@link #inletTemperatureCold} from other value.
	 */
	public static HeatExchanger getHeatExchangerComputeColdIn(EnergyFlow theHotFlow, double hotInT, double hotOutT,
			EnergyFlow theColdFlow,
			double coldOutT) {
		double hotHeatPower = (hotInT - hotOutT) * theHotFlow.getC();
		double coldInT = coldOutT - hotHeatPower / theColdFlow.getC();
		return new HeatExchanger(theHotFlow, hotInT, hotOutT, theColdFlow,
				coldInT, coldOutT);
	}

	/**
	 *
	 * @param theHotFlow  {@link #hotFlow}.
	 * @param hotInT      {@link #inletTemperatureHot}.
	 * @param theColdFlow {@link #coldFlow}.
	 * @param coldInT     {@link #inletTemperatureCold}.
	 * @param coldOutT    {@link #outletTemperatureCold}.
	 * @return An heat exchanger, compute automatically the outlet temperature of
	 *         the hot process {@link #outletTemperatureHot} from other value.
	 */
	public static HeatExchanger getHeatExchangerComputeHotOut(EnergyFlow theHotFlow, double hotInT,
			EnergyFlow theColdFlow,
			double coldInT, double coldOutT) {
		double coldHeatPower = (coldOutT - coldInT) * theColdFlow.getC();
		double hotOutT = hotInT - coldHeatPower / theHotFlow.getC();
		return new HeatExchanger(theHotFlow, hotInT, hotOutT, theColdFlow,
				coldInT, coldOutT);
	}

	/**
	 *
	 * @param theHotFlow  {@link #hotFlow}.
	 * @param hotOutT     {@link #outletTemperatureHot}.
	 * @param theColdFlow {@link #coldFlow}.
	 * @param coldInT     {@link #inletTemperatureCold}.
	 * @param coldOutT    {@link #outletTemperatureCold}.
	 * @return An heat exchanger, compute automatically the inlet temperature of the
	 *         hot process {@link #inletTemperatureHot} from other value.
	 */
	public static HeatExchanger getHeatExchangerComputeHotIn(EnergyFlow theHotFlow, double hotOutT,
			EnergyFlow theColdFlow,
			double coldInT, double coldOutT) {
		double coldHeatPower = (coldOutT - coldInT) * theColdFlow.getC();
		double hotInT = hotOutT + coldHeatPower / theHotFlow.getC();
		return new HeatExchanger(theHotFlow, hotInT, hotOutT, theColdFlow,
				coldInT, coldOutT);
	}

	/**
	 *
	 * @param theHotFlow
	 * @param theColdFlow
	 * @return The heat exchanger that maximize the heat exchanged between these two
	 *         flows considering a minimum delta of temperature of 0.
	 */
	public static HeatExchanger getHeatExchanger(EnergyFlow theHotFlow,
			EnergyFlow theColdFlow) {
		double exchangedPower = potentialExchangeablePower(theHotFlow, theColdFlow, 0.);
		double hotOutT = theHotFlow.getInletTemperature() - exchangedPower / theHotFlow.getC();
		double coldOutT = theColdFlow.getInletTemperature() + exchangedPower / theColdFlow.getC();
		return new HeatExchanger(theHotFlow, theHotFlow.getInletTemperature(), hotOutT, theColdFlow,
				theColdFlow.getInletTemperature(), coldOutT);
	}

	/**
	 *
	 * @param dT Minimal difference of temperature wanted.
	 * @return True if the difference is ensured in the heat exchanger, false if
	 *         not.
	 */
	public boolean validateTemperatureDifference(final double dT) {
		return outletTemperatureHot - inletTemperatureCold > dT && inletTemperatureHot - outletTemperatureCold > dT;
	}

	/**
	 *
	 * @param leavingFlow
	 * @param enteringFlow
	 * @return True if CP of leaving flow if superior to CP of entering flow (CP
	 *         rule).
	 */
	public static boolean isCpRuleRespected(EnergyFlow leavingFlow, EnergyFlow enteringFlow) {
		return leavingFlow.getC() >= enteringFlow.getC();
	}

	/**
	 *
	 * @param leavingFlow
	 * @param enteringFlow
	 * @param dTmin        Minimal difference of temperature allowed.
	 * @return Check the flow and give the exchangeable power that is compliant with
	 *         a minimal difference of temperature.
	 */
	public static double potentialExchangeablePower(EnergyFlow leavingFlow,
			EnergyFlow enteringFlow, final double dTmin) {
		double power = 0;
		if (leavingFlow.getOwner().isHot()) {
			power = Math.min(
					Math.abs(leavingFlow.getPower(
							leavingFlow.getInletTemperature(),
							Math.max(leavingFlow.getOutletTemperature(),
									enteringFlow.getInletTemperature() - dTmin))),
					Math.abs(enteringFlow.getPower(
							enteringFlow.getInletTemperature(),
							Math.min(enteringFlow.getOutletTemperature(),
									leavingFlow.getInletTemperature() + dTmin))));
		} else {
			power = Math.min(
					Math.abs(leavingFlow.getPower(
							leavingFlow.getInletTemperature(),
							Math.min(leavingFlow.getOutletTemperature(),
									enteringFlow.getInletTemperature() + dTmin))),
					Math.abs(enteringFlow.getPower(
							enteringFlow.getInletTemperature(),
							Math.max(enteringFlow.getOutletTemperature(),
									leavingFlow.getInletTemperature() - dTmin))));
		}

		return power;
	}

	/**
	 *
	 * @param aQdot      Power to be exchanged by the heat exchanger.
	 * @param deltaTmean Mean temperature difference in the heat exchanger.
	 * @param kCoeff     Exchange coefficient of the heat exchanger.
	 * @return Estimation of the surface of the heat exchanger.
	 */
	public static double estimateSurface(final double aQdot, final double deltaTmean, final double kCoeff) {
		return aQdot / (kCoeff * deltaTmean);
	}

	/**
	 *
	 * @return Logarithmic mean temperature difference for this heat exchanger.
	 */
	public double meanLogarithmicTemperatureDifference() {
		final double dTx = outletTemperatureHot - inletTemperatureCold;
		final double dTy = inletTemperatureHot - outletTemperatureCold;
		return (dTx - dTy) / Math.log(dTx / dTy);
	}

	@Override
	public String toString() {
		return "Heat exchanger from process " + hotFlow.getId() + " " + inletTemperatureHot + "->"
				+ outletTemperatureHot + " to " + coldFlow.getId() + " " + inletTemperatureCold + "->"
				+ outletTemperatureCold + " for a heat power " + heatPower + "kW";
	}

}
