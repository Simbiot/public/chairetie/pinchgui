package pinchgui.app_ui.util;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

public class HelpLabel {

	/**
	 * Create and return a label with a tooltip.
	 *
	 * @param text Text of the label
	 * @param tip  Text of the tooltip
	 * @return A label
	 */
	public static Label getLabelWithTooltip(String text, String tip) {
		Label label = new Label(text);
		Tooltip t = new Tooltip(tip);
		final int toolTipShowDelay = 50;
		t.setShowDelay(new Duration(toolTipShowDelay));
		label.setTooltip(t);
		return label;
	}

	/**
	 *
	 * @param textfield {@link TextField} to be initialized as a text which is
	 *                  supposed to contain positive numbers.
	 */
	public static void initializeTextFieldPositiveNumber(TextField textfield) {
		textfield.focusedProperty().addListener((ov, oldV, newV) -> {
			if (!newV) {
				// Focus is lost.
				// Get the text and remove all spaces.
				final String txt = textfield.getText().strip();
				if (!txt.matches("\\d+(\\.\\d*)?")) {
					// Wrong value we cancel choice.
					textfield.setText("");
					return;
				}
				textfield.setText("" + Double.parseDouble(txt));
			}
		});
		textfield.setFocusTraversable(true);
	}

}
