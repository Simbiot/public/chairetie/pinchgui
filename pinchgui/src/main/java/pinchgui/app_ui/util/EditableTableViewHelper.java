package pinchgui.app_ui.util;

import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;

public class EditableTableViewHelper<T> {

	/**
	 *
	 * @param tab
	 */
	@SuppressWarnings("unchecked")
	public void editFocusedCell(TableView<T> tab) {
		final TablePosition<T, ?> focusedCell = tab.getFocusModel().getFocusedCell();
		tab.edit(focusedCell.getRow(), focusedCell.getTableColumn());
	}

	/**
	 * Make editable the table in parameter, set actions to certain keys and set
	 * user-written text in "currentWord" attribute.
	 *
	 * @param tab Tab to make editable.
	 */
	public void editableColumns(TableView<T> tab) {
		tab.setEditable(true);
		tab.getSelectionModel().setCellSelectionEnabled(true);
		tab.setOnMouseClicked(event -> {
			editFocusedCell(tab);
		});
		tab.setOnKeyPressed(event -> {
			if (event.getCode().isLetterKey() || event.getCode().isDigitKey()) {
				editFocusedCell(tab);
			} else if (event.getCode() == KeyCode.ENTER) {
				tab.getSelectionModel().selectNext();
				event.consume();
			}
		});
	}

}
