package pinchgui.app_ui.util;

import java.io.File;

import pinchgui.app_ui.main.App;
import pinchgui.app_ui.model.EnergyProcessEnthalpyData;

/**
 * Utility classes with methods common to Table controller.
 *
 * @author tparis
 *
 */
public final class TableControllerUtils {

	/**
	 * Extension use by application to save and load data.
	 */
	public static final String EXTENSION = "csv";

	/**
	 * Path to default folder where data are saved.
	 */
	public static final String SAVE_PATH = ".";

	/**
	 * Maximal temperature allowed for the minimal delta of temperature.
	 */
	public static final double MAXIMAL_TEMPERATURE = 100.;

	/**
	 * Prevent instanciation.
	 */
	private TableControllerUtils() {
	}

	/**
	 * Export the streams table in a csv file.
	 *
	 * @param main The application.
	 */
	public static void exportCsvFile(App main) {
		File f = main.getData().fileChooser("tabSave", true, "csvFile", "defaultTabFileName", EXTENSION, SAVE_PATH,
				main.getStage());
		if (f != null) {
			main.getData().exportTabCsv(f.getAbsolutePath());
		}
	}

	/**
	 * Import a csv file in the streams table.
	 *
	 * @param main The application.
	 */
	public static void importCsvFile(App main) {
		File f = main.getData().fileChooser("tabLoad", false, "csvFile", "defaultTabFileName", EXTENSION, SAVE_PATH,
				main.getStage());
		if (f != null) {
			main.getData().importTabCsv(f.getAbsolutePath());

			// Refresh tab type.
			if (main.getData().getStreams() != null && main.getData().getStreams().size() >= 1
					&& main.getData().getStreams().get(0) instanceof EnergyProcessEnthalpyData) {
				main.getData().setEnthalpyMode(true);
			} else {
				main.getData().setEnthalpyMode(false);
			}
			main.loadNewView(1);
		}

	}

}
