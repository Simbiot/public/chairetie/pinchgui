package pinchgui.app_ui.internationalisation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import pinchgui.utils.Utils;

/**
 * Class that allows internationalization (i18n) of the application. This class
 * is limited to one instantiation (Singleton).
 *
 */
public final class I18n {

	/**
	 * Logger id.
	 */
	private static final String CONCEPT_ID = "language.loader";

	/**
	 * Single instance of the class.
	 */
	private static I18n instance;

	/**
	 * Table of all languages supported by application.
	 */
	private static final String[] ALL_LANGUAGES = {"fr", "en" };

	/**
	 * Current language.
	 */
	private String language;

	/**
	 * Parent node of the language xml document.
	 */
	private Element root;

	/**
	 * Path to the folder which contains the language XML files.
	 */
	private static final String PATHTOLANGUAGEFOLDER = "languages/";

	/**
	 * Logger of this class.
	 */
	private final Logger logger;

	/**
	 * Private constructor of the class. Language fr by default.
	 */
	private I18n() {
		language = "fr";
		logger = LoggerFactory.getLogger(CONCEPT_ID);
		loadLanguage();
	}

	/**
	 * Return a single instance of the class. Create a new instance if it is null.
	 *
	 * @return a single instance of the class.
	 */
	public static synchronized I18n getInstance() {
		if (instance == null) {
			instance = new I18n();
		}
		return instance;
	}

	/**
	 *
	 * @return Id of current language.
	 */
	public String getLg() {
		return language;
	}

	/**
	 * Load the current language from the language xml document.
	 */
	private void loadLanguage() {
		File fileXML = new File(Utils.extractResourcesIntoTemporaryFile(
				PATHTOLANGUAGEFOLDER + language + "/" + "strings.xml", "strings", ".xml"));
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			InputStream is = new FileInputStream(fileXML);
			Document xml = builder.parse(is);
			root = xml.getDocumentElement();

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Search and return the text content of the wanted tag.
	 *
	 * @param location Parent XML tag of the content.
	 * @param name     Name of Text tag attribute.
	 * @return The text content of the found tag.
	 */
	public String get(String location, String name) {

		// Expression to find text content
		String expression = "//" + location + "/text[@name='" + name + "']";
		XPath path = XPathFactory.newInstance().newXPath();
		Node node = null;
		String msg = "";
		try {
			node = (Node) path.evaluate(expression, root, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (node != null) {
			// Remove extra tab.
			msg = node.getTextContent().replaceAll("(\t)*", "");
		} else {
			logger.error("Cannot find text: " + location + "/" + name);
		}
		return msg;
	}

	/**
	 * Search and return the text content of the wanted tag.
	 *
	 * @param location Parent XML tag of the content.
	 * @param name     Name of Text tag attribute.
	 * @return True if the tag is found, false if not.
	 */
	public boolean check(String location, String name) {
		// Expression to find text content
		String expression = "//" + location + "/text[@name='" + name + "']";
		XPath path = XPathFactory.newInstance().newXPath();
		try {
			Node node = (Node) path.evaluate(expression, root, XPathConstants.NODE);
			if (node != null) {
				return true;
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Change the current language.
	 *
	 * @param i Index of the new language in the table of all languages.
	 */
	public void changeLanguage(int i) {
		if (i < ALL_LANGUAGES.length && i > -1 && ALL_LANGUAGES[i] != language) {
			language = ALL_LANGUAGES[i];
			loadLanguage();
		}
	}

	/**
	 *
	 * @return Table of all supported languages.
	 */
	public static String[] getAllLanguages() {
		return ALL_LANGUAGES;
	}

}
