package pinchgui.app_ui.main;

/**
 * Main class that doesn't extend Application.
 *
 */
public final class Main {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		App.main(args);
	}

	private Main() {

	}
}
