package pinchgui.app_ui.main;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pinchgui.app_ui.model.AppData;
import pinchgui.app_ui.view.Controller;
import pinchgui.app_ui.view.HelpController;
import pinchgui.app_ui.view.HomeController;
import pinchgui.app_ui.view.MenuController;

/**
 * JavaFX App
 */
public class App extends Application {

	/**
	 * The main stage (equivalent to a JFrame).
	 */
	private Stage mainStage;

	/**
	 * The main container (BorderPane).
	 */
	private BorderPane mainContainer;

	/**
	 * The streams table container
	 */
	private SplitPane streamsContainer;

	/**
	 * The main controller used (menu controller).
	 */
	private MenuController mainController;

	/**
	 * The current controller used.
	 */
	private Controller currentController;

	/**
	 * The current graph controller used.
	 */
	private Controller currentGraphController;

	/**
	 * Data of current execution of the application.
	 */
	private AppData data;

	/**
	 * Path to FXML files.
	 */
	private static final String PATHTOFXML = "/pinchgui/app_ui/view";

	/**
	 * Last position of the separator between the table pane and the graphics pane.
	 */
	public double currentTableWidth;

	/**
	 * Method called at application launch. Create and show the main window.
	 */
	@Override
	public void start(Stage primaryStage) {
		mainStage = primaryStage;
		mainStage.setTitle("Analyse Pinch");
		currentTableWidth = 300;
		data = new AppData();

		initMainContainer();
		initHomeContent();
	}

	/**
	 * Create a frame with the main container. The main container is a BorderPane
	 * with the menu at left.
	 */
	private void initMainContainer() {
		// Create FXML loader
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource(PATHTOFXML + "/MenuView.fxml"));

		try {
			// Load the Main view, which is a BorderPane
			mainContainer = (BorderPane) loader.load();
			Scene scene = new Scene(mainContainer);

			String css = this.getClass().getResource(PATHTOFXML + "/css/style.css").toExternalForm();
			scene.getStylesheets().add(css);

			mainStage.setScene(scene);

			// Set controller
			mainController = loader.getController();
			mainController.setMainApp(this);

			// Show the main stage, which is the frame
			mainStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load the home view in a container (AnchorPane) and set this container to the
	 * center of the main container (BorderPane)
	 */
	private void initHomeContent() {
		// Create FXML loader
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource(PATHTOFXML + "/HomeView.fxml"));

		try {
			// Load Home view, which is an AnchorPane
			AnchorPane homeContainer = (AnchorPane) loader.load();
			// Set homeContainer (AnchorPane) to the center of mainContainer (BorderPane)
			mainContainer.setCenter(homeContainer);

			// Set controller
			HomeController home = loader.getController();
			home.setMainApp(this);
			currentController = home;
			mainController.setBackgroud(0);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load the four graphs view in a container (AnchorPane) and set this container
	 * to the center of the streams table container (BorderPane)
	 */
	private void initGraphContent(String fxmlName) {
		// Create FXML loader
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource(PATHTOFXML + "/" + fxmlName + ".fxml"));

		try {
			// Load Home view, which is an AnchorPane
			AnchorPane graphContainer = (AnchorPane) loader.load();

			streamsContainer.getItems().remove(1);
			streamsContainer.getItems().add(graphContainer);

			// Set graph controller
			Controller graph = loader.getController();
			graph.setMainApp(this);
			currentGraphController = graph;

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load the content from this FXML file name to be place on the left side.
	 *
	 * @param fxmlName
	 */
	private void initParameterContent(String fxmlName, int backGroundIdentifier) {
		// Create FXML loader
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource(PATHTOFXML + "/" + fxmlName + ".fxml"));

		try {
			// Load Home view, which is an AnchorPane
			streamsContainer = (SplitPane) loader.load();

			// Set homeContainer (AnchorPane) to the center of mainContainer (BorderPane)
			mainContainer.setCenter(streamsContainer);

			// Set controller
			Controller stream = loader.getController();
			stream.setMainApp(this);
			currentController = stream;
			mainController.setBackgroud(backGroundIdentifier);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Remove Graph container and his controller.
	 */
	public void removeGraph() {
		streamsContainer.getItems().remove(1);
		streamsContainer.getItems().add(new AnchorPane());
		currentGraphController = null;
	}

	/**
	 * Load the help view in a container (AnchorPane) and set this container to the
	 * center of the main container (BorderPane)
	 */
	private void initHelpContent() {
		// Create FXML loader
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(App.class.getResource(PATHTOFXML + "/HelpView.fxml"));

		try {
			// Load Home view, which is an AnchorPane
			AnchorPane helpContainer = (AnchorPane) loader.load();
			// Set homeContainer (AnchorPane) to the center of mainContainer (BorderPane)
			mainContainer.setCenter(helpContainer);

			// Set controller
			HelpController help = loader.getController();
			help.setMainApp(this);
			currentController = help;
			mainController.setBackgroud(2);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Change and load a view in the main container, depending of i value : 0 and
	 * default : Home; 1 : Stream table and graph(s); 2 : Help view; 3 : Four graphs
	 * view; 4 : Single graph view, 5 : Four cost graph, 6 : 1 cost graph view.
	 *
	 * @param i Number of the view to load
	 */
	public void loadNewView(int i) {
		if (streamsContainer != null && i > 3 && i < 7) {
			currentTableWidth = streamsContainer.getDividerPositions()[0];
		}

		switch (i) {
		case 1:
			final boolean enthalpyMode = getData().getEnthalpyMode();
			if (!enthalpyMode) {
				initParameterContent("StreamHeatCapacityTableView", i);
			} else {
				initParameterContent("StreamEnthalpyTableView", i);
			}
			if (streamsContainer != null) {
				streamsContainer.setDividerPosition(0, currentTableWidth);
			}
			break;
		case 2:
			initHelpContent();
			break;
		case 3:
			initGraphContent("Graph4View");
			streamsContainer.setDividerPosition(0, currentTableWidth);
			break;
		case 4:
			initGraphContent("Graph1View");
			streamsContainer.setDividerPosition(0, currentTableWidth);
			break;
		case 5:
			initParameterContent("SuperTargetingView", i);
			if (streamsContainer != null) {
				streamsContainer.setDividerPosition(0, currentTableWidth);
			}
			break;
		case 6:
			initGraphContent("CostGraph4View");
			streamsContainer.setDividerPosition(0, currentTableWidth);
			break;
		case 7:
			initGraphContent("CostGraph1View");
			streamsContainer.setDividerPosition(0, currentTableWidth);
			break;
		case 8:
			initParameterContent("HenDesignView", i);
			if (streamsContainer != null) {
				streamsContainer.setDividerPosition(0, currentTableWidth);
			}
			break;
		case 9:
			initGraphContent("HenResultView");
			if (streamsContainer != null) {
				streamsContainer.setDividerPosition(0, currentTableWidth);
			}
			break;
		default:
			initHomeContent();
			break;
		}

	}

	/**
	 *
	 * @return The main stage.
	 */
	public Stage getStage() {
		return mainStage;
	}

	/**
	 *
	 * @return Data of current execution of the application
	 */
	public AppData getData() {
		return data;
	}

	/**
	 * Reload all the application text with the current language.
	 */
	public void loadNewLanguage() {
		currentController.loadLanguage();
		if (currentGraphController != null) {
			currentGraphController.loadLanguage();
		}
	}

	/**
	 * Run the application
	 *
	 * @param args Not used
	 */
	public static void main(String[] args) {
		launch(args);
	}

}