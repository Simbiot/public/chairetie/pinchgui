package pinchgui.app_ui.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.EnergyProcessHeatCapacity;

/**
 * This class models a stream.
 *
 */
public class EnergyProcessHeatCapacityData extends EnergyProcessData {

	/**
	 * Default serial IUD.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Heat capacity of the stream.
	 */
	private SimpleDoubleProperty heatCapacityProperty;

	/**
	 * Type of the stream.
	 */
	private SimpleStringProperty typeProperty;

	/**
	 * Default constructor of the class.
	 */
	public EnergyProcessHeatCapacityData() {
		super();
		heatCapacityProperty = new SimpleDoubleProperty(0.);
		typeProperty = new SimpleStringProperty();
		updateType();
	}

	/**
	 * Constructor with parameters.
	 *
	 * @param name Name of the stream.
	 * @param hc   heat capacity of the stream.
	 * @param it   input temperature of the stream.
	 * @param ot   output temperature of the stream.
	 * @param dm   mass flow of the stream.
	 */
	public EnergyProcessHeatCapacityData(String name, double hc, double it, double ot, double dm) {
		super(name, it, ot, dm);
		heatCapacityProperty = new SimpleDoubleProperty(hc);
		typeProperty = new SimpleStringProperty();
		updateType();
	}

	/**
	 * Check if all values of the stream are completed. If it true, the type is
	 * calculated and updated.
	 *
	 * @return true if the stream is complete, false otherwise.
	 */
	@Override
	public boolean isComplete() {
		if (super.isComplete() && heatCapacityProperty.get() != 0.) {
			return true;
		}
		return false;
	}

	/**
	 * Call each time a temperature is updated.
	 */
	private void updateType() {
		// Update type of the stream
		I18n i = I18n.getInstance();
		if (getInputTemp() <= getOutputTemp()) {
			typeProperty.set(i.get("table", "cold"));
		} else {
			typeProperty.set(i.get("table", "hot"));
		}
	}

	/**
	 * @return Heat capacity (Cp)
	 */
	public double getHeatCapacity() {
		return heatCapacityProperty.get();
	}

	/**
	 * @param anHeatCapacity Heat capacity (Cp).
	 */
	public void setHeatCapacity(double anHeatCapacity) {
		heatCapacityProperty.set(anHeatCapacity);
	}

	@Override
	public void setInputTemp(double anInputTemp) {
		super.setInputTemp(anInputTemp);
		updateType();
	}

	@Override
	public void setOutputTemp(double anOutputTemp) {
		super.setOutputTemp(anOutputTemp);
		updateType();
	}

	/**
	 * @return Type Type of the stream (hot or cold)
	 */
	public String getType() {
		return typeProperty.get();
	}

	@Override
	public pinchgui.pinch.EnergyProcess getProcess() {
		return new EnergyProcessHeatCapacity(getName(), "", getHeatCapacity(),
				getInputTemp(), getOutputTemp(), getMassFlow());
	}

	/**
	 *
	 * @return {@link #heatCapacityProperty}
	 */
	public SimpleDoubleProperty getHeatCapacityProperty() {
		return heatCapacityProperty;
	}

	/**
	 *
	 * @return {@link #typeProperty}
	 */
	public SimpleStringProperty getTypeProperty() {
		return typeProperty;
	}

}
