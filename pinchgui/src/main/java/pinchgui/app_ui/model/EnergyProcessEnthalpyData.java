package pinchgui.app_ui.model;

import javafx.beans.property.SimpleDoubleProperty;
import pinchgui.pinch.EnergyProcessEnthalpy;

/**
 * This class models a stream.
 *
 */
public class EnergyProcessEnthalpyData extends EnergyProcessData {

	/**
	 * Default serial IUD.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Input specific enthalpy of the stream.
	 */
	private SimpleDoubleProperty inputEnthalpyProperty;

	/**
	 * Output specific enthalpy of the stream.
	 */
	private SimpleDoubleProperty outputEnthalpyProperty;

	/**
	 * Default constructor of the class.
	 */
	public EnergyProcessEnthalpyData() {
		super();
		inputEnthalpyProperty = new SimpleDoubleProperty(0.);
		outputEnthalpyProperty = new SimpleDoubleProperty(0.);
	}

	/**
	 * Constructor with parameters.
	 *
	 * @param aName Name of the stream
	 * @param it    Input temperature of the stream.
	 * @param ot    Output temperature of the stream.
	 * @param ih    Input specific enthalpy of the stream.
	 * @param oh    Output specific enthalpy of the stream.
	 * @param d     Mass flow of the stream.
	 */
	public EnergyProcessEnthalpyData(String aName, double it, double ot, double ih, double oh, double d) {
		super(aName, it, ot, d);
		inputEnthalpyProperty = new SimpleDoubleProperty(ih);
		outputEnthalpyProperty = new SimpleDoubleProperty(oh);
	}

	/**
	 *
	 * @param process
	 */
	public EnergyProcessEnthalpyData(EnergyProcessEnthalpy process) {
		super(process);
		inputEnthalpyProperty = new SimpleDoubleProperty(process.getInletEnthalpy());
		outputEnthalpyProperty = new SimpleDoubleProperty(process.getOutletEnthalpy());
	}

	/**
	 * Check if all values of the stream are completed. If it true, the type is
	 * calculated and updated.
	 *
	 * @return true if the stream is complete, false otherwise.
	 */
	@Override
	public boolean isComplete() {
		if (super.isComplete()
				&& inputEnthalpyProperty.get() != 0. && outputEnthalpyProperty.get() != 0.) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @return {@link #inputEnthalpyProperty}.
	 */
	public double getInputEnthalpy() {
		return inputEnthalpyProperty.get();
	}

	/**
	 *
	 * @return {@link #outputEnthalpyProperty}.
	 */
	public double getOutputEnthalpy() {
		return outputEnthalpyProperty.get();
	}

	/**
	 *
	 * @param ih New {@link #inputEnthalpyProperty}.
	 */
	public void setInputEnthalpy(double ih) {
		this.inputEnthalpyProperty.set(ih);
	}

	/**
	 *
	 * @param oh New {@link #outputEnthalpyProperty}.
	 */
	public void setOutputEnthalpy(double oh) {
		this.outputEnthalpyProperty.set(oh);
	}

	@Override
	public pinchgui.pinch.EnergyProcess getProcess() {
		return new EnergyProcessEnthalpy(getName(), "", getMassFlow(),
				getInputTemp(), getOutputTemp(),
				inputEnthalpyProperty.get(), outputEnthalpyProperty.get());
	}

	/**
	 *
	 * @return {@link #inputEnthalpyProperty}
	 */
	public SimpleDoubleProperty getInputEnthalpyProperty() {
		return inputEnthalpyProperty;
	}

	/**
	 *
	 * @return {@link #outputEnthalpyProperty}
	 */
	public SimpleDoubleProperty getOutputEnthalpyProperty() {
		return outputEnthalpyProperty;
	}

}
