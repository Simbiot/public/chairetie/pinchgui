package pinchgui.app_ui.model;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

/**
 * Class which contains methods to manage the text display. It allows to have a
 * uniform display throughout the application.
 *
 */
public class TextSetter {

	/**
	 * Default text size.
	 */
	private int textSize;

	/**
	 * Default font of the text.
	 */
	private String font;

	/**
	 * Width of the container of the text area.
	 */
	private ReadOnlyDoubleProperty containerWidth;

	/**
	 *
	 * @param s Size of text
	 * @param f Font of the text
	 */
	public TextSetter(int s, String f) {
		super();
		textSize = s;
		font = f;
	}

	/**
	 *
	 * @param aContainerWidth
	 */
	public void setContainerWidth(ReadOnlyDoubleProperty aContainerWidth) {
		this.containerWidth = aContainerWidth;
	}

	/**
	 * Transform and configured the content to be displayed as a title. Use
	 * {@link #textSize}+3 to set the size of the text.
	 *
	 * @param content   Text to transform
	 * @param f
	 * @param addedSize
	 * @return Displayable text
	 */
	public TextField setTitle(String content, FontWeight f, int addedSize) {
		TextField p1 = new TextField(content);
		p1.setFont(Font.font(font, f, textSize + addedSize));
		p1.setEditable(false);
		p1.getStyleClass().add("text-field");
		return p1;
	}

	/**
	 * Transform and configured the content to be displayed as a paragraph. Use
	 * {@link #textSize} to set the size of the text. The text is not copyable.
	 *
	 * @param content Text to transform
	 * @return Displayable text
	 */
	public TextFlow setText(String content) {
		TextFlow paragraph = new TextFlow();
		paragraph.setTextAlignment(TextAlignment.JUSTIFY);

		// Disregard the layout of text in XML files,
		// because it's a single paragraph.
		String clean1 = content.replaceAll("\n", "").trim().replaceAll("\t+", " ");
		String cleanText = clean1.trim().replaceAll(" +", " ");
		Text p1 = new Text(cleanText);

		p1.setFont(new Font(font, textSize));
		paragraph.getChildren().add(p1);
		paragraph.setStyle("-fx-padding: 0 0 0 0;");
		return paragraph;
	}

	/**
	 * Method to display copyable text using TextArea.
	 *
	 * @param content Text to transform
	 * @return Copyable text
	 */
	public TextArea setParagraph(String content) {
		// Disregard the layout of text in XML files,
		// because it's a single paragraph.
		String clean1 = content.replaceAll("\n", "").trim().replaceAll("\t+", " ");
		String cleanText = clean1.trim().replaceAll(" +", " ");

		TextArea text = new TextArea(cleanText);
		text.setFont(new Font(font, textSize));
		text.setEditable(false);
		text.setWrapText(true);
		text.getStyleClass().add("text-field");

		// Update height of TextArea depending on the text content and on the container
		// width.
		text.widthProperty().addListener((o, oldVal, newVal) -> {
			int nblignes = (int) Math.ceil(text.getText().length() / (containerWidth.get() / 9));
			text.setMinHeight(nblignes * textSize * 2);
		});

		// Set height of TextArea depending on the text content and on the container
		// width.
		int nblignes = (int) Math.ceil(text.getText().length() / (containerWidth.get() / 9));
		text.setMinHeight(nblignes * textSize * 2);

		return text;
	}

}
