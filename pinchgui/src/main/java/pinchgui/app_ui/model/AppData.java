package pinchgui.app_ui.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pinchgui.app_ui.exception.IncompleteTableException;
import pinchgui.app_ui.exception.SameStreamNameException;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.graph.AbstractGraphSetup;
import pinchgui.app_ui.model.graph.GraphComposite;
import pinchgui.app_ui.model.graph.GraphCosts;
import pinchgui.app_ui.model.graph.GraphLargeComposite;
import pinchgui.app_ui.model.graph.GraphMinimalEnergyHeatingCooling;
import pinchgui.app_ui.model.graph.GraphNbHeatExchangers;
import pinchgui.app_ui.model.graph.GraphOffsetComposite;
import pinchgui.app_ui.model.graph.GraphPayback;
import pinchgui.app_ui.model.graph.UnifiedTempScale;
import pinchgui.pinch.EnergyProcess;
import pinchgui.pinch.EnergyTargetingAnalysis;
import pinchgui.pinch.EnergyUtility;
import pinchgui.pinch.HeatExchanger;
import pinchgui.pinch.HenAnalysis;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Class that allows the application data to be temporarily stored during
 * execution.
 *
 */
public class AppData implements Serializable {

	/**
	 * Default serial number.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.app_ui.model.appdata";

	/**
	 * List of streams in the stream table.
	 */
	private ObservableList<EnergyProcessData> streams;

	/**
	 * List of utilities in the stream table.
	 */
	private ObservableList<EnergyUtilityData> utilities;

	/**
	 * List of heat exchangers in the hen result table.
	 */
	private ObservableList<HeatExchangerData> heatExchangers;

	/**
	 * Current graph view of the application : -1 : no graph displayed, 0 : 4 graphs
	 * displayed, 1 : unified temperature scale, 2 : composite curve, 3 : offset
	 * composite curve, 4 : large composite curve, 5 : cost 4 graphs.
	 */
	private int currentGraphView;

	/**
	 * Current definition selected in help view.
	 */
	private String currentDefinition;

	/**
	 * Represent an Energy Targeting analysis for a given list.
	 */
	private EnergyTargetingAnalysis energyTargeting;

	/**
	 * Represent a Supertargeting analysis for a given list.
	 */
	private SuperTargetingAnalysis superTargeting;

	/**
	 * Represent all we need to propose a HEN.
	 */
	private HenAnalysis henAnalysis;

	/**
	 * Minimal difference of temperature between cold and hot composite curbs.
	 */
	private Double chosenDTmin;

	/**
	 * Set true if stream table must be set with inlet and outlet enthalpies instead
	 * of heat capacity.
	 */
	private boolean enthalpyMode;

	/**
	 * Last graph displayed.
	 */
	private AbstractGraphSetup currentGraph;

	/**
	 * Default text size.
	 */
	private int textSize;

	/**
	 * Default font of the text.
	 */
	private String font;

	/**
	 * Used to save and restore language.
	 */
	private String commentLanguage;

	/**
	 * Content of the user comments.
	 */
	private String comments;

	/**
	 * Tag name (in language xml files) of the last exception throw by the pinch
	 * analysis.
	 */
	private String lastPinchException;

	/**
	 * True if the exception message is visible.
	 */
	private boolean exceptionVisible;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger;

	/**
	 * Constructor of the class.
	 */
	public AppData() {
		streams = FXCollections.observableArrayList();
		utilities = FXCollections.observableArrayList();
		heatExchangers = FXCollections.observableArrayList();
		// Assume, only two utilities (1 cold, 1 hot).
		utilities.add(new EnergyUtilityData("Cold"));
		utilities.add(new EnergyUtilityData("Hot"));
		currentGraphView = -1;
		currentDefinition = "streamTable";
		currentGraph = null;
		enthalpyMode = false;
		font = "System";
		textSize = 15;
		comments = "";
		commentLanguage = "";
		lastPinchException = "";
		exceptionVisible = false;
		logger = LoggerFactory.getLogger(CONCEPT_ID);
	}

	/**
	 *
	 * @return {@link #enthalpyMode}.
	 */
	public boolean getEnthalpyMode() {
		return enthalpyMode;
	}

	/**
	 * Set {@link #enthalpyMode}.
	 *
	 * @param newEnthalpyMode
	 */
	public void setEnthalpyMode(boolean newEnthalpyMode) {
		enthalpyMode = newEnthalpyMode;
	}

	/**
	 *
	 * @return True if the exception message is visible, else false.
	 */
	public boolean isExceptionVisible() {
		return exceptionVisible;
	}

	/**
	 *
	 * @param anExceptionVisible True if the exception message is visible, else
	 *                           false.
	 */
	public void setExceptionVisible(boolean anExceptionVisible) {
		exceptionVisible = anExceptionVisible;
	}

	/**
	 *
	 * @return Tag name (in language xml files) of the last exception throw by the
	 *         pinch analysis.
	 */
	public String getLastPinchException() {
		return lastPinchException;
	}

	/**
	 *
	 * @param lastPinchException Tag name (in language xml files) of the last
	 *                           exception throw by the pinch analysis.
	 */
	public void setLastPinchException(String lastPinchException) {
		this.lastPinchException = lastPinchException;
	}

	/**
	 *
	 * @return Comments from the user.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Update comments from the user.
	 *
	 * @param comments Comments from the user
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 *
	 * @return Language used to write the comments.
	 */
	public String getCommentLanguage() {
		return commentLanguage;
	}

	/**
	 * Set size of the text.
	 *
	 * @param size New size.
	 */
	public void setTextSize(int size) {
		if (size > 0)
			textSize = size;
	}

	/**
	 * Set font of the text.
	 *
	 * @param f New font.
	 */
	public void setFont(String f) {
		font = f;
	}

	/**
	 *
	 * @return {@link TextSetter TextSetter.class} object with current properties.
	 */
	public TextSetter getTextSetter() {
		return new TextSetter(textSize, font);
	}

	/**
	 * Set current definition selected in help view.
	 *
	 * @param def
	 */
	public void setCurrentDefinition(String def) {
		currentDefinition = def;
	}

	/**
	 *
	 * @return Current definition selected in help view.
	 */
	public String getCurrentDefinition() {
		return currentDefinition;
	}

	/**
	 * Add serie(s) in a graph and set css properties.
	 *
	 * @param graph    Graph to complete
	 * @param numCurve Number of the graph to set
	 */
	public void setGraph(LineChart<Number, Number> graph, int numCurve) {
		switch (numCurve) {
		case 1:
			currentGraph = new UnifiedTempScale(energyTargeting);
			break;
		case 2:
			currentGraph = new GraphComposite(energyTargeting);
			break;
		case 3:
			currentGraph = new GraphOffsetComposite(energyTargeting);
			break;
		case 4:
			currentGraph = new GraphLargeComposite(energyTargeting);
			break;
		case 5:
			currentGraph = new GraphNbHeatExchangers(superTargeting);
			break;
		case 6:
			currentGraph = new GraphCosts(superTargeting);
			break;
		case 7:
			currentGraph = new GraphMinimalEnergyHeatingCooling(superTargeting);
			break;
		case 8:
			currentGraph = new GraphPayback(superTargeting);
			break;
		default:
			logger.error("Unknown number to identify a graph! " + numCurve);
		}
		currentGraph.setGraph(graph);
	}

	/**
	 * Update the labels in the graph.
	 *
	 * @param graph    Graph to update
	 * @param numCurve Number of the graph to set
	 */
	public void updateGraph(LineChart<Number, Number> graph, int numCurve) {
		switch (numCurve) {
		case 1:
			currentGraph = new UnifiedTempScale(energyTargeting);
			break;
		case 2:
			currentGraph = new GraphComposite(energyTargeting);
			break;
		case 3:
			currentGraph = new GraphOffsetComposite(energyTargeting);
			break;
		case 4:
			currentGraph = new GraphLargeComposite(energyTargeting);
			break;
		case 5:
			currentGraph = new GraphNbHeatExchangers(superTargeting);
			break;
		case 6:
			currentGraph = new GraphCosts(superTargeting);
			break;
		case 7:
			currentGraph = new GraphMinimalEnergyHeatingCooling(superTargeting);
			break;
		case 8:
			currentGraph = new GraphPayback(superTargeting);
			break;
		default:
			logger.error("Unknown number to identify a graph! " + numCurve);
		}
		currentGraph.updateLabelsAndNames(graph);
	}

	/**
	 *
	 * @return {@link #currentGraph}.
	 */
	public AbstractGraphSetup getCurrentGraph() {
		return currentGraph;
	}

	/**
	 * Add text that is specific to the current graph, in the container.
	 *
	 * @param textContainer Container of the text.
	 */
	public void setResulsts(VBox textContainer) {
		currentGraph.setResults(textContainer, getTextSetter());
	}

	/**
	 * Initialize pinch analysis.
	 *
	 * @throws IncompleteTableException  If the table is not complete.
	 * @throws SameStreamNameException
	 * @throws NoPinchPointException
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public void runAlgo() throws IncompleteTableException, SameStreamNameException, NotEnoughProcessException,
			NoPinchPointException, NoDTminFound, InconsistentDTmin {

		checkNoDupplicateInStreamsTable();

		boolean thereAreIncompleteStreams = false;
		energyTargeting = new EnergyTargetingAnalysis("PinchAnalysis");
		energyTargeting.setChosenDeltaTmin(chosenDTmin);
		for (int i = 0; i < streams.size(); i++) {
			if (streams.get(i).isComplete()) {
				EnergyProcessData s = streams.get(i);
				energyTargeting.addProcess(s.getProcess());
			} else {
				thereAreIncompleteStreams = true;
			}
		}
		energyTargeting.initialize();

		if (thereAreIncompleteStreams) {
			throw new IncompleteTableException();
		}

	}

	/**
	 *
	 * @return Energy Targeting Analysis object to get its data.
	 */
	public EnergyTargetingAnalysis getEnergyTargeting() {
		return energyTargeting;
	}

	/**
	 * Use the current {@link #energyTargeting} to compute the costs.
	 *
	 * @param min
	 * @param step
	 * @param max
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public void computeCosts(double min, double step, double max)
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		if (superTargeting != null) {
			superTargeting.compute(min, step, max);
		}
	}

	/**
	 *
	 * @param dtMin    Used to update {@link #chosenDTmin} and redo the energy
	 *                 analysis.
	 * @param minPower Minimal power for proposed heat exchanger.
	 * @throws IncompleteTableException
	 * @throws SameStreamNameException
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public void proposeHen(double dtMin, double minPower) throws IncompleteTableException, SameStreamNameException,
			NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		chosenDTmin = dtMin;
		runAlgo();

		Set<EnergyUtility> utilitySet = new HashSet<EnergyUtility>();
		for (EnergyUtilityData utilityData : utilities) {
			utilitySet.add(utilityData.getEnergyUtility());
		}

		henAnalysis = new HenAnalysis(energyTargeting, utilitySet);
		henAnalysis.setMinimalHeatExchangerPower(minPower);

		henAnalysis.initialize();
		heatExchangers.clear();
		for (HeatExchanger exchanger : henAnalysis.getHeatExchangerList()) {
			heatExchangers.add(new HeatExchangerData(exchanger));
		}
	}

	/**
	 *
	 * @param heFixedCost
	 * @param heRefSurface
	 * @param aModelFactor
	 * @param nbAnnualExploitHours
	 */
	public void prepareSuperTargeting(double heFixedCost, double heRefSurface, double aModelFactor,
			double nbAnnualExploitHours) {
		if (energyTargeting != null && energyTargeting.isInitialized()) {
			superTargeting = new SuperTargetingAnalysis(energyTargeting, heFixedCost, heRefSurface, aModelFactor,
					nbAnnualExploitHours);
		} else {
			logger.error("Cannot instanciate a SuperTargeting if no EnergyTargeting has run!");
		}
	}

	/**
	 *
	 * @return SuperTargeting Analysis object to get its data.
	 */
	public SuperTargetingAnalysis getSuperTargeting() {
		return superTargeting;
	}

	/**
	 *
	 * @return Hen Analysis object to get its data.
	 */
	public HenAnalysis getHenAnalysis() {
		return henAnalysis;
	}

	/**
	 * @return The current graph view of the application
	 */
	public int getCurrentGraphView() {
		return currentGraphView;
	}

	/**
	 * Set the current graph view of the application
	 *
	 * @param graphViewMode The current graph view
	 */
	public void setCurrentGraphView(int graphViewMode) {
		this.currentGraphView = graphViewMode;
	}

	/**
	 * Add a stream to the list of streams.
	 *
	 * @param s New stream to add.
	 */
	public void addStream(EnergyProcessData s) {
		streams.add(s);
	}

	/**
	 * @return List of streams.
	 */
	public ObservableList<EnergyProcessData> getStreams() {
		return streams;
	}

	/**
	 * @return List of utilities.
	 */
	public ObservableList<EnergyUtilityData> getUtilities() {
		return utilities;
	}

	/**
	 * @return List of heat exchangers.
	 */
	public ObservableList<HeatExchangerData> getHeatExchangers() {
		return heatExchangers;
	}

	/**
	 * Refresh text of type column of streams table with the current language.
	 */
	public void refreshTypeColumn() {
		for (EnergyProcessData stream : streams) {
			stream.isComplete();
		}
	}

	/**
	 * Check all cells of streams table, and throw exception if a cell is not
	 * complete.
	 *
	 * @throws IncompleteTableException
	 * @throws SameStreamNameException
	 */
	public void checkNoDupplicateInStreamsTable() throws IncompleteTableException, SameStreamNameException {
		ArrayList<String> list = new ArrayList<String>();
		for (EnergyProcessData stream : streams) {
			if (list.contains(stream.getName())) {
				throw new SameStreamNameException(stream.getName());
			}
			list.add(stream.getName());
		}
	}

	/**
	 * Export the heat cascade in a csv file.
	 *
	 * @param filePath
	 */
	public void exportHeatCascadeCsv(String filePath) {
		if (energyTargeting != null) {
			try {
				energyTargeting.exportHeatCascade(filePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Export the streams table in a csv file.
	 *
	 * @param filePath
	 */
	public void exportTabCsv(String filePath) {
		EnergyTargetingAnalysis pa = new EnergyTargetingAnalysis("export");
		EnergyProcessData s = null;
		for (int i = 0; i < streams.size(); i++) {
			s = streams.get(i);
			pa.addProcess(s.getProcess());
		}
		try {
			// No need to initialize
			pa.exportProcessesTab(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Import a csv file in the streams table.
	 *
	 * @param filePath
	 */
	public void importTabCsv(String filePath) {
		EnergyTargetingAnalysis pa = new EnergyTargetingAnalysis("import");
		try {
			pa.importFluidTab(filePath);
			// For now, auto cast as we know it is processes with constanty cp.
			SortedSet<EnergyProcess> set = new TreeSet<EnergyProcess>(pa.getSortedProcessSet());
			streams.clear();
			for (EnergyProcess p : set) {
				streams.add(p.getProcessData());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		refreshTypeColumn();
	}

	/**
	 * Popup a message to indicate that this part of the code request an update to
	 * work properly.
	 */
	public void alertUpdateNeeded() {
		I18n i = I18n.getInstance();
		Alert problem = new Alert(AlertType.ERROR);
		problem.setTitle(i.get("alert", "update_title"));
		problem.setHeaderText(i.get("alert", "update_message"));
		problem.showAndWait();
	}

	/**
	 * Save data in a file.
	 *
	 * @param fName Name of the file
	 */
	public void save(String fName) {
		alertUpdateNeeded();
//		commentLanguage = I18n.getInstance().getLg();
//		try {
//			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fName));
//			oos.writeInt(currentGraphView);
//			oos.writeObject(currentDefinition);
//			oos.writeObject(new ArrayList<EnergyProcessData>(streams));
//			oos.writeObject(energyTargeting);
//			oos.writeObject(commentLanguage);
//			oos.writeObject(comments);
//			oos.writeObject(lastPinchException);
//			oos.writeObject(exceptionVisible);
//			oos.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//			I18n i = I18n.getInstance();
//			Alert problem = new Alert(AlertType.ERROR);
//			problem.setTitle(i.get("alert", "title"));
//			problem.setHeaderText(i.get("alert/file", "save"));
//			problem.showAndWait();
//		}
	}

	/**
	 * Load data from a file.
	 *
	 * @param fName Name of the file
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */
	public void load(String fName) throws FileNotFoundException, IOException, ClassNotFoundException {
		alertUpdateNeeded();
//		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fName));
//
//		currentGraphView = ois.readInt();
//		currentDefinition = (String) ois.readObject();
//		@SuppressWarnings("unchecked")
//		ArrayList<EnergyProcessHeatCapacityData> list = (ArrayList<EnergyProcessHeatCapacityData>) ois.readObject();
//		streams = FXCollections.observableArrayList(list);
//		energyTargeting = (EnergyTargetingAnalysis) ois.readObject();
//		commentLanguage = (String) ois.readObject();
//		comments = (String) ois.readObject();
//		lastPinchException = (String) ois.readObject();
//		exceptionVisible = (boolean) ois.readObject();
//
//		ois.close();

	}

	/**
	 * Open file chooser and return the file chosen by the user.
	 *
	 * @param tagId        Id to get the title of the file chooser.
	 * @param save         True if the file chooser is used to save a file.
	 * @param typeOfFileId Id to get the extension of the file chooser.
	 * @param fileNameId   Id to get the default file name when the user wants to
	 *                     save a file.
	 * @param anExtension  Extension of the file(s).
	 * @param savePath     Path to default folder where data are saved.
	 * @param stage        Stage to open dialog.
	 * @return The file chosen by the user.
	 */
	public File fileChooser(String tagId, boolean save, String typeOfFileId, String fileNameId, String anExtension,
			String savePath, Stage stage) {
		I18n i = I18n.getInstance();
		FileChooser fileChooser = new FileChooser();
		File initialFolder = new File(savePath);
		if (initialFolder.exists() && initialFolder.isDirectory()) {
			fileChooser.setInitialDirectory(new File(savePath));
		}
		fileChooser.setTitle(i.get("menu/fileChooser", tagId));
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
				i.get("menu/fileChooser", typeOfFileId) + " (*." + anExtension + ")", "*." + anExtension));

		File selected = null;
		if (save) {
			fileChooser.setInitialFileName(i.get("menu/fileChooser", fileNameId));
			selected = fileChooser.showSaveDialog(stage);
		} else {
			selected = fileChooser.showOpenDialog(stage);
		}
		return selected;
	}

	/**
	 *
	 * @return The minimal difference of temperature chosen to conduct the pinch.
	 */
	public Double getChosenDTmin() {
		return chosenDTmin;
	}

	/**
	 * Set the minimal difference of temperature to have for the pinch. Try to
	 * impose it.
	 *
	 * @param newDTmin The new DT min.
	 */
	public void setChosenDTmin(Double newDTmin) {
		chosenDTmin = newDTmin;
	}

}
