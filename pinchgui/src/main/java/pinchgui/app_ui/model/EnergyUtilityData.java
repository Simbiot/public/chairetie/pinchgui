package pinchgui.app_ui.model;

import java.io.Serializable;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import pinchgui.pinch.EnergyUtility;

/**
 * Represent a {@link EnergyUtility} for the TableView.
 *
 *
 */
public class EnergyUtilityData implements Serializable, Cloneable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the stream.
	 */
	private SimpleStringProperty nameProperty;

	/**
	 * Input temperature of the stream.
	 */
	private SimpleDoubleProperty inputTempProperty;

	/**
	 * Output temperature of the stream.
	 */
	private SimpleDoubleProperty outputTempProperty;

	/**
	 * Mass flow of the fluid.
	 */
	private SimpleDoubleProperty costProperty;

	/**
	 * Constructor with parameters.
	 *
	 * @param aName Name of the stream.
	 * @param it    Input temperature of the stream.
	 * @param ot    Output temperature of the stream.
	 * @param c     Cost by kW.
	 */
	public EnergyUtilityData(String aName, double it, double ot, double c) {
		super();
		nameProperty = new SimpleStringProperty(aName);
		inputTempProperty = new SimpleDoubleProperty(it);
		outputTempProperty = new SimpleDoubleProperty(ot);
		costProperty = new SimpleDoubleProperty(c);
	}

	/**
	 *
	 * @param name Name of the utility.
	 */
	public EnergyUtilityData(String name) {
		this(name, 0., 0., 0.);
	}

	/**
	 * Default constructor of the class.
	 */
	public EnergyUtilityData() {
		this("", 0., 0., 0.);
	}

	/**
	 *
	 * @param utility
	 */
	public EnergyUtilityData(EnergyUtility utility) {
		super();
		nameProperty = new SimpleStringProperty(utility.getName());
		inputTempProperty = new SimpleDoubleProperty(utility.getInletT());
		outputTempProperty = new SimpleDoubleProperty(utility.getOutletT());
		costProperty = new SimpleDoubleProperty(utility.getCost());
	}

	/**
	 * Check if all values of the stream are completed. If it true, the type is
	 * calculated and updated.
	 *
	 * @return true if the stream is complete, false otherwise.
	 */
	public boolean isComplete() {
		if (!nameProperty.get().equals("") && inputTempProperty.get() != outputTempProperty.get()
				&& costProperty.get() > 0.) {
			return true;
		}
		return false;
	}

	/**
	 * @return Name Name of the stream given by the user
	 */
	public String getName() {
		return nameProperty.get();
	}

	/**
	 * @param aName Name of the stream given by the user
	 */
	public void setName(String aName) {
		nameProperty.set(aName);
	}

	/**
	 * @return Input temperature (Te)
	 */
	public double getInputTemp() {
		return inputTempProperty.get();
	}

	/**
	 * @param anInputTemp temperature (Te)
	 */
	public void setInputTemp(double anInputTemp) {
		inputTempProperty.set(anInputTemp);
	}

	/**
	 * @return Output temperature (Ts)
	 */
	public double getOutputTemp() {
		return outputTempProperty.get();
	}

	/**
	 * @param anOutputTemp temperature (Ts)
	 */
	public void setOutputTemp(double anOutputTemp) {
		outputTempProperty.set(anOutputTemp);
	}

	/**
	 *
	 * @return {@link #costProperty}.
	 */
	public double getCost() {
		return costProperty.get();
	}

	/**
	 *
	 * @param cc New {@link #costProperty}.
	 */
	public void setCost(double cc) {
		costProperty.set(cc);
	}

	/**
	 * @return true if the type of the stream is cold, else false.
	 */
	public boolean isCold() {
		return getInputTemp() < getOutputTemp();
	}

	/**
	 * @return Name Name property of the stream given by the user
	 */
	public SimpleStringProperty getNameProperty() {
		return nameProperty;
	}

	/**
	 *
	 * @return {@link #inputTempProperty}
	 */
	public SimpleDoubleProperty getInputTempProperty() {
		return inputTempProperty;
	}

	/**
	 *
	 * @return {@link #outputTempProperty}
	 */
	public SimpleDoubleProperty getOutputTempProperty() {
		return outputTempProperty;
	}

	/**
	 *
	 * @return {@link #costProperty}
	 */
	public SimpleDoubleProperty getCostProperty() {
		return costProperty;
	}

	/**
	 * @return Equivalent {@link EnergyUtility}.
	 */
	public EnergyUtility getEnergyUtility() {
		return new EnergyUtility(nameProperty.getValue(), inputTempProperty.getValue(), outputTempProperty.getValue(),
				costProperty.getValue());
	}

	/**
	 * Copy an instance of the object. Implemented method of "Cloneable" interface.
	 *
	 * @see java.lang.Cloneable
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
