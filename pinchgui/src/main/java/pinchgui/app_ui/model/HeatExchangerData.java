package pinchgui.app_ui.model;

import java.io.Serializable;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import pinchgui.pinch.HeatExchanger;

/**
 * Data class for JavaFX linked to {@link HeatExchanger}.
 *
 * @author tparis
 *
 */
public class HeatExchangerData implements Serializable, Cloneable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the hot stream.
	 */
	private SimpleStringProperty hotProcessNameProperty;

	/**
	 * Input temperature of the hot stream.
	 */
	private SimpleDoubleProperty hotInputTempProperty;

	/**
	 * Output temperature of the hot stream.
	 */
	private SimpleDoubleProperty hotOutputTempProperty;

	/**
	 * Mass flow of the hot stream.
	 */
	private SimpleDoubleProperty hotMassFlowProperty;

	/**
	 * Name of the cold stream.
	 */
	private SimpleStringProperty coldProcessNameProperty;

	/**
	 * Input temperature of the cold stream.
	 */
	private SimpleDoubleProperty coldInputTempProperty;

	/**
	 * Output temperature of the cold stream.
	 */
	private SimpleDoubleProperty coldOutputTempProperty;

	/**
	 * Mass flow of the cold stream.
	 */
	private SimpleDoubleProperty coldMassFlowProperty;

	/**
	 * Mass flow of the fluid.
	 */
	private SimpleDoubleProperty powerProperty;

	/**
	 *
	 * @param hotName
	 * @param coldName
	 * @param hotInT
	 * @param hotOutT
	 * @param hotMassFlow
	 * @param coldInT
	 * @param coldOutT
	 * @param coldMassFlow
	 * @param aPower
	 */
	public HeatExchangerData(String hotName, String coldName, double hotInT, double hotOutT, double hotMassFlow,
			double coldInT,
			double coldOutT, double coldMassFlow, double aPower) {
		super();
		hotProcessNameProperty = new SimpleStringProperty(hotName);
		hotInputTempProperty = new SimpleDoubleProperty(hotInT);
		hotOutputTempProperty = new SimpleDoubleProperty(hotOutT);
		hotMassFlowProperty = new SimpleDoubleProperty(hotMassFlow);
		coldProcessNameProperty = new SimpleStringProperty(coldName);
		coldInputTempProperty = new SimpleDoubleProperty(coldInT);
		coldOutputTempProperty = new SimpleDoubleProperty(coldOutT);
		coldMassFlowProperty = new SimpleDoubleProperty(coldMassFlow);
		powerProperty = new SimpleDoubleProperty(aPower);
	}

	/**
	 * Instantiate a {@link HeatExchangerData} from an {@link HeatExchanger}.
	 *
	 * @param exchanger
	 */
	public HeatExchangerData(HeatExchanger exchanger) {
		this(exchanger.getHotFlow().getOwner().getName(),
				exchanger.getColdFlow().getOwner().getName(),
				exchanger.getInletTemperatureHot(),
				exchanger.getOutletTemperatureHot(),
				exchanger.getHotFlow().getMassFlow(),
				exchanger.getInletTemperatureCold(),
				exchanger.getOutletTemperatureCold(),
				exchanger.getColdFlow().getMassFlow(),
				exchanger.getHeatPower());
	}

	/**
	 * @return Name of the hot stream given by the user.
	 */
	public String getHotProcessName() {
		return hotProcessNameProperty.get();
	}

	/**
	 * @return Name of the cold stream given by the user.
	 */
	public String getColdProcessName() {
		return coldProcessNameProperty.get();
	}

	/**
	 * @return Inlet temperature of the hot process.
	 */
	public double getHotProcessInputTemp() {
		return hotInputTempProperty.get();
	}

	/**
	 * @return Outlet temperature of the hot process.
	 */
	public Double getHotProcessOutputTemp() {
		return hotOutputTempProperty.get();
	}

	/**
	 *
	 * /**
	 *
	 * @return Inlet temperature of the cold process.
	 */
	public double getColdProcessInputTemp() {
		return coldInputTempProperty.get();
	}

	/**
	 * @return Outlet temperature of the cold process.
	 */
	public Double getColdProcessOutputTemp() {
		return coldOutputTempProperty.get();
	}

	/**
	 * @return Heat power exchanged by the heat exchanger.
	 */
	public double getPower() {
		return powerProperty.get();
	}

	/**
	 * @return {@link #hotProcessNameProperty}.
	 */
	public SimpleStringProperty getHotProcessNameProperty() {
		return hotProcessNameProperty;
	}

	/**
	 * @return {@link #hotInputTempProperty}.
	 */
	public SimpleDoubleProperty getHotInputTempProperty() {
		return hotInputTempProperty;
	}

	/**
	 * @return {@link #hotOutputTempProperty}.
	 */
	public SimpleDoubleProperty getHotOutputTempProperty() {
		return hotOutputTempProperty;
	}

	/**
	 * @return {@link #coldProcessNameProperty}.
	 */
	public SimpleStringProperty getColdProcessNameProperty() {
		return coldProcessNameProperty;
	}

	/**
	 * @return {@link #coldInputTempProperty}.
	 */
	public SimpleDoubleProperty getColdInputTempProperty() {
		return coldInputTempProperty;
	}

	/**
	 * @return {@link #coldOutputTempProperty}.
	 */
	public SimpleDoubleProperty getColdOutputTempProperty() {
		return coldOutputTempProperty;
	}

	/**
	 * @return the {@link #hotMassFlowProperty}.
	 */
	public SimpleDoubleProperty getHotMassFlowProperty() {
		return hotMassFlowProperty;
	}

	/**
	 * @return the {@link #coldMassFlowProperty}.
	 */
	public SimpleDoubleProperty getColdMassFlowProperty() {
		return coldMassFlowProperty;
	}

	/**
	 * @return {@link #powerProperty}.
	 */
	public SimpleDoubleProperty getPowerProperty() {
		return powerProperty;
	}

	/**
	 * @param hotProcessNameProperty the {@link #hotProcessNameProperty} to set.
	 */
	public void setHotProcessNameProperty(SimpleStringProperty hotProcessNameProperty) {
		this.hotProcessNameProperty = hotProcessNameProperty;
	}

	/**
	 * @param hotInputTempProperty the {@link #hotInputTempProperty} to set.
	 */
	public void setHotInputTempProperty(SimpleDoubleProperty hotInputTempProperty) {
		this.hotInputTempProperty = hotInputTempProperty;
	}

	/**
	 * @param hotOutputTempProperty the {@link #hotOutputTempProperty} to set.
	 */
	public void setHotOutputTempProperty(SimpleDoubleProperty hotOutputTempProperty) {
		this.hotOutputTempProperty = hotOutputTempProperty;
	}

	/**
	 * @param coldProcessNameProperty the {@link #coldProcessNameProperty} to set.
	 */
	public void setColdProcessNameProperty(SimpleStringProperty coldProcessNameProperty) {
		this.coldProcessNameProperty = coldProcessNameProperty;
	}

	/**
	 * @param coldInputTempProperty the {@link #coldInputTempProperty} to set.
	 */
	public void setColdInputTempProperty(SimpleDoubleProperty coldInputTempProperty) {
		this.coldInputTempProperty = coldInputTempProperty;
	}

	/**
	 * @param coldOutputTempProperty the {@link #coldOutputTempProperty} to set.
	 */
	public void setColdOutputTempProperty(SimpleDoubleProperty coldOutputTempProperty) {
		this.coldOutputTempProperty = coldOutputTempProperty;
	}

	/**
	 * @param powerProperty the {@link #powerProperty} to set.
	 */
	public void setPowerProperty(SimpleDoubleProperty powerProperty) {
		this.powerProperty = powerProperty;
	}

	/**
	 * @param hotMassFlowProperty the {@link #hotMassFlowProperty} to set.
	 */
	public void setHotMassFlowProperty(SimpleDoubleProperty hotMassFlowProperty) {
		this.hotMassFlowProperty = hotMassFlowProperty;
	}

	/**
	 * @param coldMassFlowProperty the {@link #coldMassFlowProperty} to set.
	 */
	public void setColdMassFlowProperty(SimpleDoubleProperty coldMassFlowProperty) {
		this.coldMassFlowProperty = coldMassFlowProperty;
	}

	/**
	 * Copy an instance of the object. Implemented method of "Cloneable" interface.
	 *
	 * @see java.lang.Cloneable
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
