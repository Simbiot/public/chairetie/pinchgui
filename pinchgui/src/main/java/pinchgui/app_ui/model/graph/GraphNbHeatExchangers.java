package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.SuperTargetingData;

/**
 * Set graph with composite curves.
 *
 */
public class GraphNbHeatExchangers extends SuperTargetingGraphSetup {

	/**
	 *
	 * @param p SuperTargeting Analysis object
	 */
	public GraphNbHeatExchangers(SuperTargetingAnalysis p) {
		super(p, "graph5");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();

		Series<Number, Number> serie = new Series<Number, Number>();
		I18n i = I18n.getInstance();

		serie.setName(i.get(getName(), "yLabel"));
		allSeries.add(serie);

		List<SuperTargetingData> results = supertargeting.getSuperTargetingResults();
		for (SuperTargetingData data : results) {
			serie.getData().add(new Data<Number, Number>(data.getdTmin(), data.getEstimatedNumberHe()));
		}

		return allSeries;
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		I18n i = I18n.getInstance();
		// Only one serie here.
		graph.getData().get(0).setName(i.get(getName(), "yLabel"));
	}
}
