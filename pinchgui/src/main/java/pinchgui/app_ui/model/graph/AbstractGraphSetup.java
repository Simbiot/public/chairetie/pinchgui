package pinchgui.app_ui.model.graph;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;

/**
 * Abstract class to set data and css properties in graph.
 *
 */
public abstract class AbstractGraphSetup {

	/**
	 * Name of the graph in the code.
	 */
	private String name;

	/**
	 * Constructor of the class.
	 *
	 * @param n Name of the graph in the code
	 */
	public AbstractGraphSetup(String n) {
		name = n;
	}

	/**
	 * Set global information about pinch analysis below each graph.
	 *
	 * @param elems Container of information
	 * @param t     Object to format text
	 */
	protected abstract void setGlobalResults(ObservableList<Node> elems, TextSetter t);

	/**
	 * Add serie(s) in a graph and set css properties
	 *
	 * @param graph Graph to complete
	 */
	public abstract void setGraph(LineChart<Number, Number> graph);

	/**
	 * Set labels on the graph axis.
	 *
	 * @param graph
	 */
	public void setGraphLabels(LineChart<Number, Number> graph) {
		I18n i = I18n.getInstance();
		graph.getXAxis().setLabel(i.get("graphs_info/" + name, "xLabel"));
		graph.getYAxis().setLabel(i.get("graphs_info/" + name, "yLabel"));

	}

	/**
	 * Get the data from {@link #getCurves()} and add them to the graph.
	 *
	 * @param graph
	 */
	public void addGraphData(LineChart<Number, Number> graph) {
		for (Series<Number, Number> s : getCurves()) {
			// Add data
			graph.getData().add(s);
		}
	}

	/**
	 * Add specific information in the container.
	 *
	 * @param textContainer Container of the text.
	 * @param t             Object to format text.
	 */
	public abstract void setResults(VBox textContainer, TextSetter t);

	/**
	 * Return a list of series which correspond to curves of graph. Note that names
	 * are added at this step.
	 *
	 * @return List of series
	 */
	protected abstract List<Series<Number, Number>> getCurves();

	/**
	 * Update all texts when the language is changed.
	 *
	 * @param graph
	 */
	public abstract void updateLabelsAndNames(LineChart<Number, Number> graph);

	/**
	 *
	 * @param graph The graph to screenshot.
	 * @param f     File where to save the graph.
	 */
	public void saveGraphPng(LineChart<Number, Number> graph, File f) {
		WritableImage image = graph.snapshot(new SnapshotParameters(), null);
		try {
			BufferedImage bfImage = SwingFXUtils.fromFXImage(image, null);
			final String acceptedFormat = "png";
			ImageIO.write(bfImage, acceptedFormat, f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 * @return {@link #name}.
	 */
	public String getName() {
		return name;
	}

}
