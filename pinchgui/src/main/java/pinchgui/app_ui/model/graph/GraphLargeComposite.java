package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;
import pinchgui.pinch.EnergyTargetingAnalysis;

/**
 * Set graph with large composite curve.
 *
 */
public class GraphLargeComposite extends EnergyGraphSetup {

	/**
	 * Constructor of the class.
	 *
	 * @param p Pinch Analysis object.
	 */
	public GraphLargeComposite(EnergyTargetingAnalysis p) {
		super(p, "graph4");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series.
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();
		List<NavigableMap<Double, Double>> curves = new ArrayList<NavigableMap<Double, Double>>();

		Series<Number, Number> serie = new Series<Number, Number>();
		I18n i = I18n.getInstance();

		curves.add(pinch.getHeatCascade());
		serie.setName(i.get(getName(), "title"));
		allSeries.add(serie);

		// Add values to the serie(s)
		for (Double temp : curves.get(0).keySet()) {
			allSeries.get(0).getData().add(new Data<Number, Number>(curves.get(0).get(temp), temp));
		}
		return allSeries;
	}

	/**
	 * Add serie(s) in a graph and set css properties.
	 *
	 * @param graph Graph to complete.
	 */
	@Override
	public void setGraph(LineChart<Number, Number> graph) {
		setGraphLabels(graph);
		addGraphData(graph);

		boolean pinch = checkData(graph);
		if (pinch) {
			// Setup name of autosufficient area
			for (int i = 1; i < graph.getData().size(); i++) {
				graph.getData().get(i).setName(I18n.getInstance().get(getName(), "detail") + " " + i);
			}
		}
	}

	/**
	 * Return X value of a point in the list "s" at index "id". Useful especially
	 * for more readability in {@link #checkData(LineChart<Number, Number> graph)
	 * checkData} method.
	 *
	 * @param s  List of points.
	 * @param id Index of the point in the list.
	 * @return X value of the point.
	 */
	private double getX(ObservableList<Data<Number, Number>> s, int id) {
		return (double) s.get(id).getXValue();
	}

	/**
	 * Check if there are energy self-sufficiency areas in the graph. Get also
	 * indexes of several points in the list, to call
	 * {@link #addDetails(LineChart<Number, Number>, int, int, int id2) addDetails}
	 * method.
	 *
	 * @param graph Graph to check.
	 * @return true if there are self-sufficient areas.
	 */
	private boolean checkData(LineChart<Number, Number> graph) {
		ObservableList<Data<Number, Number>> s = getCurves().get(0).getData();
		boolean areaFound = false;

		// Indexes of the points delimiting energy self-sufficiency areas.
		int maxPoint = 0; // Point of the area with the highest x coordinate.
		int highPoint = 0; // Point of the area with the highest y coordinate.
		int startPoint = 0; // Point from which a new area is sought.

		for (int i = 1; i < s.size(); i++) {
			if (maxPoint == startPoint) {
				if (getX(s, i) > getX(s, startPoint)) {
					maxPoint = i;
				} else if (getX(s, i) < getX(s, startPoint)) {
					startPoint = i;
				}
			} else {
				if (highPoint == startPoint) {
					if (getX(s, i) > getX(s, maxPoint)) {
						maxPoint = i;
					} else {
						highPoint = i;
					}
				}
				if (highPoint != startPoint) {
					if (getX(s, i) < getX(s, highPoint)) {
						highPoint = i;
					}

					// Energy self-sufficiency area detected
					if (getX(s, i) > getX(s, highPoint) || highPoint == s.size() - 1) {

						// An end of the segment to be reached to close the self-sufficiency area
						int nextPoint = 0;

						// Add curve on the graph to to delineate the area
						if (getX(s, startPoint) >= getX(s, highPoint)) {
							nextPoint = getNextPoint(s, maxPoint, highPoint, startPoint);
							addDetails(graph, nextPoint, startPoint, nextPoint - 1);
						} else {
							nextPoint = getPreviousPoint(s, startPoint, maxPoint, highPoint);
							addDetails(graph, nextPoint, highPoint, nextPoint - 1);
						}

						// Reset indexes to search a new area
						i--;
						startPoint = i;
						maxPoint = startPoint;
						highPoint = startPoint;
						areaFound = true;
					}
				}
			}
		}
		return areaFound;
	}

	/**
	 * Return the last end of the segment to be reached to close the
	 * self-sufficiency area.
	 *
	 * @param s            List of points.
	 * @param lim1         Index from which the end of the segment is searched.
	 * @param lim2         Index from which the search for the end of the segment
	 *                     ends.
	 * @param betweenPoint Point on the segment whose Y coordinate is sought.
	 * @return An end of the segment to be reached to close the self-sufficiency
	 *         area..
	 */
	private int getNextPoint(ObservableList<Data<Number, Number>> s, int lim1, int lim2, int betweenPoint) {
		for (int i = lim1; i <= lim2; i++) {
			if (getX(s, i) < getX(s, betweenPoint)) {
				return i;
			}
		}
		return lim2;
	}

	/**
	 * Return the fist end of the segment to be reached to close the
	 * self-sufficiency area.
	 *
	 * @param s            List of points.
	 * @param lim1         Index from which the end of the segment is searched.
	 * @param lim2         Index from which the search for the end of the segment
	 *                     ends.
	 * @param betweenPoint Point on the segment whose Y coordinate is sought.
	 * @return An end of the segment to be reached to close the self-sufficiency
	 *         area..
	 */
	private int getPreviousPoint(ObservableList<Data<Number, Number>> s, int lim1, int lim2, int betweenPoint) {
		for (int i = lim1; i <= lim2; i++) {
			if (getX(s, i) > getX(s, betweenPoint)) {
				return i;
			}
		}
		return lim2;
	}

	/**
	 * Add curve on the graph to symbolized energy self-sufficiency area.
	 *
	 * @param graph    Graph to complete
	 * @param idMaxPt  Index of point in the series where is the pinch point
	 * @param idLowPt  Index of the bottom point in the series
	 * @param idHidhPt Index of the rightmost point in the series
	 */
	private void addDetails(LineChart<Number, Number> graph, int idMaxPt, int idLowPt, int idHidhPt) {
		ObservableList<Data<Number, Number>> curveData = getCurves().get(0).getData();

		// Only if acceptable points
		if (idLowPt >= 0 && idLowPt < curveData.size() && idHidhPt >= 0 && idHidhPt < curveData.size() && idMaxPt >= 0
				&& idMaxPt < curveData.size()) {
			// Get 3 corners
			Data<Number, Number> ptN = curveData.get(idLowPt);
			Data<Number, Number> ptB = curveData.get(idHidhPt);
			Data<Number, Number> ptA = curveData.get(idMaxPt);

			double k = ((double) ptN.getXValue() - (double) ptA.getXValue())
					/ ((double) ptB.getXValue() - (double) ptA.getXValue());
			double mY = (double) ptA.getYValue() + k * ((double) ptB.getYValue() - (double) ptA.getYValue());

			// Add series to the graph
			Series<Number, Number> line = new Series<Number, Number>();
			line.getData().add(ptN);
			line.getData().add((new Data<Number, Number>(ptN.getXValue(), mY)));
			graph.getData().add(line);
		}
	}

	/**
	 * Add specific information in the container.
	 *
	 * @param textContainer Container of the text.
	 * @param t             Object to format text.
	 */
	@Override
	public void setResults(VBox textContainer, TextSetter t) {
		ObservableList<Node> elems = textContainer.getChildren();
		elems.clear();
		setGlobalResults(elems, t);
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		// Only one curve to update, we manually change their names.
		I18n i = I18n.getInstance();
		graph.getData().get(0).setName(i.get(getName(), "title"));
		// Setup name of autosufficient area
		for (int n = 1; n < graph.getData().size(); n++) {
			graph.getData().get(n).setName(i.get(getName(), "detail") + " " + n);
		}
	}
}
