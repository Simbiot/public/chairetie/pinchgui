package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.SuperTargetingData;

/**
 * Set graph with composite curves.
 *
 */
public class GraphPayback extends SuperTargetingGraphSetup {

	/**
	 *
	 * @param p SuperTargeting Analysis object
	 */
	public GraphPayback(SuperTargetingAnalysis p) {
		super(p, "graph8");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();

		I18n i = I18n.getInstance();
		Series<Number, Number> seriePayback = new Series<Number, Number>();
		seriePayback.setName(i.get(getName(), "yLabel"));

		List<SuperTargetingData> results = supertargeting.getSuperTargetingResults();
		for (SuperTargetingData data : results) {
			seriePayback.getData()
					.add(new Data<Number, Number>(data.getdTmin(), data.getEstimatedPayBackTimeInMonth()));
		}

		allSeries.add(seriePayback);

		return allSeries;
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		I18n i = I18n.getInstance();
		// Two series here.
		graph.getData().get(0).setName(i.get(getName(), "yLabel"));
	}
}
