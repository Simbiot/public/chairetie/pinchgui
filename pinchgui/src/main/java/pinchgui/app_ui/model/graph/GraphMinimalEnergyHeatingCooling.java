package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.SuperTargetingData;

/**
 * Set graph with composite curves.
 *
 */
public class GraphMinimalEnergyHeatingCooling extends SuperTargetingGraphSetup {

	/**
	 *
	 * @param p SuperTargeting Analysis object
	 */
	public GraphMinimalEnergyHeatingCooling(SuperTargetingAnalysis p) {
		super(p, "graph7");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();

		I18n i = I18n.getInstance();
		Series<Number, Number> serieCooling = new Series<Number, Number>();
		serieCooling.setName(i.get(getName(), "cooling"));
		Series<Number, Number> serieHeating = new Series<Number, Number>();
		serieHeating.setName(i.get(getName(), "heating"));

		List<SuperTargetingData> results = supertargeting.getSuperTargetingResults();
		for (SuperTargetingData data : results) {
			serieCooling.getData()
					.add(new Data<Number, Number>(data.getdTmin(), data.getMinimalCoolingEnergyRequested()));
			serieHeating.getData()
					.add(new Data<Number, Number>(data.getdTmin(), data.getMinimalHeatingEnergyRequested()));
		}

		allSeries.add(serieCooling);
		allSeries.add(serieHeating);

		return allSeries;
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		I18n i = I18n.getInstance();
		// Two series here.
		graph.getData().get(0).setName(i.get(getName(), "cooling"));
		graph.getData().get(1).setName(i.get(getName(), "heating"));
	}
}
