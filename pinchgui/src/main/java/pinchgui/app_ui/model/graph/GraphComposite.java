package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;
import pinchgui.pinch.EnergyTargetingAnalysis;

/**
 * Set graph with composite curves.
 *
 */
public class GraphComposite extends EnergyGraphSetup {

	/**
	 * Constructor of the class.
	 *
	 * @param p Pinch Analysis object
	 */
	public GraphComposite(EnergyTargetingAnalysis p) {
		super(p, "graph2");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();
		List<NavigableMap<Double, Double>> curves = new ArrayList<NavigableMap<Double, Double>>();

		Series<Number, Number> serie = new Series<Number, Number>();
		I18n i = I18n.getInstance();

		curves.add(pinch.getOriginalColdCompositeCurb());
		serie.setName(i.get(getName(), "cold"));
		allSeries.add(serie);
		serie = new Series<Number, Number>();

		curves.add(pinch.getOriginalHotCompositeCurb());
		serie.setName(i.get(getName(), "hot"));
		allSeries.add(serie);

		// Add values to the serie(s)
		for (int j = 0; j < allSeries.size(); j++) {
			for (Double temp : curves.get(j).keySet()) {
				allSeries.get(j).getData().add(new Data<Number, Number>(curves.get(j).get(temp), temp));
			}
		}
		return allSeries;
	}

	/**
	 * Add serie(s) in a graph.
	 *
	 * @param graph Graph to complete.
	 */
	@Override
	public void setGraph(LineChart<Number, Number> graph) {
		setGraphLabels(graph);
		addGraphData(graph);
	}

	/**
	 * Add specific information in the container.
	 *
	 * @param textContainer Container of the text.
	 * @param t             Object to format text.
	 */
	@Override
	public void setResults(VBox textContainer, TextSetter t) {
		ObservableList<Node> elems = textContainer.getChildren();
		elems.clear();
		setGlobalResults(elems, t);
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		// Only two curves to update, we manually change their names.
		I18n i = I18n.getInstance();
		graph.getData().get(0).setName(i.get(getName(), "cold"));
		graph.getData().get(1).setName(i.get(getName(), "hot"));
	}
}
