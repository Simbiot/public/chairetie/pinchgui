package pinchgui.app_ui.model.graph;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.model.TextSetter;
import pinchgui.pinch.SuperTargetingAnalysis;

/**
 * Abstract class to set data and css properties in energy graphs.
 *
 */
public abstract class SuperTargetingGraphSetup extends AbstractGraphSetup {

	/**
	 * Represent a SuperTargeting analysis for a given list.
	 */
	protected SuperTargetingAnalysis supertargeting;

	/**
	 * Constructor of the class.
	 *
	 * @param p Pinch Analysis object
	 * @param n Name of the graph in the code
	 */
	public SuperTargetingGraphSetup(SuperTargetingAnalysis p, String n) {
		super(n);
		supertargeting = p;
	}

	@Override
	public void setGraph(LineChart<Number, Number> graph) {
		setGraphLabels(graph);
		addGraphData(graph);
	}

	/**
	 * Set global information about the results below each graph.
	 *
	 * @param elems Container of information
	 * @param t     Object to format text
	 */
	@Override
	protected void setGlobalResults(ObservableList<Node> elems, TextSetter t) {
		// Nothing to do yet.
	}

	/**
	 * Add specific information in the container.
	 *
	 * @param textContainer Container of the text.
	 * @param t             Object to format text.
	 */
	@Override
	public void setResults(VBox textContainer, TextSetter t) {
		ObservableList<Node> elems = textContainer.getChildren();
		elems.clear();
		setGlobalResults(elems, t);
	}

}
