package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.SuperTargetingData;

/**
 * Set graph with composite curves.
 *
 */
public class GraphCosts extends SuperTargetingGraphSetup {

	/**
	 *
	 * @param p SuperTargeting Analysis object
	 */
	public GraphCosts(SuperTargetingAnalysis p) {
		super(p, "graph6");
	}

	/**
	 * Return a list of series which correspond to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();

		I18n i = I18n.getInstance();

		Series<Number, Number> serieInvestment = new Series<Number, Number>();
		serieInvestment.setName(i.get(getName(), "investment"));
		Series<Number, Number> serieOperation = new Series<Number, Number>();
		serieOperation.setName(i.get(getName(), "operation"));
		Series<Number, Number> serieTotalAnnualCosts = new Series<Number, Number>();
		serieTotalAnnualCosts.setName(i.get(getName(), "total"));
		Series<Number, Number> serieWorstOperation = new Series<Number, Number>();
		serieWorstOperation.setName(i.get(getName(), "worst"));

		List<SuperTargetingData> results = supertargeting.getSuperTargetingResults();
		for (SuperTargetingData data : results) {
			serieInvestment.getData().add(new Data<Number, Number>(data.getdTmin(), data.getHenInvestmentCost()));
			serieOperation.getData().add(new Data<Number, Number>(data.getdTmin(), data.getHenAnnualOperationCost()));
			serieTotalAnnualCosts.getData()
					.add(new Data<Number, Number>(data.getdTmin(), data.getHenAnnualTotalCost()));
			serieWorstOperation.getData()
					.add(new Data<Number, Number>(data.getdTmin(), data.getWorstAnnualOperationCost()));
		}

		allSeries.add(serieInvestment);
		allSeries.add(serieOperation);
		allSeries.add(serieTotalAnnualCosts);
		allSeries.add(serieWorstOperation);

		return allSeries;
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels
		setGraphLabels(graph);
		I18n i = I18n.getInstance();
		// Two series here.
		graph.getData().get(0).setName(i.get(getName(), "investment"));
		graph.getData().get(1).setName(i.get(getName(), "operation"));
		graph.getData().get(2).setName(i.get(getName(), "total"));
		graph.getData().get(3).setName(i.get(getName(), "worst"));
	}
}
