package pinchgui.app_ui.model.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.SortedSet;
import java.util.TreeMap;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;
import pinchgui.pinch.EnergyProcess;
import pinchgui.pinch.EnergyTargetingAnalysis;

/**
 * Set unified temperature scale curves.
 *
 */
public class UnifiedTempScale extends EnergyGraphSetup {

	/**
	 * Constructor of the class.
	 *
	 * @param p Pinch Analysis object
	 */
	public UnifiedTempScale(EnergyTargetingAnalysis p) {
		super(p, "graph1");
	}

	/**
	 * Return a map for unified temperature scale, corresponding to a stream at an
	 * index in the list of streams.
	 *
	 * @param process Stream wanted.
	 * @param i       Index of the stream in the list.
	 * @return A map of values to display the graph.
	 */
	private NavigableMap<Double, Double> getUnifiedTemperatureScale(EnergyProcess process, double i) {
		NavigableMap<Double, Double> curve = new TreeMap<Double, Double>();
		curve.put(process.getInletT(), i);
		curve.put(process.getOutletT(), i);
		return curve;
	}

	/**
	 * Return a list of series which corresponds to curves of graph.
	 *
	 * @return List of series
	 */
	@Override
	protected List<Series<Number, Number>> getCurves() {
		List<Series<Number, Number>> allSeries = new ArrayList<Series<Number, Number>>();
		List<NavigableMap<Double, Double>> curves = new ArrayList<NavigableMap<Double, Double>>();

		Series<Number, Number> serie = new Series<Number, Number>();
		I18n i = I18n.getInstance();

		double it = 1;
		SortedSet<EnergyProcess> processSet = pinch.getSortedProcessSet();
		for (EnergyProcess s : processSet) {
			curves.add(getUnifiedTemperatureScale(s, it));
			it++;
			if (s.isCold()) {
				serie.setName(s.getName() + " (" + i.get("stream", "cold") + ")");
			} else {
				serie.setName(s.getName() + " (" + i.get("stream", "hot") + ")");
			}
			allSeries.add(serie);
			serie = new Series<Number, Number>();
		}

		// Add values to the serie(s)
		for (int j = 0; j < allSeries.size(); j++) {
			for (Double temp : curves.get(j).keySet()) {
				allSeries.get(j).getData().add(new Data<Number, Number>(curves.get(j).get(temp), temp));
			}
		}
		return allSeries;
	}

	/**
	 * Add serie(s) in a graph and set css properties.
	 *
	 * @param graph Graph to complete
	 */
	@Override
	public void setGraph(LineChart<Number, Number> graph) {
		setGraphLabels(graph);

		// Don't use addGraphData() because extra treatment requested to draw arrow
		// instead of lines.
		List<EnergyProcess> processList = new ArrayList<EnergyProcess>(pinch.getSortedProcessSet());
		int it = 0;
		for (Series<Number, Number> s : getCurves()) {
			// Add data
			graph.getData().add(s);
			// Change series line
			for (int i = 0; i < s.getData().size(); i++) {
				Node symbol = s.getData().get(i).getNode().lookup(".chart-line-symbol");
				// The shape is an up or down arrow depending on the type of stream
				// and the number of the point.
				String shape = "";
				if (i % 2 != 0 && processList.get(it).isCold()) {
					shape = "-fx-shape: \"M5,0 L10,8 L0,8 Z\";";
				}
				if (i % 2 == 0 && !processList.get(it).isCold()) {
					shape = "-fx-shape: \"M5,0 L10,-8 L0,-8 Z\";";
				}

				symbol.setStyle(shape);
			}
			it++;
		}
	}

	/**
	 * Add specific information in the container.
	 *
	 * @param textContainer Container of the text.
	 * @param t             Object to format text.
	 */
	@Override
	public void setResults(VBox textContainer, TextSetter t) {
		ObservableList<Node> elems = textContainer.getChildren();
		elems.clear();
		setGlobalResults(elems, t);
	}

	@Override
	public void updateLabelsAndNames(LineChart<Number, Number> graph) {
		// update labels (only them, curve names are numbers)
		setGraphLabels(graph);
	}

}
