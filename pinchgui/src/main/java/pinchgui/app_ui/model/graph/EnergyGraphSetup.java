package pinchgui.app_ui.model.graph;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.text.FontWeight;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;
import pinchgui.pinch.EnergyTargetingAnalysis;

/**
 * Abstract class to set data and css properties in energy graphs.
 *
 */
public abstract class EnergyGraphSetup extends AbstractGraphSetup {

	/**
	 * Represent a Pinch analysis for a given list.
	 */
	protected EnergyTargetingAnalysis pinch;

	/**
	 * Constructor of the class.
	 *
	 * @param p Pinch Analysis object
	 * @param n Name of the graph in the code
	 */
	public EnergyGraphSetup(EnergyTargetingAnalysis p, String n) {
		super(n);
		pinch = p;
	}

	/**
	 * Set global information about pinch analysis below each graph.
	 *
	 * @param elems Container of information
	 * @param t     Object to format text
	 */
	@Override
	protected void setGlobalResults(ObservableList<Node> elems, TextSetter t) {
		I18n i = I18n.getInstance();

		elems.add(t.setTitle(i.get("graphs_info/globalResults", "title"), FontWeight.BOLD, 0));

		elems.add(t.setTitle(
				i.get("graphs_info/globalResults", "pinchPtTemp") + " : " + pinch.getPinchPointTemperature() + " °C",
				FontWeight.NORMAL, -1));
		elems.add(t.setTitle(i.get("graphs_info/globalResults", "minHeatingE") + " : "
				+ pinch.getMinimalHeatingEnergyRequested() + " kW", FontWeight.NORMAL, -1));
		elems.add(t.setTitle(i.get("graphs_info/globalResults", "minCoolingE") + " : "
				+ pinch.getMinimalCoolingEnergyRequested() + " kW", FontWeight.NORMAL, -1));
		elems.add(t.setTitle(i.get("graphs_info/globalResults", "minE") + " : " + pinch.getMinimalEnergy() + " kW",
				FontWeight.NORMAL, -1));
	}

}
