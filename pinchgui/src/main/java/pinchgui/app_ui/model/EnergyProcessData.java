package pinchgui.app_ui.model;

import java.io.Serializable;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import pinchgui.pinch.EnergyProcess;

/**
 * Represent a {@link #EnergyProcess} for the TableView.
 *
 *
 */
public abstract class EnergyProcessData implements Serializable, Cloneable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the stream.
	 */
	private SimpleStringProperty nameProperty;

	/**
	 * Input temperature of the stream.
	 */
	private SimpleDoubleProperty inputTempProperty;

	/**
	 * Output temperature of the stream.
	 */
	private SimpleDoubleProperty outputTempProperty;

	/**
	 * Mass flow of the fluid.
	 */
	private SimpleDoubleProperty massFlowProperty;

	/**
	 * Constructor with parameters.
	 *
	 * @param aName Name of the stream
	 * @param it    Input temperature of the stream.
	 * @param ot    Output temperature of the stream.
	 * @param d     Mass flow of the stream.
	 */
	public EnergyProcessData(String aName, double it, double ot, double d) {
		super();
		nameProperty = new SimpleStringProperty(aName);
		inputTempProperty = new SimpleDoubleProperty(it);
		outputTempProperty = new SimpleDoubleProperty(ot);
		massFlowProperty = new SimpleDoubleProperty(d);
	}

	/**
	 * Default constructor of the class.
	 */
	public EnergyProcessData() {
		this("", 0., 0., 0.);
	}

	/**
	 *
	 * @param process
	 */
	public EnergyProcessData(EnergyProcess process) {
		super();
		nameProperty = new SimpleStringProperty(process.getName());
		inputTempProperty = new SimpleDoubleProperty(process.getInletT());
		outputTempProperty = new SimpleDoubleProperty(process.getOutletT());
		massFlowProperty = new SimpleDoubleProperty(process.getMassFlow());
	}

	/**
	 * Check if all values of the stream are completed. If it true, the type is
	 * calculated and updated.
	 *
	 * @return true if the stream is complete, false otherwise.
	 */
	public boolean isComplete() {
		if (!nameProperty.get().equals("") && inputTempProperty.get() != outputTempProperty.get()
				&& massFlowProperty.get() != 0.) {
			return true;
		}
		return false;
	}

	/**
	 * @return Name Name of the stream given by the user
	 */
	public String getName() {
		return nameProperty.get();
	}

	/**
	 * @param aName Name of the stream given by the user
	 */
	public void setName(String aName) {
		nameProperty.set(aName);
	}

	/**
	 * @return Input temperature (Te)
	 */
	public double getInputTemp() {
		return inputTempProperty.get();
	}

	/**
	 * @param anInputTemp temperature (Te)
	 */
	public void setInputTemp(double anInputTemp) {
		inputTempProperty.set(anInputTemp);
	}

	/**
	 * @return Output temperature (Ts)
	 */
	public double getOutputTemp() {
		return outputTempProperty.get();
	}

	/**
	 * @param anOutputTemp temperature (Ts)
	 */
	public void setOutputTemp(double anOutputTemp) {
		outputTempProperty.set(anOutputTemp);
	}

	/**
	 *
	 * @return {@link #massFlowProperty}.
	 */
	public double getMassFlow() {
		return massFlowProperty.get();
	}

	/**
	 *
	 * @param dm New {@link #massFlowProperty}.
	 */
	public void setMassFlow(double dm) {
		massFlowProperty.set(dm);
	}

	/**
	 * @return true if the type of the stream is cold, else false.
	 */
	public boolean isCold() {
		return getInputTemp() < getOutputTemp();
	}

	/**
	 * @return Name Name property of the stream given by the user
	 */
	public SimpleStringProperty getNameProperty() {
		return nameProperty;
	}

	/**
	 *
	 * @return {@link #inputTempProperty}
	 */
	public SimpleDoubleProperty getInputTempProperty() {
		return inputTempProperty;
	}

	/**
	 *
	 * @return {@link #outputTempProperty}
	 */
	public SimpleDoubleProperty getOutputTempProperty() {
		return outputTempProperty;
	}

	/**
	 *
	 * @return {@link #massFlowProperty}
	 */
	public SimpleDoubleProperty getMassFlowProperty() {
		return massFlowProperty;
	}

	/**
	 * Copy an instance of the object. Implemented method of "Cloneable" interface.
	 *
	 * @see java.lang.Cloneable
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 *
	 * @return A {@link pinchgui.pinch.EnergyProcess}.
	 */
	public abstract pinchgui.pinch.EnergyProcess getProcess();
}
