package pinchgui.app_ui.view;

import javafx.fxml.Initializable;
import pinchgui.app_ui.main.App;

/**
 * Interface that all controllers implements.
 */
public abstract class Controller implements Initializable {

	/**
	 * Object main reference.
	 */
	protected App main;

	/**
	 * Set main attribute.
	 *
	 * @param mainApp Reference to Main instance
	 */
	public void setMainApp(App mainApp) {
		main = mainApp;
		afterSetMainApp();
		loadLanguage();
	}

	/**
	 * (Re)load the text of a container with the current language. Always called
	 * after {@link #setMainApp} and {@link #afterSetMainApp}.
	 */
	public abstract void loadLanguage();

	/**
	 * Method called just after {@link #setMainApp}, enable to automate some
	 * treatments when changing view.
	 */
	protected abstract void afterSetMainApp();
}
