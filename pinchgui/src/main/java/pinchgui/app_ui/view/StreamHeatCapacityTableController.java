package pinchgui.app_ui.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.EnergyProcessData;
import pinchgui.app_ui.model.EnergyProcessHeatCapacityData;
import pinchgui.app_ui.util.EditableTableViewHelper;
import pinchgui.utils.StringToDoubleConverter;

/**
 * Stream table container controller.
 *
 */
public class StreamHeatCapacityTableController extends StreamTableController {

	/**
	 * Cp column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessHeatCapacityData, Double> cpColumn;

	/**
	 * Mass flow column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessData, Double> dmColumn;

	/**
	 * Type column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessHeatCapacityData, String> typeColumn;

	/**
	 * Default constructor of the class.
	 */
	public StreamHeatCapacityTableController() {
	}

	/**
	 * Method called when the controller is create. Initialize the stream table
	 * container.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
		EditableTableViewHelper<EnergyProcessData> helper = new EditableTableViewHelper<EnergyProcessData>();
		helper.editableColumns(tab);
	}

	/**
	 * Add new row to the table.
	 */
	@Override
	@FXML
	public void addNewRow() {
		main.getData().addStream(new EnergyProcessHeatCapacityData());
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		super.loadLanguage();
		I18n i = I18n.getInstance();
		cpColumn.setGraphic(getLabelWithTooltip(i.get("table", "cp"), i.get("table", "cp_tip")));
		dmColumn.setGraphic(getLabelWithTooltip(i.get("table", "dm"), i.get("table", "dm_tip")));
		typeColumn.setGraphic(getLabelWithTooltip(i.get("table", "type"), i.get("table", "type_tip")));

		main.getData().refreshTypeColumn();
		tab.refresh();
	}

	/**
	 *
	 */
	private void setupCpColumn() {
		cpColumn.setEditable(true);
		cpColumn.setCellFactory(
				EditableCell.<EnergyProcessHeatCapacityData, Double>forTableColumn(new StringToDoubleConverter()));
		cpColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessHeatCapacityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessHeatCapacityData, Double> p) {
						return p.getValue().getHeatCapacityProperty().asObject();
					}
				});
		// updates the the value in the data.
		cpColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setHeatCapacity(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	private void setupDmColumn() {
		dmColumn.setEditable(true);
		dmColumn.setCellFactory(
				EditableCell.<EnergyProcessData, Double>forTableColumn(new StringToDoubleConverter()));
		dmColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessData, Double> p) {
						return p.getValue().getMassFlowProperty().asObject();
					}
				});
		// updates the the value in the data.
		dmColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setMassFlow(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	private void setupTypeColumn() {
		typeColumn.setEditable(false);
		typeColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessHeatCapacityData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<EnergyProcessHeatCapacityData, String> p) {
						return p.getValue().getTypeProperty();
					}
				});
	}

	/**
	 * Initialize columns of the table.
	 */
	@Override
	protected void initColumns() {
		super.initColumns();
		setupCpColumn();
		setupTypeColumn();
		setupDmColumn();
	}

}
