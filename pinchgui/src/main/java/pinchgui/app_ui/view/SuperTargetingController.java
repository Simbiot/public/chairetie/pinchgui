package pinchgui.app_ui.view;

import static pinchgui.app_ui.util.HelpLabel.getLabelWithTooltip;
import static pinchgui.app_ui.util.HelpLabel.initializeTextFieldPositiveNumber;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.EnergyUtilityData;
import pinchgui.app_ui.util.EditableTableViewHelper;
import pinchgui.pinch.SuperTargetingAnalysis;
import pinchgui.pinch.SuperTargetingData;
import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;
import pinchgui.utils.StringToDoubleConverter;

/**
 *
 * @author tparis
 *
 */
public class SuperTargetingController extends Controller {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.app_ui.view.supertargeting";

	/**
	 * Title of table.
	 */
	@FXML
	private Label title;

	/**
	 * Text to inform the user.
	 */
	@FXML
	private Label infoText;

	/**
	 * Button to run algorithm.
	 */
	@FXML
	private Button run;

	/**
	 * Stream table.
	 */
	@FXML
	private TableView<EnergyUtilityData> tab;

	/**
	 * Stream column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, String> nameColumn;

	/**
	 * Te (input temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> teColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> tsColumn;

	/**
	 * Cost column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> costColumn;

	/**
	 * Field to set the number of annual exploitation hours.
	 */
	@FXML
	private TextField nbAnnualExploitationHoursField;

	/**
	 * Label for the number of exploitation hours per year.
	 */
	@FXML
	private Label nbAnnualExploitationHoursLabel;

	/**
	 * Field to set the capital annual interest rest.
	 */
	@FXML
	private TextField capitalAnnualInterestField;

	/**
	 * Label to set the capital annual interest rest.
	 */
	@FXML
	private Label capitalAnnualInterestLabel;

	/**
	 * Field to set pay off period.
	 */
	@FXML
	private TextField payOffPeriodField;

	/**
	 * Field to set pay off period.
	 */
	@FXML
	private Label payOffPeriodLabel;

	/**
	 * Field to set the fixed part of a heat exchanger cost.
	 */
	@FXML
	private TextField heFixedCostField;

	/**
	 * Label to set the fixed part of a heat exchanger cost.
	 */
	@FXML
	private Label heFixedCostLabel;

	/**
	 * Field to set the reference surface for the heat exchanger (linked to the
	 * reference cost and fixed price).
	 */
	@FXML
	private TextField heRefSurfaceField;

	/**
	 * Label of the reference surface for the heat exchanger (linked to the
	 * reference cost and fixed price).
	 */
	@FXML
	private Label heRefSurfaceLabel;

	/**
	 * Set the cost exponent function, a parameter used to adapt the prices of a
	 * heat exchanger using the reference value.
	 */
	@FXML
	private TextField costFuctionExponentField;

	/**
	 * Label for the cost exponent function.
	 */
	@FXML
	private Label costFuctionExponentLabel;

	/**
	 * Field to set the model factor, a price that represents part of the variable
	 * part of the heat exchanger price.
	 */
	@FXML
	private TextField modelFactorField;

	/**
	 * Label for the model factor, a price that represents part of the variable part
	 * of the heat exchanger price.
	 */
	@FXML
	private Label modelFactorLabel;

	/**
	 * Field to set the actualization factor, a factor that take into account the
	 * time evolution of the price.
	 */
	@FXML
	private TextField actualizationFactorField;

	/**
	 * Label for the actualization factor.
	 */
	@FXML
	private Label actualizationFactorLabel;

	/**
	 * Field to set the installation factor, a parameter that take into account the
	 * transport and installation part of the price (multiplier factor for the model
	 * factor).
	 */
	@FXML
	private TextField installationFactorField;

	/**
	 * Label for the installation factor.
	 */
	@FXML
	private Label installationFactorLabel;

	/**
	 * Convection heat transfer coefficient (used to estimate heat exchanger
	 * surface).
	 */
	@FXML
	private TextField convectionCoefficientField;

	/**
	 * Label of convection heat transfer coefficient (used to estimate heat
	 * exchanger surface).
	 */
	@FXML
	private Label convectionCoefficientLabel;

	/**
	 * Minimal difference of temperature label.
	 */
	@FXML
	private Label dtMinLabel;

	/**
	 * Minimal difference of temperature for the super targeting analysis.
	 */
	@FXML
	private TextField minDTField;

	/**
	 * DT step for the super targeting.
	 */
	@FXML
	private TextField stepDTField;

	/**
	 * DT maximal value for the super targeting.
	 */
	@FXML
	private TextField maxDTField;

	/**
	 * Title of the error message.
	 */
	@FXML
	private Label exceptionTitle;

	/**
	 * Exception error message.
	 */
	@FXML
	private TextArea exceptionMessage;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger = LoggerFactory.getLogger(CONCEPT_ID);

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		final String languageLoc = "supertargeting/table";
		title.setText(i.get(languageLoc, "title"));
		infoText.setText("(" + i.get(languageLoc, "info") + ")");
		run.setText(i.get(languageLoc, "run"));

		nameColumn.setText("");
		nameColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "utility"), i.get(languageLoc, "utility_tip")));
		teColumn.setText("");
		teColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "te"), i.get(languageLoc, "te_tip")));
		tsColumn.setText("");
		tsColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "ts"), i.get(languageLoc, "ts_tip")));
		costColumn.setText("");
		costColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "cost"), i.get(languageLoc, "cost_tip")));

		String txtLoc = "annualhours";
		nbAnnualExploitationHoursLabel.setText("");
		nbAnnualExploitationHoursLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		nbAnnualExploitationHoursField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		txtLoc = "capital";
		capitalAnnualInterestLabel.setText("");
		capitalAnnualInterestLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, "capital"), i.get(languageLoc, txtLoc + "_tip")));
		capitalAnnualInterestField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		capitalAnnualInterestField.setText(SuperTargetingAnalysis.DEFAULT_INTEREST_OF_CAPITAL + "");
		txtLoc = "payoff";
		payOffPeriodLabel.setText("");
		payOffPeriodLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, "payoff"), i.get(languageLoc, txtLoc + "_tip")));
		payOffPeriodField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		payOffPeriodField.setText(SuperTargetingAnalysis.DEFAULT_PAY_OFF_PERIOD + "");
		txtLoc = "fixcost";
		heFixedCostLabel.setText("");
		heFixedCostLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, "fixcost"), i.get(languageLoc, txtLoc + "_tip")));
		heFixedCostField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		txtLoc = "refsurface";
		heRefSurfaceLabel.setText("");
		heRefSurfaceLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, "refsurface"), i.get(languageLoc, txtLoc + "_tip")));
		heRefSurfaceField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		txtLoc = "costfunction";
		costFuctionExponentLabel.setText("");
		costFuctionExponentLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		costFuctionExponentField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		costFuctionExponentField.setText(SuperTargetingAnalysis.DEFAULT_COST_FUNCTION_EXPONENT + "");
		txtLoc = "modelfactor";
		modelFactorLabel.setText("");
		modelFactorLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		modelFactorField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		txtLoc = "actualizationfactor";
		actualizationFactorLabel.setText("");
		actualizationFactorLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		actualizationFactorField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		actualizationFactorField.setText(SuperTargetingAnalysis.DEFAULT_ACTUALIZATION_FACTOR + "");
		txtLoc = "installfactor";
		installationFactorLabel.setText("");
		installationFactorLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		installationFactorField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		installationFactorField.setText(SuperTargetingAnalysis.DEFAULT_INSTALLATION_FACTOR + "");
		txtLoc = "convection";
		convectionCoefficientLabel.setText("");
		convectionCoefficientLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		convectionCoefficientField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));
		convectionCoefficientField.setText(SuperTargetingAnalysis.DEFAULT_CONVECTION_COEFFICIENT + "");
		txtLoc = "dt";
		dtMinLabel.setText("");
		dtMinLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		minDTField.setPromptText(i.get(languageLoc, "mindt_prompt"));
		stepDTField.setPromptText(i.get(languageLoc, "stepdt_prompt"));
		maxDTField.setPromptText(i.get(languageLoc, "maxdt_prompt"));

		exceptionTitle.setText(i.get("alert", "messageTitle") + ":");
		String exceptionName = main.getData().getLastPinchException();
		if (exceptionName != null && !exceptionName.equals("")) {
			if (exceptionName.equals("noHotProcess") || exceptionName.equals("noColdProcess")) {
				exceptionMessage.setText(
						i.get("alert/exception", "notEnoughProcess") + " "
								+ i.get("alert/exception", exceptionName));
			} else {
				exceptionMessage.setText(i.get("alert/exception", exceptionName));
			}
		}
		exceptionMessage.setVisible(main.getData().isExceptionVisible());
		exceptionTitle.setVisible(main.getData().isExceptionVisible());

		main.getData().refreshTypeColumn();
		tab.refresh();

	}

	/**
	 *
	 */
	protected void setupNameColumn() {
		nameColumn.setEditable(true);
		nameColumn.setCellFactory(EditableCell.<EnergyUtilityData, String>forTableColumn(new DefaultStringConverter()));
		nameColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<EnergyUtilityData, String> p) {
						return p.getValue().getNameProperty();
					}
				});
		// updates the value in the data.
		nameColumn.setOnEditCommit(event -> {
			final String value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setName(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTeColumn() {
		teColumn.setEditable(true);
		teColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		teColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getInputTempProperty().asObject();
					}
				});
		// updates the value in the data.
		teColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setInputTemp(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTsColumn() {
		tsColumn.setEditable(true);
		tsColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		tsColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getOutputTempProperty().asObject();
					}
				});
		// updates the value in the data.
		tsColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setOutputTemp(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupCostColumn() {
		costColumn.setEditable(true);
		costColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		costColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getCostProperty().asObject();
					}
				});
		// updates the value in the data.
		costColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setCost(value);
			tab.refresh();
		});
	}

	/**
	 * Initialize columns of the table.
	 */
	protected void initColumns() {
		tab.getColumns().forEach(e -> e.setReorderable(false));
		setupNameColumn();
		setupTsColumn();
		setupTeColumn();
		setupCostColumn();
	}

	/**
	 * Compute super targeting elements and show graphs..
	 */
	@FXML
	public void computeCosts() {

		if (main.getData().getEnergyTargeting() != null && main.getData().getEnergyTargeting().isInitialized()) {
			try {
				final double heFixedCost = Double.parseDouble(heFixedCostField.getText());
				final double heRefSurface = Double.parseDouble(heRefSurfaceField.getText());
				final double modelFactor = Double.parseDouble(modelFactorField.getText());
				final double nbAnnualExploitHours = Double.parseDouble(nbAnnualExploitationHoursField.getText());

				final double min = Double.parseDouble(minDTField.getText());
				final double step = Double.parseDouble(stepDTField.getText());
				final double max = Double.parseDouble(maxDTField.getText());

				// Either initialize of just set the values.
				if (main.getData().getSuperTargeting() == null
						|| main.getData().getSuperTargeting().getEnergyTargeting() != main.getData()
								.getEnergyTargeting()) {
					main.getData().prepareSuperTargeting(heFixedCost, heRefSurface, modelFactor, nbAnnualExploitHours);
				}
				SuperTargetingAnalysis supertargeting = main.getData().getSuperTargeting();
				supertargeting.setHeatExchangerFixedCost(heFixedCost);
				supertargeting.setHeatExchangerReferenceSurface(heRefSurface);
				supertargeting.setModelFactor(modelFactor);
				supertargeting.setNbAnnualExploitationHours(nbAnnualExploitHours);

				// Impose default values are still set.
				final double annualInterestOfCapital = Double.parseDouble(capitalAnnualInterestField.getText());
				supertargeting.setCapitalAnnualInterest(annualInterestOfCapital);
				final double targetPayOffPeriod = Double.parseDouble(payOffPeriodField.getText());
				supertargeting.setPayOffPeriod(targetPayOffPeriod);
				final double newCostFunctionExponent = Double.parseDouble(costFuctionExponentField.getText());
				supertargeting.setCostFunctionExponent(newCostFunctionExponent);
				final double actualizationFactor = Double.parseDouble(actualizationFactorField.getText());
				supertargeting.setActualizationFactor(actualizationFactor);
				final double installFactor = Double.parseDouble(installationFactorField.getText());
				supertargeting.setTransportationAndInstallationFactor(installFactor);
				final double convection = Double.parseDouble(convectionCoefficientField.getText());
				supertargeting.setConvectionHeatTransferCoefficient(convection);

				// Add Utilities.
				main.getData().getUtilities().forEach(utility -> supertargeting.addUtility(utility.getEnergyUtility()));

				try {
					main.getData().computeCosts(min, step, max);
				} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
					e.printStackTrace();
				}
				if (supertargeting != null && supertargeting.hasComputed()) {
					// If the exception doesn't prevent the computation we print the results.
					main.getData().setCurrentGraphView(Math.max(0, main.getData().getCurrentGraphView()));
					// Load view one graph or view 4 graphs
					if (main.getData().getCurrentGraphView() > 4
							&& main.getData().getCurrentGraphView() <= 8) {
						main.loadNewView(7);
					} else {
						main.loadNewView(6);
					}
				}

			} catch (NumberFormatException e) {
				getLogger().error("One of the requested parameter is not set! " + e.getMessage());
			}

		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
		EditableTableViewHelper<EnergyUtilityData> helper = new EditableTableViewHelper<EnergyUtilityData>();
		helper.editableColumns(tab);
		initializeTextFieldPositiveNumber(nbAnnualExploitationHoursField);
		initializeTextFieldPositiveNumber(capitalAnnualInterestField);
		initializeTextFieldPositiveNumber(payOffPeriodField);
		initializeTextFieldPositiveNumber(heFixedCostField);
		initializeTextFieldPositiveNumber(heRefSurfaceField);
		initializeTextFieldPositiveNumber(costFuctionExponentField);
		initializeTextFieldPositiveNumber(modelFactorField);
		initializeTextFieldPositiveNumber(actualizationFactorField);
		initializeTextFieldPositiveNumber(installationFactorField);
		initializeTextFieldPositiveNumber(convectionCoefficientField);
		initializeTextFieldPositiveNumber(minDTField);
		initializeTextFieldPositiveNumber(stepDTField);
		initializeTextFieldPositiveNumber(maxDTField);

	}

	@Override
	protected void afterSetMainApp() {
		tab.setItems(main.getData().getUtilities());

		int i = main.getData().getCurrentGraphView();
		if (main.getData().getSuperTargeting() != null
				&& main.getData().getSuperTargeting().hasComputed()) {
			if (i > 4 && i <= 8) {
				main.loadNewView(7);
			} else {
				main.loadNewView(6);
			}

			// Get back the last values
			SuperTargetingAnalysis spt = main.getData().getSuperTargeting();
			nbAnnualExploitationHoursField.setText("" + spt.getNbAnnualExploitationHours());
			capitalAnnualInterestField.setText("" + spt.getCapitalAnnualInterest());
			payOffPeriodField.setText("" + spt.getPayOffPeriod());
			heFixedCostField.setText("" + spt.getHeatExchangerFixedCost());
			heRefSurfaceField.setText("" + spt.getHeatExchangerReferenceSurface());
			costFuctionExponentField.setText("" + spt.getCostFunctionExponent());
			modelFactorField.setText("" + spt.getModelFactor());
			actualizationFactorField.setText("" + spt.getActualizationFactor());
			installationFactorField.setText("" + spt.getTransportationAndInstallationFactor());
			convectionCoefficientField.setText("" + spt.getConvectionHeatTransferCoefficient());
			if (spt.hasComputed()) {
				List<SuperTargetingData> results = spt.getSuperTargetingResults();
				if (results.size() > 0) {
					minDTField.setText("" + results.get(0).getdTmin());
					stepDTField.setText("" + (results.get(1).getdTmin() - results.get(0).getdTmin()));
					maxDTField.setText("" + results.get(results.size() - 1).getdTmin());
				}
			}
		}
	}

	/**
	 *
	 * @return {@link #logger}.
	 */
	private Logger getLogger() {
		return logger;
	}
}
