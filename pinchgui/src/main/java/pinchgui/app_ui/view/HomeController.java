package pinchgui.app_ui.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.TextSetter;

/**
 * Help container controller.
 *
 */
public class HomeController extends Controller {

	/**
	 * Container of the text.
	 */
	@FXML
	private VBox container;

	/**
	 * Title of the page.
	 */
	@FXML
	private Text title;

	/**
	 * Default constructor of the class.
	 */
	public HomeController() {
	}

	/**
	 * Method called when the controller is create. Initialize the help container.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		TextSetter t = main.getData().getTextSetter();
		title.setText(i.get("home", "title"));
		container.prefWidthProperty().bind(main.getStage().widthProperty().multiply(0.90));
		container.setMaxWidth(1500);
		container.setSpacing(0);

		// To
		t.setContainerWidth(container.widthProperty());

		container.getChildren().clear();
		container.getChildren().add(title);
		container.getChildren().add(t.setTitle(i.get("home", "subt1"), FontWeight.BOLD, 3));
		container.getChildren().add(t.setParagraph(i.get("home", "p1")));
		container.getChildren().add(t.setParagraph(i.get("home", "p2")));
		container.getChildren().add(t.setTitle(i.get("home", "subt2"), FontWeight.BOLD, 3));
		container.getChildren().add(t.setParagraph(i.get("home", "p3")));
		container.getChildren().add(t.setParagraph(i.get("home", "p4")));
		container.getChildren().add(t.setParagraph(i.get("home", "p4.1")));
		container.getChildren().add(t.setParagraph(i.get("home", "p5")));
		container.getChildren().add(t.setParagraph(i.get("home", "p6")));

	}

	@Override
	protected void afterSetMainApp() {
		// Nothing to do.
	}

}