package pinchgui.app_ui.view;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import pinchgui.app_ui.internationalisation.I18n;

/**
 * Menu container controller.
 *
 */
public class MenuController extends Controller {

	/**
	 * Home button.
	 */
	@FXML
	private Button home;

	/**
	 * Algo button.
	 */
	@FXML
	private Button algo;

	/**
	 * Cost button.
	 */
	@FXML
	private Button costButton;

	/**
	 * Cost button.
	 */
	@FXML
	private Button henButton;

	/**
	 * Help button.
	 */
	@FXML
	private Button help;

	/**
	 * Button to save data.
	 */
	@FXML
	private MenuItem save;

	/**
	 * Button to load data.
	 */
	@FXML
	private MenuItem load;

	/**
	 * Button correspond to the current view load.
	 */
	private Button activeButton;

	/**
	 * Combo box to select the current language.
	 */
	@FXML
	private ComboBox<String> languageChoice;

	/**
	 * Extension use by application to save and load data.
	 */
	private static final String EXTENSION = "pinch";

	/**
	 * Path to default folder where data are saved.
	 */
	private static final String SAVE_PATH = ".";

	/**
	 * Default constructor of the class.
	 */
	public MenuController() {
	}

	/**
	 * Method called when the controller is create. Initialize the menu container.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		activeButton = home;
		loadLanguage();
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();

		home.setText(i.get("menu", "home"));
		algo.setText(i.get("menu", "algo"));
		costButton.setText(i.get("menu", "cost"));
		henButton.setText(i.get("menu", "hen"));
		help.setText(i.get("menu", "help"));
		save.setText(i.get("menu", "save"));
		load.setText(i.get("menu", "load"));

		languageChoice.getItems().clear();
		String[] lgs = I18n.getAllLanguages();
		for (int j = 0; j < lgs.length; j++) {
			languageChoice.getItems().add(i.get("languages", lgs[j]));

			// As the box has been updated, we have to reselect the item
			// (French by default)
			if (lgs[j].equals(i.getLg())) {
				languageChoice.getSelectionModel().select(j);
			} else {
				languageChoice.getSelectionModel().select(0);
			}
		}
	}

	/**
	 * Change and load a new language throughout the application. Method called when
	 * the comboBox of languages changes.
	 *
	 */
	@FXML
	private void changeLanguage() {
		I18n i = I18n.getInstance();
		String[] lgs = I18n.getAllLanguages();
		int index = languageChoice.getSelectionModel().getSelectedIndex();

		// The "clear" and "add" methods on the comboBox also call this method,
		// so we have to check that it is the who user has selected a new item
		if (languageChoice.getItems().size() == I18n.getAllLanguages().length && !lgs[index].equals(i.getLg())) {
			I18n.getInstance().changeLanguage(index);
			loadLanguage();
			main.loadNewLanguage();
		}
	}

	/**
	 * Change the backgroud of the selected tab.
	 *
	 * @param i Number of the new selected tab
	 */
	public void setBackgroud(int i) {
		activeButton.getStyleClass().clear();
		activeButton.getStyleClass().add("menu_button");
		switch (i) {
		case 0:
			activeButton = home;
			break;
		case 1:
			activeButton = algo;
			break;
		case 2:
			activeButton = help;
			break;
		case 5:
			activeButton = costButton;
			break;
		case 8:
			activeButton = henButton;
			break;
		default:
			activeButton = home;
			break;
		}
		activeButton.getStyleClass().clear();
		activeButton.getStyleClass().add("menu_active_button");
	}

	/**
	 * Load the home container at the center of the main container.
	 */
	public void setHomeView() {
		main.loadNewView(0);
	}

	/**
	 * Load the stream table container at the center of the main container.
	 */
	public void setStreamTableView() {
		main.loadNewView(1);
	}

	/**
	 * Load the stream table container at the center of the main container.
	 */
	public void setSupertargetingView() {
		main.loadNewView(5);
	}

	/**
	 * Load the stream table container at the center of the main container.
	 */
	public void setHenView() {
		main.loadNewView(8);
	}

	/**
	 * Load the help container at the center of the main container.
	 */
	public void setHelpView() {
		main.loadNewView(2);
	}

	/**
	 * Create a file chooser to save data in a file.
	 */
	@FXML
	public void save() {
		File selected = main.getData().fileChooser("appSave", true, "pinchFile", "defaultPinchFileName", EXTENSION,
				SAVE_PATH, main.getStage());
		if (selected != null) {
			main.getData().save(selected.getAbsolutePath());
		}
	}

	/**
	 * Create a file chooser to load data from a file and refresh the display.
	 */
	@FXML
	public void load() {
		File selected = main.getData().fileChooser("appLoad", false, "pinchFile", "defaultPinchFileName", EXTENSION,
				SAVE_PATH, main.getStage());
		if (selected != null) {
			try {
				main.getData().load(selected.getAbsolutePath());
				activeButton.fire();
			} catch (Exception e) {
				e.printStackTrace();
				I18n i = I18n.getInstance();
				Alert problem = new Alert(AlertType.ERROR);
				problem.setTitle(i.get("alert", "title"));
				problem.setHeaderText(i.get("alert/file", "load"));
				problem.showAndWait();
			}
		}
	}

	@Override
	protected void afterSetMainApp() {
		// Nothing to do.
	}

}
