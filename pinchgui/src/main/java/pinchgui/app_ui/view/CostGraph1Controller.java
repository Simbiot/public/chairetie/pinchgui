package pinchgui.app_ui.view;

import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.chart.SimpleGraphXY;

/**
 * Single graph container controller.
 *
 */
public class CostGraph1Controller extends Controller {

	/**
	 * Title of the current graph.
	 */
	@FXML
	private Label title;

	/**
	 * Chart of the current graph.
	 */
	@FXML
	private LineChart<Number, Number> graph;

	/**
	 * Container of textual information about the graph.
	 */
	@FXML
	private VBox textContainer;

	/**
	 * Title of comment area.
	 */
	@FXML
	private TextField commentLabel;

	/**
	 * Container of comments from the user.
	 */
	@FXML
	private TextArea commentArea;

	/**
	 * Minimum limit of coment area heigth.
	 */
	private static final int MIN_AREA_HEIGHT = 40;

	/**
	 * Maximum limit of coment area heigth.
	 */
	private static final int MAX_AREA_HEIGHT = 95;

	/**
	 * Extension use by application to save and load data.
	 */
	private static final String EXTENSION = "csv";

	/**
	 * Path to default folder where data are saved.
	 */
	private static final String SAVE_PATH = ".";

	/**
	 * Combo box with export options.
	 */
	@FXML
	private MenuButton exportMenuButton;

	/**
	 * Button to export graphs in a csv file.
	 */
	@FXML
	private MenuItem exportCsv;

	/**
	 * Button to export graphs in a Png file.
	 */
	@FXML
	private MenuItem exportPng;

	/**
	 * Export the graphique as a JFreechart graph (more options like zoom etc).
	 */
	@FXML
	private MenuItem exportExternalView;

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		title.setText(i.get("graph" + main.getData().getCurrentGraphView(), "title"));

		exportMenuButton.setText(i.get("graphs_info", "exportMenu"));
		exportMenuButton.setTooltip(new Tooltip(i.get("graphs_info", "exportMenuTooltip")));
		exportCsv.setText(i.get("graphs_info", "exportCsvButton"));
		exportPng.setText(i.get("graphs_info", "exportPngButton"));
		exportExternalView.setText(i.get("graphs_info", "externalViewButton"));

		// Set title of comment area
		if (!main.getData().getCommentLanguage().equals(i.getLg()) && !main.getData().getComments().isEmpty()) {
			commentLabel.setText(i.get("graphs_info", "commentsLabel") + " " + i.get("graphs_info", "commentAlert"));
		} else {
			commentLabel.setText(i.get("graphs_info", "commentsLabel"));
		}
		commentLabel.setEditable(false);
		commentLabel.getStyleClass().add("text-field");

		// Set comment area
		commentArea.setText(main.getData().getComments());
		commentArea.setWrapText(true);
		updateAreaSize();
		commentArea.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				updateAreaSize();
				main.getData().setComments(newValue);
			}
		});

		if (graph == null || graph.getData().isEmpty()) {
			loadGraphs();
		} else {
			reloadGraphTexts();
		}
		main.getData().setResulsts(textContainer);
	}

	/**
	 * Update labels
	 */
	private void reloadGraphTexts() {
		// Add data in all graphs.
		int numGraphView = main.getData().getCurrentGraphView();
		main.getData().updateGraph(graph, numGraphView);
	}

	/**
	 * Export the heat cascade in a csv file.
	 */
	@FXML
	public void exportGraphCsv() {
		File f = main.getData().fileChooser("heatCascadeSave", true, "csvFile", "defaultheatCascadeFileName", EXTENSION,
				SAVE_PATH, main.getStage());
		if (f != null) {
			main.getData().exportHeatCascadeCsv(f.getAbsolutePath());
		}
	}

	/**
	 * Export the heat cascade in a png file.
	 */
	@FXML
	public void exportGraphPng() {
		final String tagId = "pngSave";
		final String extension = "png";
		final String languageId = "pngFile";
		final String defaultNameId = "defaultPngFileName";
		File f = main.getData().fileChooser(tagId, true, languageId, defaultNameId, extension,
				SAVE_PATH, main.getStage());
		if (f != null) {
			main.getData().getCurrentGraph().saveGraphPng(graph, f);
		}
	}

	/**
	 * Create a JFrame with a {@link SimpleXYGraph}.
	 */
	@FXML
	public void runExternalView() {
		I18n i = I18n.getInstance();
		final String graphId = main.getData().getCurrentGraph().getName();
		final String labelX = i.get("graphs_info/" + graphId, "xLabel");
		final String labelY = i.get("graphs_info/" + graphId, "yLabel");

		SimpleGraphXY simpleGraph = new SimpleGraphXY(title.getText(), labelX, labelY);
		List<Series<Number, Number>> curves = graph.getData();
		for (Series<Number, Number> serie : curves) {
			for (Data<Number, Number> points : serie.getData()) {
				simpleGraph.add(serie.getName(), points.getXValue().doubleValue(), points.getYValue().doubleValue());
			}
			// Check if the serie name referenced cold or hot to adapt the color.
			// First check that the tags hot and cold exists to avoid errors.
			if (i.check(graphId, "cold") && serie.getName().contains(i.get(graphId, "cold"))) {
				simpleGraph.setColor(serie.getName(), Color.BLUE);
			} else if (i.check(graphId, "hot") && serie.getName().contains(i.get(graphId, "hot"))) {
				simpleGraph.setColor(serie.getName(), Color.RED);
			}
			simpleGraph.setWidth(serie.getName(), 3f);
		}
		simpleGraph.display();
	}

	/**
	 * Update comment area size. There are two different sizes : area is smaller
	 * when empty.
	 */
	private void updateAreaSize() {
		if (commentArea.getText().isEmpty()) {
			commentArea.setMaxHeight(MIN_AREA_HEIGHT);
			commentArea.setMinHeight(MIN_AREA_HEIGHT);
		} else {
			commentArea.setMinHeight(MAX_AREA_HEIGHT);
			commentArea.setMaxHeight(MAX_AREA_HEIGHT);
		}
	}

	/**
	 * Load or reload data of the current graph, including text of the legend.
	 */
	private void loadGraphs() {
		// Remove data from current graph.
		graph.getData().clear();

		// Add data in the current graph
		int numGraphView = main.getData().getCurrentGraphView();
		main.getData().setGraph(graph, numGraphView);
	}

	/**
	 * Load the view with all graphs.
	 */
	public void showAllGraphics() {
		main.loadNewView(6);
	}

	/**
	 * Method called when the controller is create. Not used.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@Override
	protected void afterSetMainApp() {
		// Nothing to do.
	}
}
