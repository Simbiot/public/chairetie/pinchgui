package pinchgui.app_ui.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.EnergyProcessData;
import pinchgui.app_ui.model.EnergyProcessEnthalpyData;
import pinchgui.app_ui.util.EditableTableViewHelper;
import pinchgui.utils.StringToDoubleConverter;

/**
 * Stream table container controller.
 *
 */
public class StreamEnthalpyTableController extends StreamTableController {

	/**
	 * He (input specific enthalpy) column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessEnthalpyData, Double> heColumn;

	/**
	 * Hs (output specific enthalpy) column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessEnthalpyData, Double> hsColumn;

	/**
	 * Mass flow column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessData, Double> dmColumn;

	/**
	 * Default constructor of the class.
	 */
	public StreamEnthalpyTableController() {
	}

	/**
	 * Method called when the controller is create. Initialize the stream table
	 * container.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
		EditableTableViewHelper<EnergyProcessData> helper = new EditableTableViewHelper<EnergyProcessData>();
		helper.editableColumns(tab);
	}

	/**
	 * Add new row to the table.
	 */
	@Override
	@FXML
	public void addNewRow() {
		main.getData().addStream(new EnergyProcessEnthalpyData());
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		super.loadLanguage();
		I18n i = I18n.getInstance();
		heColumn.setGraphic(getLabelWithTooltip(i.get("table", "he"), i.get("table", "he_tip")));
		hsColumn.setGraphic(getLabelWithTooltip(i.get("table", "hs"), i.get("table", "hs_tip")));
		dmColumn.setGraphic(getLabelWithTooltip(i.get("table", "dm"), i.get("table", "dm_tip")));

		main.getData().refreshTypeColumn();
		tab.refresh();
	}

	/**
	 *
	 */
	private void setupHeColumn() {
		heColumn.setEditable(true);
		heColumn.setCellFactory(
				EditableCell.<EnergyProcessEnthalpyData, Double>forTableColumn(new StringToDoubleConverter()));
		heColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessEnthalpyData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessEnthalpyData, Double> p) {
						return p.getValue().getInputEnthalpyProperty().asObject();
					}
				});
		// updates the the value in the data.
		heColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setInputEnthalpy(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	private void setupHsColumn() {
		hsColumn.setEditable(true);
		hsColumn.setCellFactory(
				EditableCell.<EnergyProcessEnthalpyData, Double>forTableColumn(new StringToDoubleConverter()));
		hsColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessEnthalpyData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessEnthalpyData, Double> p) {
						return p.getValue().getOutputEnthalpyProperty().asObject();
					}
				});
		// updates the the value in the data.
		hsColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setOutputEnthalpy(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	private void setupDmColumn() {
		dmColumn.setEditable(true);
		dmColumn.setCellFactory(
				EditableCell.<EnergyProcessData, Double>forTableColumn(new StringToDoubleConverter()));
		dmColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessData, Double> p) {
						return p.getValue().getMassFlowProperty().asObject();
					}
				});
		// updates the the value in the data.
		dmColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setMassFlow(value);
			tab.refresh();
		});
	}

	/**
	 * Initialize columns of the table.
	 */
	@Override
	protected void initColumns() {
		super.initColumns();
		setupHeColumn();
		setupHsColumn();
		setupDmColumn();
	}

}
