package pinchgui.app_ui.view;

import static pinchgui.app_ui.util.HelpLabel.getLabelWithTooltip;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.HeatExchangerData;
import pinchgui.utils.StringToDoubleConverter;

/**
 * Four graphs container controller.
 *
 */
public class HenResultController extends Controller {

	/**
	 * Title of the unified temperature scale.
	 */
	@FXML
	private Label title1;

	/**
	 * Chart of the unified temperature scale.
	 */
	@FXML
	private LineChart<Number, Number> graph1;

	/**
	 * Title of the great composite curve.
	 */
	@FXML
	private Label title4;

	/**
	 * Chart of the great composite curve.
	 */
	@FXML
	private LineChart<Number, Number> graph4;

	/**
	 * Stream table.
	 */
	@FXML
	private TableView<HeatExchangerData> tab;

	/**
	 * Stream column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, String> nameHotColumn;

	/**
	 * Te (input temperature) column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> teHotColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> tsHotColumn;

	/**
	 * Hot fluid mass flow column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> massFlowHotColumn;

	/**
	 * Stream column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, String> nameColdColumn;

	/**
	 * Te (input temperature) column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> teColdColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> tsColdColumn;

	/**
	 * Cold fluid mass flow column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> massFlowColdColumn;

	/**
	 * Power column of table.
	 */
	@FXML
	private TableColumn<HeatExchangerData, Double> powerColumn;

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		title1.setText(i.get("graph1", "title"));
		title4.setText(i.get("graph4", "title"));
		final String languageLoc = "hen/exchangers";

		String resource = "hname";
		nameHotColumn.setText("");
		nameHotColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "cname";
		nameColdColumn.setText("");
		nameColdColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "hte";
		teHotColumn.setText("");
		teHotColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "hts";
		tsHotColumn.setText("");
		tsHotColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "hmf";
		massFlowHotColumn.setText("");
		massFlowHotColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "cte";
		teColdColumn.setText("");
		teColdColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "cts";
		tsColdColumn.setText("");
		tsColdColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "cmf";
		massFlowColdColumn.setText("");
		massFlowColdColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		resource = "power";
		powerColumn.setText("");
		powerColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, resource),
				i.get(languageLoc, resource + "_tip")));

		// (Re)load graphs to update text of the legend
		if (graph1 == null || graph1.getData().isEmpty()
				|| graph4 == null || graph4.getData().isEmpty()) {
			loadGraphs();
		} else {
			reloadGraphTexts();
		}
	}

	/**
	 * Update labels
	 */
	private void reloadGraphTexts() {
		// Add data in all graphs.
		main.getData().updateGraph(graph1, 1);
		main.getData().updateGraph(graph4, 4);
	}

	/**
	 *
	 */
	protected void initColumns() {
		nameHotColumn.setEditable(false);
		nameHotColumn.setCellFactory(
				EditableCell.<HeatExchangerData, String>forTableColumn(new DefaultStringConverter()));
		nameHotColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<HeatExchangerData, String> p) {
						return p.getValue().getHotProcessNameProperty();
					}
				});
		nameColdColumn.setEditable(false);
		nameColdColumn.setCellFactory(
				EditableCell.<HeatExchangerData, String>forTableColumn(new DefaultStringConverter()));
		nameColdColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<HeatExchangerData, String> p) {
						return p.getValue().getColdProcessNameProperty();
					}
				});

		teHotColumn.setEditable(false);
		teHotColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		teHotColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getHotInputTempProperty().asObject();
					}
				});
		tsHotColumn.setEditable(false);
		tsHotColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		tsHotColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getHotOutputTempProperty().asObject();
					}
				});
		massFlowHotColumn.setEditable(false);
		massFlowHotColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		massFlowHotColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getHotMassFlowProperty().asObject();
					}
				});

		teColdColumn.setEditable(false);
		teColdColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		teColdColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getColdInputTempProperty().asObject();
					}
				});
		tsColdColumn.setEditable(false);
		tsColdColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		tsColdColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getColdOutputTempProperty().asObject();
					}
				});
		massFlowColdColumn.setEditable(false);
		massFlowColdColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		massFlowColdColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getColdMassFlowProperty().asObject();
					}
				});

		powerColumn.setEditable(false);
		powerColumn.setCellFactory(
				EditableCell.<HeatExchangerData, Double>forTableColumn(new StringToDoubleConverter()));
		powerColumn.setCellValueFactory(
				new Callback<CellDataFeatures<HeatExchangerData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<HeatExchangerData, Double> p) {
						return p.getValue().getPowerProperty().asObject();
					}
				});
	}

	/**
	 * Load or reload data of graphs, including text of the legend.
	 */
	private void loadGraphs() {
		// Remove data from all graphs.
		graph1.getData().clear();
		graph4.getData().clear();
		main.getData().setCurrentGraphView(9);

		// Add data in all graphs.
		main.getData().setGraph(graph1, 1);
		main.getData().setGraph(graph4, 4);

	}

	/**
	 * Change current definition and load help view based on an Event.
	 *
	 * @param e Event trigger by the user by clicking on a button.
	 */
	@FXML
	public void goToDefinition(Event e) {
		String fxid = ((Control) e.getSource()).getId();
		main.getData().setCurrentDefinition(fxid);
		main.loadNewView(2);
	}

	/**
	 * Method called when the controller is created. Not used.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
	}

	@Override
	protected void afterSetMainApp() {
		tab.setItems(main.getData().getHeatExchangers());
	}

}
