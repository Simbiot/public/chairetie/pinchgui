package pinchgui.app_ui.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Label;
import pinchgui.app_ui.internationalisation.I18n;

/**
 * Four graphs container controller.
 *
 */
public class CostGraph4Controller extends Controller {

	/**
	 * Title of the unified temperature scale.
	 */
	@FXML
	private Label title1;

	/**
	 * Chart of minimal heating and cooling required energy depending on the minimal
	 * difference of temperature.
	 */
	@FXML
	private LineChart<Number, Number> graph5;

	/**
	 * Title of the composite curve.
	 */
	@FXML
	private Label title2;

	/**
	 * Chart of surface depending on the minimal difference of temperature.
	 */
	@FXML
	private LineChart<Number, Number> graph6;

	/**
	 * Title of the unified temperature scale.
	 */
	@FXML
	private Label title3;

	/**
	 * Chart of HEN installation cost depending on the minimal difference of
	 * temperature.
	 */
	@FXML
	private LineChart<Number, Number> graph7;

	/**
	 * Title of the large composite curve.
	 */
	@FXML
	private Label title4;

	/**
	 * Chart of the return on investment time.
	 */
	@FXML
	private LineChart<Number, Number> graph8;

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		title1.setText(i.get("graph5", "title"));
		title2.setText(i.get("graph6", "title"));
		title3.setText(i.get("graph7", "title"));
		title4.setText(i.get("graph8", "title"));

		// (Re)load graphs to update text of the legend
		if (graph5 == null || graph5.getData().isEmpty()
				|| graph6 == null || graph6.getData().isEmpty()
				|| graph7 == null || graph7.getData().isEmpty()
				|| graph8 == null || graph8.getData().isEmpty()) {
			loadGraphs();
		} else {
			reloadGraphTexts();
		}
	}

	/**
	 * Update labels
	 */
	private void reloadGraphTexts() {
		// Add data in all graphs.
		main.getData().updateGraph(graph5, 5);
		main.getData().updateGraph(graph6, 6);
		main.getData().updateGraph(graph7, 7);
		main.getData().updateGraph(graph8, 8);
	}

	/**
	 * Load or reload data of graphs, including text of the legend.
	 */
	private void loadGraphs() {
		// Remove data from all graphs.
		graph5.getData().clear();
		graph6.getData().clear();
		graph7.getData().clear();
		graph8.getData().clear();
		main.getData().setCurrentGraphView(0);

		// Add data in all graphs.
		main.getData().setGraph(graph5, 5);
		main.getData().setGraph(graph6, 6);
		main.getData().setGraph(graph7, 7);
		main.getData().setGraph(graph8, 8);

	}

	/**
	 * Load the view of the unified temperature scale.
	 */
	public void showGraph1() {
		main.getData().setCurrentGraphView(5);
		main.loadNewView(7);
	}

	/**
	 * Load the view of the composite graph.
	 */
	public void showGraph2() {
		main.getData().setCurrentGraphView(6);
		main.loadNewView(7);
	}

	/**
	 * Load the view of the offset composite curve.
	 */
	public void showGraph3() {
		main.getData().setCurrentGraphView(7);
		main.loadNewView(7);
	}

	/**
	 * Load the view of the large composite curve.
	 */
	public void showGraph4() {
		main.getData().setCurrentGraphView(8);
		main.loadNewView(7);
	}

	/**
	 * Method called when the controller is created. Not used.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@Override
	protected void afterSetMainApp() {
		// Nothing to do.
	}

}
