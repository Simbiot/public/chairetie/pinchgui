package pinchgui.app_ui.view;

import static pinchgui.app_ui.util.HelpLabel.getLabelWithTooltip;
import static pinchgui.app_ui.util.HelpLabel.initializeTextFieldPositiveNumber;

import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import pinchgui.app_ui.exception.IncompleteTableException;
import pinchgui.app_ui.exception.SameStreamNameException;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.EnergyUtilityData;
import pinchgui.app_ui.util.EditableTableViewHelper;
import pinchgui.pinch.HenAnalysis;
import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;
import pinchgui.utils.StringToDoubleConverter;

/**
 *
 * @author tparis
 *
 */
public class HenAnalysisController extends Controller {

	/**
	 * Hierarchical dot notation to identify logger.
	 */
	public static final String CONCEPT_ID = "pinchgui.app_ui.view.hen";

	/**
	 * Title of table.
	 */
	@FXML
	private Label title;

	/**
	 * Text to inform the user.
	 */
	@FXML
	private Label infoText;

	/**
	 * Button to run algorithm.
	 */
	@FXML
	private Button run;

	/**
	 * Stream table.
	 */
	@FXML
	private TableView<EnergyUtilityData> tab;

	/**
	 * Stream column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, String> nameColumn;

	/**
	 * Te (input temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> teColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> tsColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyUtilityData, Double> costColumn;

	/**
	 * Label for the parameter of the algorithm.
	 */
	@FXML
	private Label parameterLabel;

	/**
	 * Minimal difference of temperature label.
	 */
	@FXML
	private Label dtMinLabel;

	/**
	 * Minimal difference of temperature to use for the HEN proposal.
	 */
	@FXML
	private TextField dtMinField;

	/**
	 * Minimal power required to propose a heat exchangers label.
	 */
	@FXML
	private Label minPowerLabel;

	/**
	 * Minimal power required to propose a heat exchangers.
	 */
	@FXML
	private TextField minPowerField;

	/**
	 * Title of the error message.
	 */
	@FXML
	private Label exceptionTitle;

	/**
	 * Exception error message.
	 */
	@FXML
	private TextArea exceptionMessage;

	/**
	 * Logger to debug code.
	 */
	private final Logger logger = LoggerFactory.getLogger(CONCEPT_ID);

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		final String languageLoc = "hen/table";
		title.setText(i.get(languageLoc, "title"));
		infoText.setText("(" + i.get(languageLoc, "info") + ")");
		run.setText(i.get(languageLoc, "run"));

		nameColumn.setText("");
		nameColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "utility"), i.get(languageLoc, "utility_tip")));
		teColumn.setText("");
		teColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "te"), i.get(languageLoc, "te_tip")));
		tsColumn.setText("");
		tsColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "ts"), i.get(languageLoc, "ts_tip")));
		costColumn.setText("");
		costColumn.setGraphic(getLabelWithTooltip(i.get(languageLoc, "cost"), i.get(languageLoc, "cost_tip")));

		String txtLoc = "parameter";
		parameterLabel.setText(i.get(languageLoc, txtLoc));

		txtLoc = "dt";
		dtMinLabel.setText("");
		dtMinLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		dtMinField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));

		txtLoc = "minpower";
		minPowerLabel.setText("");
		minPowerLabel.setGraphic(
				getLabelWithTooltip(i.get(languageLoc, txtLoc), i.get(languageLoc, txtLoc + "_tip")));
		minPowerField.setPromptText(i.get(languageLoc, txtLoc + "_prompt"));

		exceptionTitle.setText(i.get("alert", "messageTitle") + ":");
		String exceptionName = main.getData().getLastPinchException();
		if (exceptionName != null && !exceptionName.equals("")) {
			if (exceptionName.equals("noHotProcess") || exceptionName.equals("noColdProcess")) {
				exceptionMessage.setText(
						i.get("alert/exception", "notEnoughProcess") + " "
								+ i.get("alert/exception", exceptionName));
			} else {
				exceptionMessage.setText(i.get("alert/exception", exceptionName));
			}
		}
		exceptionMessage.setVisible(main.getData().isExceptionVisible());
		exceptionTitle.setVisible(main.getData().isExceptionVisible());

		main.getData().refreshTypeColumn();
		tab.refresh();

	}

	/**
	 *
	 */
	protected void setupNameColumn() {
		nameColumn.setEditable(true);
		nameColumn.setCellFactory(EditableCell.<EnergyUtilityData, String>forTableColumn(new DefaultStringConverter()));
		nameColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<EnergyUtilityData, String> p) {
						return p.getValue().getNameProperty();
					}
				});
		// updates the value in the data.
		nameColumn.setOnEditCommit(event -> {
			final String value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setName(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTeColumn() {
		teColumn.setEditable(true);
		teColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		teColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getInputTempProperty().asObject();
					}
				});
		// updates the value in the data.
		teColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setInputTemp(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTsColumn() {
		tsColumn.setEditable(true);
		tsColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		tsColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getOutputTempProperty().asObject();
					}
				});
		// updates the value in the data.
		tsColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setOutputTemp(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupCostColumn() {
		costColumn.setEditable(true);
		costColumn.setCellFactory(
				EditableCell.<EnergyUtilityData, Double>forTableColumn(new StringToDoubleConverter()));
		costColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyUtilityData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyUtilityData, Double> p) {
						return p.getValue().getCostProperty().asObject();
					}
				});
		// updates the value in the data.
		costColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setCost(value);
			tab.refresh();
		});
	}

	/**
	 * Initialize columns of the table.
	 */
	protected void initColumns() {
		tab.getColumns().forEach(e -> e.setReorderable(false));
		setupNameColumn();
		setupTsColumn();
		setupTeColumn();
		setupCostColumn();
	}

	@FXML
	public void testHen() {
		boolean exception = true;
		try {
			main.getData().proposeHen(Double.parseDouble(dtMinField.getText()),
					Double.parseDouble(minPowerField.getText()));
			main.loadNewView(9);
			exception = false;

		} catch (IncompleteTableException | NotEnoughProcessException | NoPinchPointException | NoDTminFound
				| InconsistentDTmin | SameStreamNameException e) {
			// All exception are handled via their id.
			Alert problem = new Alert(AlertType.ERROR);
			problem.setTitle(I18n.getInstance().get("alert", "title"));
			problem.setHeaderText(I18n.getInstance().get("exception", e.getId()));
			problem.showAndWait();
			main.getData().setLastPinchException(e.getId());
		} catch (Exception e) {
			I18n i = I18n.getInstance();
			Alert problem = new Alert(AlertType.ERROR);
			problem.setTitle(i.get("alert", "title"));
			problem.setHeaderText(i.get("alert/exception", "defaultException"));
			problem.showAndWait();
			main.getData().setLastPinchException("defaultException");
		}

		String exceptionName = main.getData().getLastPinchException();
		if (exceptionName != null && !exceptionName.equals("")) {
			I18n i18n = I18n.getInstance();
			if (exceptionName.equals("noHotProcess") || exceptionName.equals("noColdProcess")) {
				exceptionMessage.setText(i18n.get("alert/exception", "notEnoughProcess") + " "
						+ i18n.get("alert/exception", exceptionName));
			} else {
				exceptionMessage.setText(i18n.get("alert/exception", exceptionName));
			}
		}
		exceptionMessage.setVisible(exception);
		exceptionTitle.setVisible(exception);
		main.getData().setExceptionVisible(exception);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initColumns();
		EditableTableViewHelper<EnergyUtilityData> helper = new EditableTableViewHelper<EnergyUtilityData>();
		helper.editableColumns(tab);
		initializeTextFieldPositiveNumber(dtMinField);
		initializeTextFieldPositiveNumber(minPowerField);

	}

	@Override
	protected void afterSetMainApp() {
		tab.setItems(main.getData().getUtilities());

		if (main.getData().getEnergyTargeting() != null
				&& main.getData().getEnergyTargeting().isInitialized()) {
			// Load view of HEN results.
			main.loadNewView(9);

			// Get back the last values
			HenAnalysis spt = main.getData().getHenAnalysis();
			if (spt != null && spt.hasComputed()) {
				dtMinField.setText("" + spt.getEnergyTargeting().getDeltaTmin());
				minPowerField.setText("" + spt.getMinimalHeatExchangerPower());
			}
		}
	}

	/**
	 *
	 * @return {@link #logger}.
	 */
	private Logger getLogger() {
		return logger;
	}
}
