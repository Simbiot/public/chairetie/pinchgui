package pinchgui.app_ui.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;

/**
 *
 * Alternative implementation of {@link TableCell} inspired from
 * {@link TextFieldTableCell}. This class provide methods to edit the Cell and
 * render it as a TextField.
 *
 * Code taken from @see
 * <a href= "https://github.com/lankydan/JavaFX-Table-Tutorial"> lankydan JavaFX
 * tutorial</a>.
 *
 *
 * @param <S>
 * @param <T>
 */
public class EditableCell<S, T> extends TableCell<S, T> {

	/**
	 * The text field to be used when editing.
	 */
	private TextField textField;

	/**
	 * Whether or not the escaped key is pressed.
	 */
	private boolean escapePressed = false;

	/**
	 * Position in the Table.
	 */
	private TablePosition<S, ?> tablePos = null;

	/**
	 * Properties
	 */
	private ObjectProperty<StringConverter<T>> converter = new SimpleObjectProperty<StringConverter<T>>(this,
			"converter");

	/**
	 *
	 * @param aConverter
	 */
	public EditableCell(final StringConverter<T> aConverter) {
		this.getStyleClass().add("text-field-table-cell");
		setConverter(aConverter);
	}

	/**
	 * The {@link StringConverter} property.
	 *
	 * @return the {@link StringConverter} property
	 */
	public final ObjectProperty<StringConverter<T>> converterProperty() {
		return converter;
	}

	/**
	 * Sets the {@link StringConverter} to be used in this cell.
	 *
	 * @param value the {@link StringConverter} to be used in this cell
	 */
	public final void setConverter(StringConverter<T> value) {
		converterProperty().set(value);
	}

	/**
	 * Returns the {@link StringConverter} used in this cell.
	 *
	 * @return the {@link StringConverter} used in this cell
	 */
	public final StringConverter<T> getConverter() {
		return converterProperty().get();
	}

	/**
	 *
	 * @param <S> Type
	 * @return Call back method that return the {@link EditableCell} with a
	 *         {@link DefaultStringConverter}.
	 */
	public static <S> Callback<TableColumn<S, String>, TableCell<S, String>> forTableColumn() {
		return forTableColumn(new DefaultStringConverter());
	}

	/**
	 *
	 * @param <S>       Type
	 * @param <T>       Type
	 * @param converter Converter to convert from S to T and vice versa.
	 * @return Call back method that return the {@link EditableCell}.
	 */
	public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(
			final StringConverter<T> converter) {
		return list -> new EditableCell<S, T>(converter);
	}

	@Override
	public void startEdit() {
		if (!isEditable() || !getTableView().isEditable()
				|| !getTableColumn().isEditable()) {
			return;
		}
		super.startEdit();

		if (isEditing()) {
			if (textField == null) {
				textField = getTextField();
			}
			escapePressed = false;
			startEdit(textField);
			final TableView<S> table = getTableView();
			tablePos = table.getEditingCell();
		}
	}

	/** {@inheritDoc} */
	@Override
	public void commitEdit(T newValue) {
		if (!isEditing()) {
			return;
		}
		final TableView<S> table = getTableView();
		if (table != null) {
			// Inform the TableView of the edit being ready to be committed.
			@SuppressWarnings("unchecked")
			CellEditEvent<?, ?> editEvent = new CellEditEvent(table, tablePos,
					TableColumn.editCommitEvent(), newValue);

			Event.fireEvent(getTableColumn(), editEvent);
		}
		// we need to setEditing(false):
		super.cancelEdit(); // this fires an invalid EditCancelEvent.
		// update the item within this cell, so that it represents the new value
		updateItem(newValue, false);
		if (table != null) {
			// reset the editing cell on the TableView
			table.edit(-1, null);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void cancelEdit() {
		if (escapePressed) {
			// this is a cancel event after escape key
			super.cancelEdit();
			setText(getItemText()); // restore the original text in the view
		} else {
			// this is not a cancel event after escape key
			// we interpret it as commit.
			String newText = textField.getText();
			// commit the new text to the model
			this.commitEdit(getConverter().fromString(newText));
		}
		setGraphic(null); // stop editing with TextField
	}

	/** {@inheritDoc} */
	@Override
	public void updateItem(T item, boolean empty) {
		super.updateItem(item, empty);
		updateItem();
	}

	private TextField getTextField() {

		final TextField aTextField = new TextField(getItemText());

		// Use onAction here rather than onKeyReleased (with check for Enter),
		aTextField.setOnAction(event -> {
			if (getConverter() == null) {
				throw new IllegalStateException("StringConverter is null.");
			}
			this.commitEdit(getConverter().fromString(aTextField.getText()));
			event.consume();
		});

		aTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					commitEdit(getConverter().fromString(aTextField.getText()));
				}
			}
		});

		aTextField.setOnKeyPressed(t -> {
			if (t.getCode() == KeyCode.ESCAPE) {
				escapePressed = true;
			} else {
				escapePressed = false;
			}
		});
		aTextField.setOnKeyReleased(t -> {
			if (t.getCode() == KeyCode.ESCAPE) {
				throw new IllegalArgumentException(
						"Did not expect escape key releases here.");
			}
		});

		/*
		 * Treat particular events not linked to the edition (enter, left key etc).
		 */
		aTextField.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			if (event.getCode() == KeyCode.ESCAPE) {
				// Commit the update if escape is pressed.
				aTextField.setText(getConverter().toString(getItem()));
				cancelEdit();
				event.consume();
			} else if (event.getCode() == KeyCode.RIGHT) {
				aTextField.forward();
				event.consume();
			} else if (event.getCode() == KeyCode.LEFT) {
				aTextField.backward();
				event.consume();
			} else if (event.getCode() == KeyCode.UP) {
				getTableView().getSelectionModel().selectAboveCell();
				event.consume();
			} else if (event.getCode() == KeyCode.DOWN) {
				getTableView().getSelectionModel().selectBelowCell();
				event.consume();
			} else if (event.getCode() == KeyCode.ENTER
					|| event.getCode() == KeyCode.TAB) {
				cancelEdit();
				getTableView().getSelectionModel().selectNext();
				event.consume();
			}
		});

		return aTextField;
	}

	private String getItemText() {
		return getConverter() == null
				? getItem() == null ? "" : getItem().toString()
				: getConverter().toString(getItem());
	}

	private void updateItem() {
		if (isEmpty()) {
			setText(null);
			setGraphic(null);
		} else {
			if (isEditing()) {
				if (textField != null) {
					textField.setText(getItemText());
				}
				setText(null);
				setGraphic(textField);
			} else {
				setText(getItemText());
				setGraphic(null);
			}
		}
	}

	private void startEdit(final TextField aTextField) {
		if (aTextField != null) {
			aTextField.setText(getItemText());
		}
		setText(null);
		setGraphic(aTextField);
		aTextField.selectAll();
		// requesting focus so that key input can immediately go into the
		// TextField
		aTextField.requestFocus();
	}
}
