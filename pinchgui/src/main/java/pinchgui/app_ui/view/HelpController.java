package pinchgui.app_ui.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import pinchgui.app_ui.internationalisation.I18n;

/**
 * Help container controller.
 *
 */
public class HelpController extends Controller {

	/**
	 * Title of the current word.
	 */
	@FXML
	private Label title;

	/**
	 * Definition of the current word.
	 */
	@FXML
	private Text content;

	/**
	 * Stream table button.
	 */
	@FXML
	private Button streamTable;

	/**
	 * Unified temperature scale button.
	 */
	@FXML
	private Button unifiedTemp;

	/**
	 * Composite curve button.
	 */
	@FXML
	private Button composite;

	/**
	 * Offset composite curve button.
	 */
	@FXML
	private Button offsetComposite;

	/**
	 * Large composite curve button.
	 */
	@FXML
	private Button largeComposite;

	/**
	 * Last button pressed.
	 */
	@FXML
	private Control activeButton;

	/**
	 * Default constructor of the class.
	 */
	public HelpController() {
	}

	/**
	 * Change the current definition depending based on an Event.
	 *
	 * @param e Event trigger by the user by clicking on a button.
	 */
	@FXML
	private void changeDefinition(Event e) {
		activeButton.getStyleClass().clear();
		activeButton.getStyleClass().add("def_button");

		activeButton = (Control) e.getSource();
		activeButton.getStyleClass().clear();
		activeButton.getStyleClass().add("def_active_button");

		main.getData().setCurrentDefinition(activeButton.getId());
		loadLanguage();
	}

	/**
	 * Change the current definition and load the help view.
	 */
	@FXML
	private void goToDefinition() {
		String def = main.getData().getCurrentDefinition();
		switch (def) {
		case "streamTable":
			activeButton = streamTable;
			break;
		case "unifiedTemp":
			activeButton = unifiedTemp;
			break;
		case "composite":
			activeButton = composite;
			break;
		case "offsetComposite":
			activeButton = offsetComposite;
			break;
		case "largeComposite":
			activeButton = largeComposite;
			break;
		default:
			activeButton = streamTable;
			break;
		}
		activeButton.getStyleClass().clear();
		activeButton.getStyleClass().add("def_active_button");
		main.getData().setCurrentDefinition(activeButton.getId());
		loadLanguage();
	}

	/**
	 * Method called when the controller is create. Initialize the stream table
	 * container.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		String def = main.getData().getCurrentDefinition();

		title.setText(i.get("definition/" + def, "title"));
		streamTable.setText(i.get("definition/streamTable", "title"));
		unifiedTemp.setText(i.get("definition/unifiedTemp", "title"));
		composite.setText(i.get("definition/composite", "title"));
		offsetComposite.setText(i.get("definition/offsetComposite", "title"));
		largeComposite.setText(i.get("definition/largeComposite", "title"));

		content.setText(i.get("definition/" + def, "content"));

	}

	@Override
	protected void afterSetMainApp() {
		goToDefinition();
	}

}
