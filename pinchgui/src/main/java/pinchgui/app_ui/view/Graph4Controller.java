package pinchgui.app_ui.view;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import pinchgui.app_ui.internationalisation.I18n;

/**
 * Four graphs container controller.
 *
 */
public class Graph4Controller extends Controller {

	/**
	 * Title of the unified temperature scale.
	 */
	@FXML
	private Label title1;

	/**
	 * Chart of the unified temperature scale.
	 */
	@FXML
	private LineChart<Number, Number> graph1;

	/**
	 * Title of the composite curve.
	 */
	@FXML
	private Label title2;

	/**
	 * Chart of the composite curve.
	 */
	@FXML
	private LineChart<Number, Number> graph2;

	/**
	 * Title of the unified temperature scale.
	 */
	@FXML
	private Label title3;

	/**
	 * Chart of the offset composite curve.
	 */
	@FXML
	private LineChart<Number, Number> graph3;

	/**
	 * Title of the large composite curve.
	 */
	@FXML
	private Label title4;

	/**
	 * Chart of the large composite curve.
	 */
	@FXML
	private LineChart<Number, Number> graph4;

	/**
	 * Button to export graphs in a csv file.
	 */
	@FXML
	private Button exportCsv;

	/**
	 * Extension use by application to save and load data.
	 */
	private static final String EXTENSION = "csv";

	/**
	 * Path to default folder where data are saved.
	 */
	private static final String SAVE_PATH = ".";

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		title1.setText(i.get("graph1", "title"));
		title2.setText(i.get("graph2", "title"));
		title3.setText(i.get("graph3", "title"));
		title4.setText(i.get("graph4", "title"));
		exportCsv.setText(i.get("graphs_info", "exportCsvButton"));
		exportCsv.setTooltip(new Tooltip(i.get("graphs_info", "exportCsvButtonTooltip")));

		// (Re)load graphs to update text of the legend
		if (graph1 == null || graph1.getData().isEmpty()
				|| graph2 == null || graph2.getData().isEmpty()
				|| graph3 == null || graph3.getData().isEmpty()
				|| graph4 == null || graph4.getData().isEmpty()) {
			loadGraphs();
		} else {
			reloadGraphTexts();
		}
	}

	/**
	 * Update labels
	 */
	private void reloadGraphTexts() {
		// Add data in all graphs.
		main.getData().updateGraph(graph1, 1);
		main.getData().updateGraph(graph2, 2);
		main.getData().updateGraph(graph3, 3);
		main.getData().updateGraph(graph4, 4);
	}

	/**
	 * Load or reload data of graphs, including text of the legend.
	 */
	private void loadGraphs() {
		// Remove data from all graphs.
		graph1.getData().clear();
		graph2.getData().clear();
		graph3.getData().clear();
		graph4.getData().clear();
		main.getData().setCurrentGraphView(0);

		// Add data in all graphs.
		main.getData().setGraph(graph1, 1);
		main.getData().setGraph(graph2, 2);
		main.getData().setGraph(graph3, 3);
		main.getData().setGraph(graph4, 4);

	}

	/**
	 * Export the heat cascade in a csv file.
	 */
	@FXML
	public void exportGraphCsv() {
		File f = main.getData().fileChooser("heatCascadeSave", true, "csvFile", "defaultheatCascadeFileName", EXTENSION,
				SAVE_PATH, main.getStage());
		if (f != null)
			main.getData().exportHeatCascadeCsv(f.getAbsolutePath());
	}

	/**
	 * Change current definition and load help view based on an Event.
	 *
	 * @param e Event trigger by the user by clicking on a button.
	 */
	@FXML
	public void goToDefinition(Event e) {
		String fxid = ((Control) e.getSource()).getId();
		main.getData().setCurrentDefinition(fxid);
		main.loadNewView(2);
	}

	/**
	 * Load the view of the unified temperature scale.
	 */
	public void showGraph1() {
		main.getData().setCurrentGraphView(1);
		main.loadNewView(4);
	}

	/**
	 * Load the view of the composite graph.
	 */
	public void showGraph2() {
		main.getData().setCurrentGraphView(2);
		main.loadNewView(4);
	}

	/**
	 * Load the view of the offset composite curve.
	 */
	public void showGraph3() {
		main.getData().setCurrentGraphView(3);
		main.loadNewView(4);
	}

	/**
	 * Load the view of the large composite curve.
	 */
	public void showGraph4() {
		main.getData().setCurrentGraphView(4);
		main.loadNewView(4);
	}

	/**
	 * Method called when the controller is create. Not used.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@Override
	protected void afterSetMainApp() {
		// Nothing to do.
	}

}
