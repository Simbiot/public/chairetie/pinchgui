package pinchgui.app_ui.view;

import static pinchgui.app_ui.util.TableControllerUtils.MAXIMAL_TEMPERATURE;

import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.converter.DefaultStringConverter;
import pinchgui.app_ui.exception.IncompleteTableException;
import pinchgui.app_ui.exception.SameStreamNameException;
import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.app_ui.model.EnergyProcessData;
import pinchgui.app_ui.util.TableControllerUtils;
import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;
import pinchgui.utils.StringToDoubleConverter;

public abstract class StreamTableController extends Controller {

	/**
	 * Title of table.
	 */
	@FXML
	private Label title;

	/**
	 * Text to inform the user.
	 */
	@FXML
	private Label infoText;

	/**
	 * Button to add row.
	 */
	@FXML
	private Button addRow;

	/**
	 * Button to add row.
	 */
	@FXML
	private Button deleteRow;

	/**
	 * Button to run algorithm.
	 */
	@FXML
	private Button run;

	/**
	 * Button to switch table.
	 */
	@FXML
	private Button switchButton;

	/**
	 * Button to import a csv file in the table of streams.
	 */
	@FXML
	private Button importCsv;

	/**
	 * Button to export the table of streams in a csv file.
	 */
	@FXML
	private Button exportCsv;

	/**
	 * Stream table.
	 */
	@FXML
	protected TableView<EnergyProcessData> tab;

	/**
	 * Stream column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessData, String> nameColumn;

	/**
	 * Te (input temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessData, Double> teColumn;

	/**
	 * Ts (output temperature) column of table.
	 */
	@FXML
	private TableColumn<EnergyProcessData, Double> tsColumn;

	/**
	 * Title of the error message.
	 */
	@FXML
	private Label exceptionTitle;

	/**
	 * Exception error message.
	 */
	@FXML
	private TextArea exceptionMessage;

	/**
	 * Label of DTmin.
	 */
	@FXML
	private Label dTminLabel;

	/**
	 * Label to display DTmin, the real value computed after algorithm run.
	 */
	@FXML
	private TextField dTminDisplay;

	/**
	 * Text field to impose a DTmin.
	 */
	@FXML
	private TextField dTminChoice;

	/**
	 * Default constructor of the class.
	 */
	public StreamTableController() {
	}

	/**
	 * Add new row to the table.
	 */
	@FXML
	abstract void addNewRow();

	/**
	 * Delete row selected from the table if there are more than one row. Else, show
	 * an alert.
	 */
	@FXML
	public void deleteRow() {
		I18n i = I18n.getInstance();
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(i.get("alert", "title"));

		if (tab.getSelectionModel().getSelectedItem() != null) {
			if (tab.getItems().size() > 1) {
				tab.getItems().remove(tab.getSelectionModel().getSelectedItem());
			} else {
				alert.setHeaderText(i.get("alert/table", "deleteRow"));
				alert.showAndWait();
			}
		} else {
			alert.setHeaderText(i.get("alert/table", "unselected"));
			alert.showAndWait();
		}
	}

	/**
	 * Change the current definition depending based on an Event, and load help
	 * view.
	 *
	 * @param e Event trigger by the user by clicking on a button.
	 */
	@FXML
	public void goToDefinition(Event e) {
		String fxid = ((Control) e.getSource()).getId();
		if (fxid.equals("title")) {
			main.getData().setCurrentDefinition("streamTable");
		}
		main.loadNewView(2);
	}

	/**
	 * Export the streams table in a csv file.
	 */
	@FXML
	public void exportCsvFile() {
		TableControllerUtils.exportCsvFile(main);
	}

	/**
	 * Import a csv file in the streams table.
	 */
	@FXML
	public void importCsvFile() {
		TableControllerUtils.importCsvFile(main);
	}

	/**
	 * Apply the choice on the algorithm and to {@link #dTminChoice}.
	 */
	@FXML
	public void applyDTminTextChoice() {
		final String txt = dTminChoice.getText();
		if (txt.matches("\\d+(\\.\\d*)?")) {
			double dtmin = Double.parseDouble(txt);
			if (dtmin >= 0.0 && dtmin <= MAXIMAL_TEMPERATURE) {
				// Valid value we propagate changes.
				dTminChoice.setText("" + dtmin);
				main.getData().setChosenDTmin(dtmin);
				return;
			}
		}
		// Wrong value we cancel DTminChoice.
		dTminChoice.setText("");
		main.getData().setChosenDTmin(null);
	}

	/**
	 * (Re)load the text of the container with the current language.
	 */
	@Override
	public void loadLanguage() {
		I18n i = I18n.getInstance();
		title.setText(i.get("table", "title"));
		infoText.setText("(" + i.get("table", "info") + ")");
		addRow.setText(i.get("table", "add_row") + " +");
		deleteRow.setText(i.get("table", "delete_row") + " -");
		run.setText(i.get("table", "run"));
		importCsv.setText(i.get("table", "import"));
		exportCsv.setText(i.get("table", "export"));

		nameColumn.setGraphic(getLabelWithTooltip(i.get("table", "stream"), i.get("table", "stream_tip")));
		teColumn.setGraphic(getLabelWithTooltip(i.get("table", "te"), i.get("table", "te_tip")));
		tsColumn.setGraphic(getLabelWithTooltip(i.get("table", "ts"), i.get("table", "ts_tip")));

		dTminChoice.setTooltip(new Tooltip(i.get("table", "dt_tip")));
		switchButton.setTooltip(new Tooltip(i.get("table", "switch_tip")));

		exceptionTitle.setText(i.get("alert", "messageTitle") + ":");
		String exceptionName = main.getData().getLastPinchException();
		if (exceptionName != null && !exceptionName.equals("")) {
			if (exceptionName.equals("noHotProcess") || exceptionName.equals("noColdProcess")) {
				exceptionMessage.setText(
						i.get("alert/exception", "notEnoughProcess") + " "
								+ i.get("alert/exception", exceptionName));
			} else {
				exceptionMessage.setText(i.get("alert/exception", exceptionName));
			}
		}
		exceptionMessage.setVisible(main.getData().isExceptionVisible());
		exceptionTitle.setVisible(main.getData().isExceptionVisible());

		if (main.getData().getChosenDTmin() != null) {
			dTminChoice.setText(main.getData().getChosenDTmin().toString());
		} else {
			dTminChoice.setPromptText(i.get("table", "dtminchoice"));
		}
		dTminDisplay.setPromptText(i.get("table", "dtminfound"));

		main.getData().refreshTypeColumn();
		tab.refresh();
	}

	/**
	 * Create and return a label with a tooltip.
	 *
	 * @param text Text of the label
	 * @param tip  Text of the tooltip
	 * @return A label
	 */
	protected Label getLabelWithTooltip(String text, String tip) {
		Label label = new Label(text);
		Tooltip t = new Tooltip(tip);
		final int toolTipShowDelay = 50;
		t.setShowDelay(new Duration(toolTipShowDelay));
		label.setTooltip(t);
		return label;
	}

	/**
	 *
	 */
	protected void setupNameColumn() {
		nameColumn.setEditable(true);
		nameColumn.setCellFactory(EditableCell.<EnergyProcessData, String>forTableColumn(new DefaultStringConverter()));
		nameColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessData, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<EnergyProcessData, String> p) {
						return p.getValue().getNameProperty();
					}
				});
		// updates the the value in the data.
		nameColumn.setOnEditCommit(event -> {
			final String value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setName(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTeColumn() {
		teColumn.setEditable(true);
		teColumn.setCellFactory(
				EditableCell.<EnergyProcessData, Double>forTableColumn(new StringToDoubleConverter()));
		teColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessData, Double> p) {
						return p.getValue().getInputTempProperty().asObject();
					}
				});
		// updates the the value in the data.
		teColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setInputTemp(value);
			tab.refresh();
		});
	}

	/**
	 *
	 */
	protected void setupTsColumn() {
		tsColumn.setEditable(true);
		tsColumn.setCellFactory(
				EditableCell.<EnergyProcessData, Double>forTableColumn(new StringToDoubleConverter()));
		tsColumn.setCellValueFactory(
				new Callback<CellDataFeatures<EnergyProcessData, Double>, ObservableValue<Double>>() {
					@Override
					public ObservableValue<Double> call(CellDataFeatures<EnergyProcessData, Double> p) {
						return p.getValue().getOutputTempProperty().asObject();
					}
				});
		// updates the the value in the data.
		tsColumn.setOnEditCommit(event -> {
			final Double value = event.getNewValue() != null ? event.getNewValue()
					: event.getOldValue();
			event.getTableView().getItems()
					.get(event.getTablePosition().getRow())
					.setOutputTemp(value);
			tab.refresh();
		});
	}

	/**
	 * Initialize columns of the table.
	 */
	protected void initColumns() {
		tab.getColumns().forEach(e -> e.setReorderable(false));
		setupNameColumn();
		setupTsColumn();
		setupTeColumn();
	}

	/**
	 * Run algorithm and show graphs.
	 */
	@FXML
	public void runAlgo() {
		boolean exception = true;
		try {
			main.getData().runAlgo();
			exception = false;

		} catch (IncompleteTableException | NotEnoughProcessException | NoPinchPointException | NoDTminFound
				| InconsistentDTmin | SameStreamNameException e) {
			// All exception are handled via their id.
			Alert problem = new Alert(AlertType.ERROR);
			problem.setTitle(I18n.getInstance().get("alert", "title"));
			problem.setHeaderText(I18n.getInstance().get("exception", e.getId()));
			problem.showAndWait();
			main.getData().setLastPinchException(e.getId());
		} catch (Exception e) {
			I18n i = I18n.getInstance();
			Alert problem = new Alert(AlertType.ERROR);
			problem.setTitle(i.get("alert", "title"));
			problem.setHeaderText(i.get("alert/exception", "defaultException"));
			problem.showAndWait();
			main.getData().setLastPinchException("defaultException");
		}

		String exceptionName = main.getData().getLastPinchException();
		if (exceptionName != null && !exceptionName.equals("")) {
			I18n i18n = I18n.getInstance();
			if (exceptionName.equals("noHotProcess") || exceptionName.equals("noColdProcess")) {
				exceptionMessage.setText(i18n.get("alert/exception", "notEnoughProcess") + " "
						+ i18n.get("alert/exception", exceptionName));
			} else {
				exceptionMessage.setText(i18n.get("alert/exception", exceptionName));
			}
		}
		exceptionMessage.setVisible(exception);
		exceptionTitle.setVisible(exception);
		main.getData().setExceptionVisible(exception);

		if (main.getData().getEnergyTargeting() != null) {
			// If the exception doesn't prevent the computation we print the results.
			main.getData().setCurrentGraphView(Math.max(0, main.getData().getCurrentGraphView()));
			dTminDisplay.setText("" + main.getData()
					.getEnergyTargeting()
					.getDeltaTmin());
			// Load view one graph or view 4 graphs
			if (main.getData().getCurrentGraphView() > 0
					&& main.getData().getCurrentGraphView() <= 4) {
				main.loadNewView(4);
			} else {
				main.loadNewView(3);
			}
		}
	}

	/**
	 * Switch from normal stream table view to enthalpy one. Clear streams.
	 */
	@FXML
	public void switchTableMode() {
		main.getData().getStreams().clear();
		main.getData().setEnthalpyMode(!main.getData().getEnthalpyMode());
		main.loadNewView(1);
	}

	@Override
	protected void afterSetMainApp() {
		tab.setItems(main.getData().getStreams());

		int i = main.getData().getCurrentGraphView();
		if (main.getData().getEnergyTargeting() != null
				&& main.getData().getEnergyTargeting().isInitialized()) {
			if (i > 0 && i <= 4) {
				main.loadNewView(4);
			} else {
				main.loadNewView(3);
			}
		}
	}

}
