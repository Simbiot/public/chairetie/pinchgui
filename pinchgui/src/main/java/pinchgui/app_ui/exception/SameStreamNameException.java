package pinchgui.app_ui.exception;

import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.exception.ExceptionId;

/**
 * Custom exception class.
 *
 * Exception thrown when the user gives the same name to two streams
 *
 */
@SuppressWarnings("serial")
public class SameStreamNameException extends Exception implements ExceptionId {

	/**
	 * ID to get the message of this exception.
	 */
	private static final String ID = "sameName";

	/**
	 *
	 * @return ID to get the message of this exception.
	 */
	@Override
	public String getId() {
		return ID;
	}

	/**
	 * Default constructor.
	 */
	public SameStreamNameException() {
		super(I18n.getInstance().get("alert/exception", ID));
	}

	/**
	 * Constructor where we can specify the dupplicated name.
	 *
	 * @param aName
	 */
	public SameStreamNameException(String aName) {
		super(I18n.getInstance().get("alert/exception", ID) + " " + aName);
	}
}
