package pinchgui.app_ui.exception;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import pinchgui.app_ui.internationalisation.I18n;

/**
 * Class that creates and shows an error alert.
 * 
 * This class must be instanciate when a value that should be
 * a number is not a number.
 * 
 */
public class NotANumberAlert {

	/**
	 * Constructor that create and show an alert.
	 * 
	 * @param wrongValue Wrong value that has triggered the alert.
	 */
	public NotANumberAlert(String wrongValue) {
		I18n i = I18n.getInstance();
	
		Alert problem = new Alert(AlertType.ERROR);
		problem.setTitle(i.get("alert", "title"));
		problem.setHeaderText(i.get("alert/table/cellValue", "value")+" \""+wrongValue+"\" "+i.get("alert/table/cellValue", "incorrect"));
		problem.setContentText(">> "+i.get("alert/table/cellValue", "message"));
		problem.showAndWait();
	}
}
