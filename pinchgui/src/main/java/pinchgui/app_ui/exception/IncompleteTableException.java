package pinchgui.app_ui.exception;

import pinchgui.app_ui.internationalisation.I18n;
import pinchgui.pinch.exception.ExceptionId;

/**
 * Custom exception class.
 *
 * Exception thrown when user try to run algorithm when the streams table is
 * incomplete.
 *
 */
@SuppressWarnings("serial")
public class IncompleteTableException extends Exception implements ExceptionId {

	/**
	 * ID to get the message of this exception.
	 */
	private static final String ID = "incomplete";

	@Override
	public String getId() {
		return ID;
	}

	/**
	 * Default constructor.
	 */
	public IncompleteTableException() {
		super(I18n.getInstance().get("alert/exception", ID));
	}

}
