package pinchgui.chart.test;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import pinchgui.chart.SimpleGraphXY;

/**
 * Informal test to check the graphs behavior.
 */
public final class InformalTest {

	/**
	 * Main method.
	 *
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		// Activate logging, doesn't prevent log test success
		Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		rootLogger.setLevel(Level.ERROR);

		twoGraphs();
	}

	/**
	 * Test two graphs display.
	 */
	public static void twoGraphs() {
		final int nbPts = 10;
		SimpleGraphXY graph = new SimpleGraphXY("Graph", "x", "y");
		graph.display();
		for (int i = 0; i < nbPts; i++) {
			graph.add("Linear", i, i);
			graph.add("Square", i, i * i);
		}
	}

	/**
	 * Prevent instantiation.
	 */
	private InformalTest() {

	}

}
