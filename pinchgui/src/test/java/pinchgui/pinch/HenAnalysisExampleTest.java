package pinchgui.pinch;

import java.util.HashSet;
import java.util.List;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;;

/**
 * Examples for using {@link EnergyTargetingAnalysis}.
 *
 */
public final class HenAnalysisExampleTest {

	/**
	 * Main.
	 *
	 * @param args Unused.
	 */
	public static void main(String[] args) {
		try {
			example1HenAbove();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void example1HenAbove()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example1";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 53., 67., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 53., 97., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 122., 142., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 133., 53., 22.);

		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.initialize();

		HenAnalysis hen = new HenAnalysis(pinch, new HashSet<EnergyUtility>());
		hen.initialize();
		List<HeatExchanger> exchangerList = hen.getHeatExchangerList();
		System.out.println();
		System.out.println("Pinch Canada Paper Factory Example.");
		System.out.println("A delta T min of " + pinch.getDeltaTmin()
				+ " degrees has been chosen, the temperatures indicated are the offset temperatures. "
				+ "The pinch temperature is " + pinch.getPinchPointTemperature());
		System.out.println("Result of the stream splitting algorithm with a tick off rule.");
		for (HeatExchanger exchanger : exchangerList) {
			System.out.println(exchanger);
		}

	}

	/**
	 * Prevent instantiation.
	 */
	private HenAnalysisExampleTest() {

	}

}
