package pinchgui.pinch;

import java.awt.Color;
import java.util.NavigableMap;
import java.util.Set;

import pinchgui.chart.SimpleGraphXY;

/**
 * Utility class with methods to print pinch curbs.
 *
 * @author tparis
 *
 */
public final class Utils {

	/**
	 *
	 * @param name     Used for the name of the graph.
	 * @param coldCurb Map of cold processes (temperature, enthalpy).
	 * @param hotCurb  Map of hot processes (temperature, enthalpy).
	 */
	public static void printCompositeCurbs(final String name, final NavigableMap<Double, Double> coldCurb,
			final NavigableMap<Double, Double> hotCurb) {
		final SimpleGraphXY offsetCompositeCurbsGraph = new SimpleGraphXY(name, "H kW",
				"T Celsius");
		for (double temperature : hotCurb.descendingKeySet()) {
			offsetCompositeCurbsGraph.add("Hot composite curb", hotCurb.get(temperature), temperature);
		}
		for (double temperature : coldCurb.descendingKeySet()) {
			offsetCompositeCurbsGraph.add("Cold composite curb", coldCurb.get(temperature), temperature);
		}
		offsetCompositeCurbsGraph.display();
	}

	/**
	 *
	 * @param name        Used for the name of the graph.
	 * @param heatCascade Map of the heat cascade (temperature, enthalpy).
	 */
	public static void printGreatCompositeCurb(String name, final NavigableMap<Double, Double> heatCascade) {
		final SimpleGraphXY greatCompositeGraph = new SimpleGraphXY(name, "H kW",
				"T Celsius");
		for (double temperature : heatCascade.keySet()) {
			greatCompositeGraph.add("Great composite curb", heatCascade.get(temperature), temperature);
		}
		greatCompositeGraph.display();
	}

	/**
	 * Print the curbs of a {@link EnergyTargetingAnalysis}.
	 *
	 * @param pinch
	 */
	public static void printCurbs(EnergyTargetingAnalysis pinch) {
		printCompositeCurbs(pinch.getName() + ": Offset Composite Curbs", pinch.getColdCompositeCurb(),
				pinch.getHotCompositeCurb());
		printCompositeCurbs(pinch.getName() + ": Composite Curbs", pinch.getOriginalColdCompositeCurb(),
				pinch.getOriginalHotCompositeCurb());
		printGreatCompositeCurb(pinch.getName() + ": Great Composite Curb", pinch.getHeatCascade());
	}

	/**
	 *
	 * @param name
	 * @param setProcesses
	 * @param pinchTemperature
	 * @param deltaTmin
	 */
	public static void printTemperatures(String name, Set<EnergyProcess> setProcesses, double pinchTemperature,
			double deltaTmin) {
		final SimpleGraphXY temperatureGraph = new SimpleGraphXY(name, "Process",
				"T Celsius");
		int n = 1;
		for (EnergyProcess p : setProcesses) {
			temperatureGraph.add(p.getName(), n, p.getInletT());
			temperatureGraph.add(p.getName(), n, p.getOutletT());
			final float newWidth = 4.0f;
			temperatureGraph.setWidth(p.getName(), newWidth);
			if (p.isCold()) {
				temperatureGraph.setColor(p.getName(), Color.BLUE);
			} else {
				temperatureGraph.setColor(p.getName(), Color.RED);
			}
			n++;
		}
		temperatureGraph.add("Pinch", n, pinchTemperature);
		temperatureGraph.add("Pinch", 0, pinchTemperature);
		temperatureGraph.setColor("Pinch", Color.BLACK);
		temperatureGraph.add("PinchHot", n, pinchTemperature + deltaTmin / 2.);
		temperatureGraph.add("PinchHot", 0, pinchTemperature + deltaTmin / 2.);
		temperatureGraph.setColor("PinchHot", Color.BLACK);
		temperatureGraph.add("PinchCold", 0, pinchTemperature - deltaTmin / 2.);
		temperatureGraph.add("PinchCold", n, pinchTemperature - deltaTmin / 2.);
		temperatureGraph.setColor("PinchCold", Color.BLACK);

		temperatureGraph.display();
	}

	/**
	 * Prevent instantiation.
	 */
	private Utils() {

	}

}
