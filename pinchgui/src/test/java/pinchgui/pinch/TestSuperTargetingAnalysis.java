package pinchgui.pinch;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Check values returned by {@link SuperTargetingAnalysis}.
 *
 * @author tparis
 *
 */
class TestSuperTargetingAnalysis {

	/**
	 * Example got from a lesson on surface estimation and computation.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	@Test
	void testEstimationArea() throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test");
		EnergyProcessHeatCapacity c1 = new EnergyProcessHeatCapacity("c1", "1", 1000., 20., 140., 0.2);
		EnergyProcessHeatCapacity c2 = new EnergyProcessHeatCapacity("c2", "2", 1000., 140., 180., 0.5);
		EnergyProcessHeatCapacity c3 = new EnergyProcessHeatCapacity("c3", "3", 1000., 180., 230., 0.3);
		EnergyProcessHeatCapacity h1 = new EnergyProcessHeatCapacity("h1", "4", 1000., 80., 40., 0.15);
		EnergyProcessHeatCapacity h2 = new EnergyProcessHeatCapacity("h2", "5", 1000., 200., 80., 0.4);
		EnergyProcessHeatCapacity h3 = new EnergyProcessHeatCapacity("h3", "6", 1000., 250., 200., 0.15);

		pinch.addProcess(c1);
		pinch.addProcess(c2);
		pinch.addProcess(c3);
		pinch.addProcess(h1);
		pinch.addProcess(h2);
		pinch.addProcess(h3);

		final double chosenDTmin = 10.;
		final double offsetEnergy = 10000.;
		pinch.setChosenDeltaTmin(chosenDTmin);
		pinch.addEnergyToSystem(offsetEnergy);
		pinch.initialize();

		Set<EnergyUtility> utilitySet = new HashSet<>();
		utilitySet.add(new EnergyUtility("ColdU Water", 10., 20., pinch.getMinimalCoolingEnergyRequested()));
		utilitySet.add(new EnergyUtility("HotU Furnace", 300., 250., pinch.getMinimalCoolingEnergyRequested()));

		final double epsilon = 1.;
		final double coeffConvection = 1.; // kW/m2/K
		final double refResults = 2716.3;
		final double estimation = SuperTargetingAnalysis.estimateTotalHeatExchangerSurface(pinch, utilitySet,
				coeffConvection);
		assertTrue(Math.abs(estimation - refResults) > epsilon,
				"Expected " + refResults + "m2 got " + estimation + "m2.");
	}

	/**
	 * Paper factory example which give 4 internal heat exchanger as estimation of
	 * the minimum. As we avoid loops, it can be seen as the number of processes
	 * less one.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	@Test
	void testEstimationNbInternalHE()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "TestNbInternalHE";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.setChosenDeltaTmin(14.);
		pinch.initialize();

		final int refNbInternalHE = 4;
		final int estimation = SuperTargetingAnalysis
				.estimateMinimalNumberOfInternalHeatExchangers(pinch.getProcessSet());
		assertEquals(refNbInternalHE, estimation);
	}

	/**
	 * Paper factory example, estimation of the number of internal and external heat
	 * exchangers with another formula (see "Introduction à l’intégration
	 * énergétique de procédés par l’Analyse Pinch", 2017, appendix 1, formula A2).
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	@Test
	void testEstimationNbHE()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "TestNbHE";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.setChosenDeltaTmin(14.);
		pinch.initialize();

		Set<EnergyUtility> utilitySet = new HashSet<>();
		utilitySet.add(new EnergyUtility("HU", 200, 199, pinch.getMinimalHeatingEnergyRequested()));
		utilitySet.add(new EnergyUtility("CU", 10, 11, pinch.getMinimalCoolingEnergyRequested()));

		final int refNbInternalHE = 8;
		final int estimation = SuperTargetingAnalysis.estimateMinimalNumberOfHeatExchangers(pinch);
		assertEquals(refNbInternalHE, estimation);
	}

}
