package pinchgui.pinch;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.NavigableMap;
import java.util.TreeMap;

import org.junit.jupiter.api.Test;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

class TestPinchAnalysis {

	@Test
	void testHeatCascadeColdHotCompositeCurbsExample1() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test1");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		final double refDeltaTmin = 10.;
		pinch.setChosenDeltaTmin(refDeltaTmin);
		try {
			pinch.initialize();
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		}

		final double refTpinch = 85.;
		final double minEnergyHot = 20.;
		final double minEnergyCold = 60.;

		assertEquals(refDeltaTmin, pinch.getDeltaTmin(), "Delta T minimal should be " + refDeltaTmin);
		assertEquals(refTpinch, pinch.getPinchPointTemperature(), "Pinch point temperature should be " + refTpinch);
		assertEquals(minEnergyHot, pinch.getMinimalHeatingEnergyRequested(),
				"Minimal energy requested to heat should be " + minEnergyHot);
		assertEquals(minEnergyCold, pinch.getMinimalCoolingEnergyRequested(),
				"Minimal energy requested to cool down should be " + minEnergyCold);

		final NavigableMap<Double, Double> refHeatCascade = new TreeMap<Double, Double>();
		refHeatCascade.put(165., minEnergyHot);
		refHeatCascade.put(145., 80.);
		refHeatCascade.put(140., 82.5);
		refHeatCascade.put(refTpinch, 0.);
		refHeatCascade.put(55., 75.);
		refHeatCascade.put(25., minEnergyCold);

		final NavigableMap<Double, Double> heatCascade = pinch.getHeatCascade();

		final NavigableMap<Double, Double> coldCurb = pinch.getColdCompositeCurb();
		final NavigableMap<Double, Double> hotCurb = pinch.getHotCompositeCurb();

		for (double temperature : refHeatCascade.keySet()) {
			assertEquals(refHeatCascade.get(temperature), heatCascade.get(temperature), "Wrong heat cascade!");
		}

		// Do check only if the two fluids are present.
		for (double temperature : refHeatCascade.navigableKeySet()) {
			if (hotCurb.get(temperature) != null && coldCurb.get(temperature) != null) {
				double result = Math.abs(hotCurb.get(temperature) - coldCurb.get(temperature));
				assertEquals(refHeatCascade.get(temperature), result,
						"Wrong hot and cold energy result, wrong heat cascade!");
			}
		}

	}

	@Test
	void testHeatCascadeColdHotCompositeCurbsExampleRef() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test1");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 150., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("P5", "5", 2.5, 35., 100., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);
		// Change in algo, new upper round lead to a guessed value of 11.0.
		final double refDeltaTmin = 10.;
		pinch.setChosenDeltaTmin(refDeltaTmin);
		try {
			pinch.initialize();
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
			fail("No exception should have been raised!");
		}

		final double refTpinch = 25.;
		final double minEnergyHot = 182.5;
		final double minEnergyCold = 0.;

		assertEquals(refDeltaTmin, pinch.getDeltaTmin(), "Delta T minimal should be " + refDeltaTmin);
		assertEquals(refTpinch, pinch.getPinchPointTemperature(), "Pinch point temperature should be " + refTpinch);
		assertEquals(minEnergyHot, pinch.getMinimalHeatingEnergyRequested(),
				"Minimal energy requested to heat should be " + minEnergyHot);
		assertEquals(minEnergyCold, pinch.getMinimalCoolingEnergyRequested(),
				"Minimal energy requested to cool down should be " + minEnergyCold);

		final NavigableMap<Double, Double> refHeatCascade = new TreeMap<Double, Double>();
		refHeatCascade.put(145., minEnergyHot);
		refHeatCascade.put(140., 185.);
		refHeatCascade.put(105., 132.5);
		refHeatCascade.put(85., 52.5);
		refHeatCascade.put(55., 52.5);
		refHeatCascade.put(40., 7.5);
		refHeatCascade.put(refTpinch, minEnergyCold);

		final NavigableMap<Double, Double> heatCascade = pinch.getHeatCascade();

		final NavigableMap<Double, Double> coldCurb = pinch.getColdCompositeCurb();
		final NavigableMap<Double, Double> hotCurb = pinch.getHotCompositeCurb();

		for (double temperature : refHeatCascade.keySet()) {
			assertEquals(refHeatCascade.get(temperature), heatCascade.get(temperature), "Wrong heat cascade!");
		}

		// Do check only if the two fluids are present.
		for (double temperature : refHeatCascade.navigableKeySet()) {
			if (hotCurb.get(temperature) != null && coldCurb.get(temperature) != null) {
				double result = Math.abs(hotCurb.get(temperature) - coldCurb.get(temperature));
				assertEquals(refHeatCascade.get(temperature), result,
						"Wrong hot and cold energy result, wrong heat cascade!");
			}
		}
	}

	@Test
	void testHeatCascadeColdHotCompositeCurbsExample2NoPinchPoint() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test1");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 200., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
			fail("No pinch point exception should have been raised!");
		}
		// No chosen DTmin, so the algorithm will try to find one, it should arrive on
		// 26.
		final double refDeltaTmin = 26.;
		assertEquals(refDeltaTmin, pinch.getDeltaTmin(), "Delta T minimal should be " + refDeltaTmin);
		assertNotNull(pinch.getPinchPointTemperature(), "Pinch point shouldn't be null as a DTmin can be found!");

	}

	@Test
	void testHeatCascadeColdHotCompositeCurbsExample3() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test1");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		try {
			pinch.initialize();
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		}

		final double refDeltaTmin = 20.;
		final double refPinchPointTemperature = 90.;

		assertEquals(refDeltaTmin, pinch.getDeltaTmin(), "Delta T minimal should be " + refDeltaTmin);
		assertEquals(refPinchPointTemperature, pinch.getPinchPointTemperature(),
				"Temperature at the pinch point should be " + refPinchPointTemperature);
	}

}
