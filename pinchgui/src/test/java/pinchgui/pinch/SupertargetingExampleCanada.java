package pinchgui.pinch;

import java.util.List;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 *
 *
 */
public class SupertargetingExampleCanada {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			exampleCanada();
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void exampleCanada()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "ExampleCanada";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.initialize();

		Utils.printCompositeCurbs(pinch.getName() + ": Composite Curbs",
				pinch.getOriginalColdCompositeCurb(),
				pinch.getOriginalHotCompositeCurb());

		SuperTargetingAnalysis supertargeting = new SuperTargetingAnalysis(pinch, 110000, 1, 1733, 6000);
		supertargeting.addUtility(new EnergyUtility("Cold", 0, 1, 0.01));
		supertargeting.addUtility(new EnergyUtility("Hot", 200, 199, 0.05));

		supertargeting.compute(8, 0.5, 45);
		List<SuperTargetingData> results = supertargeting.getSuperTargetingResults();
		for (SuperTargetingData data : results) {
			System.out.println(data.toString());
		}
	}

}
