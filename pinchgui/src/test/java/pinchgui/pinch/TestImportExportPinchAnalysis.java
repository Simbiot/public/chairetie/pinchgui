package pinchgui.pinch;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;
import pinchgui.utils.Utils;

public class TestImportExportPinchAnalysis {

	/**
	 * Folder for the tests.
	 */
	private static final String TEST_FOLDER_PATH = "src/test/resources/";

	@Test
	void testHeatCascadeExport() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test1");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.setChosenDeltaTmin(10.);
		try {
			pinch.initialize();
			String fileName = "heatcascade.csv";
			String refFileName = "heatcascade_ref.csv";
			pinch.exportHeatCascade(TEST_FOLDER_PATH + fileName);
			File f = new File(TEST_FOLDER_PATH + fileName);
			assertTrue(f.exists(), "The file should have been generated: " + TEST_FOLDER_PATH + fileName);
			assertTrue(Utils.compare(TEST_FOLDER_PATH + fileName, TEST_FOLDER_PATH + refFileName),
					"Generated files is not as expected!");
			return;
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin
				| FileNotFoundException e) {
			e.printStackTrace();
		}
		fail("No exception should occur!");
	}

	@Test
	void testFluidsExport() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test2");
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
			String fileName = "fluids.csv";
			String refFileName = "fluids_ref.csv";
			pinch.exportProcessesTab(TEST_FOLDER_PATH + fileName);
			File f = new File(TEST_FOLDER_PATH + fileName);
			assertTrue(f.exists(), "The file should have been generated: " + TEST_FOLDER_PATH + fileName);
			assertTrue(Utils.compare(TEST_FOLDER_PATH + fileName, TEST_FOLDER_PATH + refFileName),
					"Generated files is not as expected!");
			return;
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin
				| FileNotFoundException e) {
			e.printStackTrace();
		}
		fail("No exception should occur!");
	}

	@Test
	void testFluidsImport() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test3");
		Set<EnergyProcess> refSet = new HashSet<EnergyProcess>();
		refSet.add(new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.));
		refSet.add(new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.));
		refSet.add(new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.));
		refSet.add(new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.));

		try {
			String refFileName = "fluids_ref.csv";
			pinch.importFluidTab(TEST_FOLDER_PATH + refFileName);
			Set<EnergyProcess> set = pinch.getProcessSet();
			assertTrue(refSet.equals(set));

			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		fail("No exception should occur!");
	}

	@Test
	void testProcessEnthalpyExport() {
		String name = "Test4";
		final double massflow = 0.0257;
		final double t1 = 40.;
		final double t2 = 40.3;
		final double t3 = 95.;
		final double t4 = 63.4;
		final double h1 = 238.42;
		final double h2 = 238.78;
		final double h3 = 437.72;
		final double h4 = 420.57;
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcess p1 = new EnergyProcessEnthalpy("1->2", "1", massflow, t1, t2, h1, h2);
		EnergyProcess p2 = new EnergyProcessEnthalpy("2->3", "2", massflow, t2, t3, h2, h3);
		EnergyProcess p3 = new EnergyProcessEnthalpy("3->4", "3", massflow, t3, t4, h3, h4);
		EnergyProcess p4 = new EnergyProcessEnthalpy("4->1", "4", massflow, t4, t1, h4, h1);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
			String fileName = "fluids_enthalpy.csv";
			String refFileName = "fluids_enthalpy_ref.csv";
			pinch.exportProcessesTab(TEST_FOLDER_PATH + fileName);
			File f = new File(TEST_FOLDER_PATH + fileName);
			assertTrue(f.exists(), "The file should have been generated: " + TEST_FOLDER_PATH + fileName);
			assertTrue(Utils.compare(TEST_FOLDER_PATH + fileName, TEST_FOLDER_PATH + refFileName),
					"Generated files is not as expected!");
			return;
		} catch (NotEnoughProcessException | NoPinchPointException | NoDTminFound | InconsistentDTmin
				| FileNotFoundException e) {
			e.printStackTrace();
		}
		fail("No exception should occur!");
	}

	@Test
	void testEnthalpyImport() {
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis("Test5");
		Set<EnergyProcess> refSet = new HashSet<EnergyProcess>();
		final double massflow = 0.0257;
		final double t1 = 40.;
		final double t2 = 40.3;
		final double t3 = 95.;
		final double t4 = 63.4;
		final double h1 = 238.42;
		final double h2 = 238.78;
		final double h3 = 437.72;
		final double h4 = 420.57;
		refSet.add(new EnergyProcessEnthalpy("1->2", "1", massflow, t1, t2, h1, h2));
		refSet.add(new EnergyProcessEnthalpy("2->3", "2", massflow, t2, t3, h2, h3));
		refSet.add(new EnergyProcessEnthalpy("3->4", "3", massflow, t3, t4, h3, h4));
		refSet.add(new EnergyProcessEnthalpy("4->1", "4", massflow, t4, t1, h4, h1));

		try {
			String refFileName = "fluids_enthalpy_ref.csv";
			pinch.importFluidTab(TEST_FOLDER_PATH + refFileName);
			Set<EnergyProcess> set = pinch.getProcessSet();
			assertTrue(refSet.equals(set));

			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		fail("No exception should occur!");
	}

}
