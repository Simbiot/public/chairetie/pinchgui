package pinchgui.pinch;

import java.util.HashSet;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Examples for using {@link EnergyTargetingAnalysis}.
 *
 */
public final class HenAnalysisExampleCanada {

	/**
	 * Main.
	 *
	 * @param args Unused.
	 */
	public static void main(String[] args) {
		try {
			example1();
			example1Hen();
			example2Hen();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Example 1.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example1()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example1";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.setChosenDeltaTmin(14.);
		pinch.initialize();
		// Utils.printCompositeCurbs(pinch.getName() + ": Composite Curbs",
		// pinch.getOriginalColdCompositeCurb(),
		// pinch.getOriginalHotCompositeCurb());
		System.out.println(pinch.getName() + "\t" + pinch.getPinchPointTemperature() + "\t" + pinch.getDeltaTmin());
		System.out.println("Q_HU = " + pinch.getMinimalHeatingEnergyRequested() + "\tQ_CU = "
				+ pinch.getMinimalCoolingEnergyRequested());

		Set<EnergyUtility> utilitySet = new HashSet<>();
		utilitySet.add(new EnergyUtility("Hot", 200, 199, pinch.getMinimalHeatingEnergyRequested()));
		utilitySet.add(new EnergyUtility("Cold", 10, 15, pinch.getMinimalCoolingEnergyRequested()));
		HenAnalysis hen = new HenAnalysis(pinch, utilitySet);
		// Utils.printTemperatures("Temperatures", pinch.getSortedProcessSet(),
		// pinch.getPinchPointTemperature(), pinch.getDeltaTmin());
		// Utils.printTemperatures("Offset Temperatures",
		// pinch.getProcessSetOffsetTemperatures(),
		// pinch.getPinchPointTemperature(), pinch.getDeltaTmin());

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Split the processes.
		SplitProcessSets l = SplitProcessSets.splitProcesses(pinch);
		Set<EnergyProcess> above = l.getAbove();
		Set<EnergyProcess> below = l.getBelow();

		EnergyTargetingAnalysis pinchAbove = new EnergyTargetingAnalysis("Pinch Above");
		pinchAbove.addProcess(above);
		pinchAbove.initialize();

		Utils.printTemperatures("Offset Temperatures above",
				pinchAbove.getProcessSetOffsetTemperatures(),
				pinchAbove.getPinchPointTemperature(), pinchAbove.getDeltaTmin());

		List<EnergyFlow> lFlowEnteringPinch = HenAnalysis.getProcessesFlowingTowardFromPinch(above,
				pinchAbove.getPinchPointTemperature());
		List<EnergyFlow> lFlowLeavingPinch = HenAnalysis.getProcessesFlowingAwayFromPinch(above,
				pinchAbove.getPinchPointTemperature());

		HenAnalysis henAbove = new HenAnalysis(pinchAbove, utilitySet);
		henAbove.streamSplittingAlgorithm(lFlowEnteringPinch, lFlowLeavingPinch,
				pinchAbove.getPinchPointTemperature());
		System.out.println("Above");
		System.out.println(henAbove.getHeatExchangerList());

		EnergyTargetingAnalysis pinchBelow = new EnergyTargetingAnalysis("Pinch Below");
		pinchBelow.addProcess(below);
		pinchBelow.initialize();

		Utils.printTemperatures("Offset Temperatures below", pinchBelow.getProcessSetOffsetTemperatures(),
				pinchBelow.getPinchPointTemperature(), pinchBelow.getDeltaTmin());
		lFlowEnteringPinch = HenAnalysis.getProcessesFlowingTowardFromPinch(below,
				pinchBelow.getPinchPointTemperature());
		lFlowLeavingPinch = HenAnalysis.getProcessesFlowingAwayFromPinch(below, pinchBelow.getPinchPointTemperature());

		HenAnalysis henBelow = new HenAnalysis(pinchAbove, utilitySet);
		henBelow.streamSplittingAlgorithm(lFlowEnteringPinch, lFlowLeavingPinch,
				pinchBelow.getPinchPointTemperature());
		System.out.println("Below");
		System.out.println(henBelow.getHeatExchangerList());

	}

	public static void example1Hen()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example1";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.setChosenDeltaTmin(14.);
		pinch.initialize();

		Utils.printCompositeCurbs(pinch.getName() + ": Composite Curbs",
				pinch.getOriginalColdCompositeCurb(),
				pinch.getOriginalHotCompositeCurb());

		HenAnalysis hen = new HenAnalysis(pinch, new HashSet<EnergyUtility>());
		hen.initialize();
		List<HeatExchanger> exchangerList = hen.getHeatExchangerList();
		System.out.println();
		System.out.println("Pinch Canada Paper Factory Example.");
		System.out.println("A delta T min of " + pinch.getDeltaTmin()
				+ " degrees has been chosen, the temperatures indicated are the offset temperatures. "
				+ "The pinch temperature is " + pinch.getPinchPointTemperature());
		System.out.println("CU = " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("HU = " + pinch.getMinimalHeatingEnergyRequested());
		System.out.println("Estimated number of heat exchangers: "
				+ SuperTargetingAnalysis.estimateMinimalNumberOfHeatExchangers(pinch));
		final double coeffConvectioTransferWater = 1;
		System.out.println(
				"Estimated heat exchanger area: "
						+ SuperTargetingAnalysis.estimateTotalHeatExchangerSurface(pinch, new HashSet<EnergyUtility>(),
								coeffConvectioTransferWater)
						+ " m2.");

		System.out.println("Result of the stream splitting algorithm with a tick off rule.");
		for (HeatExchanger exchanger : exchangerList) {
			System.out.println(exchanger);
		}

		NavigableMap<Double, Double> mapHot = pinch.getEnthalpyFlowIntervalsLinkedToTemperaturesHot();
		NavigableMap<Double, Double> mapCold = pinch.getEnthalpyFlowIntervalsLinkedToTemperaturesCold();
		List<Double> enthalpyFlows = pinch.getEnthalpyFlowValues();
		for (double energy : enthalpyFlows) {
			System.out.println("Level " + energy + " coldT " + mapCold.getOrDefault(energy, -1.)
					+ " hotT " + mapHot.getOrDefault(energy, -1.));
		}
	}

	public static void example2Hen()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example2 DT 0";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("1", "1", 4.18, 60., 35., 72.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("2", "2", 4.18, 25., 60., 67.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("3", "3", 4.18, 25., 90., 20.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("4", "4", 4.18, 65., 85., 60.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("5", "5", 4.18, 140., 35., 22.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);

		pinch.initialize();
		Utils.printTemperatures("Temperatures", pinch.getSortedProcessSet(),
				pinch.getPinchPointTemperature(), pinch.getDeltaTmin());

		Set<EnergyUtility> utilitySet = new HashSet<>();
		utilitySet.add(new EnergyUtility("Hot", 200, 199, pinch.getMinimalHeatingEnergyRequested()));
		utilitySet.add(new EnergyUtility("Cold", 10, 15, pinch.getMinimalCoolingEnergyRequested()));

		HenAnalysis hen = new HenAnalysis(pinch, utilitySet);
		hen.setMinimalHeatExchangerPower(100.);
		hen.initialize();
		List<HeatExchanger> exchangerList = hen.getHeatExchangerList();
		System.out.println();
		System.out.println("Pinch Canada Paper Factory Example.");
		System.out.println("A delta T min of " + pinch.getDeltaTmin()
				+ " degrees has been chosen, the temperatures indicated are the offset temperatures. "
				+ "The pinch temperature is " + pinch.getPinchPointTemperature());
		System.out.println("Result of the stream splitting algorithm with a tick off rule.");
		for (HeatExchanger exchanger : exchangerList) {
			System.out.println(exchanger);
		}

	}

	/**
	 * Prevent instantiation.
	 */
	private HenAnalysisExampleCanada() {

	}

}
