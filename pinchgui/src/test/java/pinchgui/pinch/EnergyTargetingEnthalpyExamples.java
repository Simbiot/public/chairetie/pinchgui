package pinchgui.pinch;

import static pinchgui.pinch.Utils.printCurbs;

import java.io.FileNotFoundException;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;

/**
 * Example which do not consider a constant heat capacity.
 *
 * @author tparis
 *
 */
public final class EnergyTargetingEnthalpyExamples {

	/**
	 * Main.
	 *
	 * @param args Unused.
	 */
	public static void main(String[] args) {
		example1();
		example2();
		example3();
	}

	/**
	 * Example 1, SORC R-123.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example1() {
		String name = "SORC R-123";
		final double massflow = 0.0257;
		final double t1 = 40.;
		final double t2 = 40.3;
		final double t3 = 95.;
		final double t4 = 63.4;
		final double h1 = 238.42;
		final double h2 = 238.78;
		final double h3 = 437.72;
		final double h4 = 420.57;
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcess p1 = new EnergyProcessEnthalpy("1->2", "R-123", massflow, t1, t2, h1, h2);
		EnergyProcess p2 = new EnergyProcessEnthalpy("2->3", "R-123", massflow, t2, t3, h2, h3);
		EnergyProcess p3 = new EnergyProcessEnthalpy("3->4", "R-123", massflow, t3, t4, h3, h4);
		EnergyProcess p4 = new EnergyProcessEnthalpy("4->1", "R-123", massflow, t4, t1, h4, h1);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
		} catch (NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		} catch (NotEnoughProcessException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println("SORC R-123");
		System.out.println("DT = " + pinch.getDeltaTmin());
		System.out.println("Pinch point = " + pinch.getPinchPointTemperature());
		System.out.println("Minimal energy = " + pinch.getMinimalEnergy());
		System.out.println("Minimal energy for cooling= " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("Minimal energy for heating = " + pinch.getMinimalHeatingEnergyRequested());
		try {
			pinch.exportProcessesTab("SORC_R-123.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Example 2, SORC R-290.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example2() {
		String name = "SORC R-290";
		final double massflow = 0.0257;
		final double t1 = 40.;
		final double t2 = 42.44;
		final double t3 = 95.;
		final double t4 = 45.35;
		final double h1 = 305.71;
		final double h2 = 311.18;
		final double h3 = 647.38;
		final double h4 = 615.76;
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcess p1 = new EnergyProcessEnthalpy("1->2", "R-290", massflow, t1, t2, h1, h2);
		EnergyProcess p2 = new EnergyProcessEnthalpy("2->3", "R-290", massflow, t2, t3, h2, h3);
		EnergyProcess p3 = new EnergyProcessEnthalpy("3->4", "R-290", massflow, t3, t4, h3, h4);
		EnergyProcess p4 = new EnergyProcessEnthalpy("4->1", "R-290", massflow, t4, t1, h4, h1);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
		} catch (NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		} catch (NotEnoughProcessException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println("SORC R-290");
		System.out.println("DT = " + pinch.getDeltaTmin());
		System.out.println("Pinch point = " + pinch.getPinchPointTemperature());
		System.out.println("Minimal energy = " + pinch.getMinimalEnergy());
		System.out.println("Minimal energy for cooling= " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("Minimal energy for heating = " + pinch.getMinimalHeatingEnergyRequested());
		try {
			pinch.exportProcessesTab("SORC_R-290.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Example 1, SORC R-134a.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example3() {
		String name = "SORC R-134a";
		final double massflow = 0.0257;
		final double t1 = 40.;
		final double t2 = 40.1;
		final double t3 = 95.;
		final double t4 = 46.57;
		final double h1 = 256.13;
		final double h2 = 256.54;
		final double h3 = 437.82;
		final double h4 = 422.30;
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcess p1 = new EnergyProcessEnthalpy("1->2", "R-134a", massflow, t1, t2, h1, h2);
		EnergyProcess p2 = new EnergyProcessEnthalpy("2->3", "R-134a", massflow, t2, t3, h2, h3);
		EnergyProcess p3 = new EnergyProcessEnthalpy("3->4", "R-134a", massflow, t3, t4, h3, h4);
		EnergyProcess p4 = new EnergyProcessEnthalpy("4->1", "R-134a", massflow, t4, t1, h4, h1);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
		} catch (NoPinchPointException | NoDTminFound | InconsistentDTmin e) {
			e.printStackTrace();
		} catch (NotEnoughProcessException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println("SORC R-134a");
		System.out.println("DT = " + pinch.getDeltaTmin());
		System.out.println("Pinch point = " + pinch.getPinchPointTemperature());
		System.out.println("Minimal energy = " + pinch.getMinimalEnergy());
		System.out.println("Minimal energy for cooling= " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("Minimal energy for heating = " + pinch.getMinimalHeatingEnergyRequested());
		try {
			pinch.exportProcessesTab("SORC_R-134a.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Avoid instantiation.
	 */
	private EnergyTargetingEnthalpyExamples() {
	}
}
