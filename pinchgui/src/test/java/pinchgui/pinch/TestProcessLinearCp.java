package pinchgui.pinch;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class TestProcessLinearCp {

	/**
	 * Accuracy or equality tests.
	 */
	private static final double EPSILON = 1e-9;

	@Test
	void testProcessLinearCp1() {
		final double mflow = 0.0257;
		final double tin = 40.3;
		final double tout = 95.;
		final double hin = 238.78;
		final double hout = 238.78;
		EnergyProcessEnthalpy p = new EnergyProcessEnthalpy("Test1", "1", mflow, tin, tout, hin, hout);
		assertTrue(Math.abs(hin / tin - p.getCp(tin)) < EPSILON,
				"Expected " + (hin / tin) + " but got " + p.getCp(tin));
		assertTrue(Math.abs(hout / tout - p.getCp(tout)) < EPSILON,
				"Expected " + (hout / tout) + " but got " + p.getCp(tout));
	}

}
