package pinchgui.pinch;

import static pinchgui.pinch.Utils.printCurbs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

import pinchgui.pinch.exception.InconsistentDTmin;
import pinchgui.pinch.exception.NoDTminFound;
import pinchgui.pinch.exception.NoPinchPointException;
import pinchgui.pinch.exception.NotEnoughProcessException;;

/**
 * Examples for using {@link EnergyTargetingAnalysis}.
 *
 */
public final class EnergyTargetingExamples {

	/**
	 * Main.
	 *
	 * @param args Unused.
	 */
	public static void main(String[] args) {
		try {
			examplePowerPlant();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Example 1.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example1()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example1";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.initialize();
		printCurbs(pinch);
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());
	}

	/**
	 * Example 2. Non pinch point, the cold and hot composite curbs won't intersect.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 * @throws NoPinchPointException
	 */
	public static void example2NoPinch() throws NotEnoughProcessException, NoDTminFound, InconsistentDTmin {
		String name = "Example2";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 200., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 150., 30., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		try {
			pinch.initialize();
		} catch (NoPinchPointException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());

	}

	/**
	 * Example 3. Three fluids.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoPinchPointException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 */
	public static void example3()
			throws NotEnoughProcessException, NoPinchPointException, NoDTminFound, InconsistentDTmin {
		String name = "Example3";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 170., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 80., 140., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.initialize();
		printCurbs(pinch);
		for (Entry<Double[], List<EnergyProcess>> entry : pinch.getSortedIntervalsToProcess()) {
			System.out.println(entry.getKey()[1] + "-" + entry.getKey()[0] + ": " + entry.getValue());
		}
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());

	}

	/**
	 * Example 4. Five fluids, no pinch point.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 * @throws NoPinchPointException
	 */
	public static void example4() throws NotEnoughProcessException, NoDTminFound, InconsistentDTmin {
		String name = "Example4";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 200., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 200., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 180., 30., 1.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("P5", "5", 3.5, 35., 100., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);
		try {
			pinch.initialize();
		} catch (NoPinchPointException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());
		System.out.println("Minimal cooling energy: " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("Minimal heating energy: " + pinch.getMinimalHeatingEnergyRequested());
		System.out.println("Minimal energy: " + pinch.getMinimalEnergy());
	}

	/**
	 * Example 4. Five fluids, chosen DTmin to find a Pinch point.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 * @throws NoPinchPointException
	 */
	public static void example4bis() throws NotEnoughProcessException, NoDTminFound, InconsistentDTmin {
		String name = "Example4";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);
		EnergyProcessHeatCapacity p1 = new EnergyProcessHeatCapacity("P1", "1", 2., 20., 135., 1.);
		EnergyProcessHeatCapacity p2 = new EnergyProcessHeatCapacity("P2", "2", 3., 200., 60., 1.);
		EnergyProcessHeatCapacity p3 = new EnergyProcessHeatCapacity("P3", "3", 4., 200., 140., 1.);
		EnergyProcessHeatCapacity p4 = new EnergyProcessHeatCapacity("P4", "4", 1.5, 180., 30., 1.);
		EnergyProcessHeatCapacity p5 = new EnergyProcessHeatCapacity("P5", "5", 3.5, 35., 100., 1.);

		pinch.addProcess(p1);
		pinch.addProcess(p2);
		pinch.addProcess(p3);
		pinch.addProcess(p4);
		pinch.addProcess(p5);
		pinch.setChosenDeltaTmin(66.);
		try {
			pinch.initialize();
		} catch (NoPinchPointException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());
		System.out.println("Minimal cooling energy: " + pinch.getMinimalCoolingEnergyRequested());
		System.out.println("Minimal heating energy: " + pinch.getMinimalHeatingEnergyRequested());
		System.out.println("Minimal energy: " + pinch.getMinimalEnergy());
	}

	/**
	 * Example Beer.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 * @throws NoPinchPointException
	 */
	public static void exampleBeer() throws NotEnoughProcessException, NoDTminFound, InconsistentDTmin {
		String name = "ExampleBeer";
		EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);

		pinch.addProcess(new EnergyProcessHeatCapacity("Eau de brassage", "6", 4.48, 8., 58., 11.3));
		pinch.addProcess(new EnergyProcessHeatCapacity("Trempe 1", "7", 3.7, 53., 61., 27.2));
		pinch.addProcess(new EnergyProcessHeatCapacity("Trempe 2", "8", 3.7, 61., 74.5, 18.2));
		pinch.addProcess(new EnergyProcessHeatCapacity("Trempe 3", "9", 3.7, 74.5, 98., 13.6));
		pinch.addProcess(new EnergyProcessHeatCapacity("Empatage 1", "10", 3.7, 53., 62., 34.));
		pinch.addProcess(new EnergyProcessHeatCapacity("Empatage 2", "10", 3.7, 62., 88., 36.));

		pinch.addProcess(new EnergyProcessHeatCapacity("Eau de rincage", "1", 4.18, 8, 76, 6.2));
		pinch.addProcess(new EnergyProcessHeatCapacity("Rechauffeur de Mout", "1", 4.1, 8, 76, 6.2));
		pinch.addProcess(new EnergyProcessHeatCapacity("Cuve", "1", 2252, 100, 101, 1.));
		pinch.addProcess(new EnergyProcessHeatCapacity("Refroidisseur de Mout", "5", 11.9, 95., 7.5, 4.1));
		pinch.addProcess(new EnergyProcessHeatCapacity("Condenseur", "1", 2252, 100, 99, 1.));
		pinch.addProcess(new EnergyProcessHeatCapacity("Refroidisseur de buees condensees", "3", 1., 99., 20., 4.18));
		pinch.addProcess(new EnergyProcessHeatCapacity("Dreches", "4", 14., 74., 20., 1.));
		pinch.addProcess(new EnergyProcessHeatCapacity("Procede en exces de chaleur", "13", 4.18, 100., 20., 14.5));

		try {
			pinch.initialize();
		} catch (NoPinchPointException e) {
			e.printStackTrace();
		}
		printCurbs(pinch);
		try {
			pinch.exportProcessesTab("BeerFactoryOfenExampleExtendedPlusCoolingProcess.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin());
	}

	/**
	 * Example Beer.
	 *
	 * @throws NotEnoughProcessException
	 * @throws NoDTminFound
	 * @throws InconsistentDTmin
	 * @throws IOException
	 * @throws NoPinchPointException
	 */
	public static void examplePowerPlant()
			throws NotEnoughProcessException, IOException {
		for (double inc = 0.; inc < 1.; inc += 0.1) {
			String name = "ExamplePowerPlant";
			EnergyTargetingAnalysis pinch = new EnergyTargetingAnalysis(name);

			pinch.importFluidTab("PowerPlant.csv");

			final double baseDtmin = 23.;
			pinch.setTryLimit(100);
			pinch.setChosenDeltaTmin(baseDtmin + inc);
			try {
				pinch.initialize();
			} catch (NoPinchPointException e) {
				e.printStackTrace();
			} catch (NoDTminFound e) {
				e.printStackTrace();
			} catch (InconsistentDTmin e) {
				e.printStackTrace();
			}
//			printGreatCompositeCurb(name + " GCC", pinch.getHeatCascade());
			System.out.println(pinch.getName() + "\t" + pinch.getDeltaTmin() + "K");
			System.out.println(pinch.getName() + "\t MERh = " + pinch.getMinimalHeatingEnergyRequested());
			System.out.println(pinch.getName() + "\t MERc = " + pinch.getMinimalCoolingEnergyRequested());
			System.out.println(pinch.getAddedEnergyToSystem());
//			Utils.printCompositeCurbs(name, pinch.getColdCompositeCurb(), pinch.geHotCompositeCurb());
		}

	}

	/**
	 * Prevent instantiation.
	 */
	private EnergyTargetingExamples() {

	}

}
