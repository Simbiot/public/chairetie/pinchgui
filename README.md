## What is PinchGUI ?

PinchGUI (Pinch analysis Graphical User Interface) is a project of introduction software to present pinch analysis.

### What is pinch analysis ?

Pinch analysis is a process integration method which enables to optimise energy use in industrial processes. More information can be found on [this wikipedia page](https://en.wikipedia.org/wiki/Pinch_analysis).

PinchGUI focuses on the first part of the Pinch analysis:
* Description of processes (mass flows, thermal capacity, temperatures at inlet and outlet).
* Heat cascade algorithm which produces the composite curbs, visuals that give information on optimization possibilities.
* The supertargeting, a step further which consists in taking into account economic factor can also be partly done (for illustration purpose).

## How to compile, run and export PinchGUI ?

### From the code

The code was developed using [Eclipse IDE](https://www.eclipse.org/), [Maven](https://maven.apache.org/) and [JavaFX](https://openjfx.io/) 11 with [AdoptOpenJDK 11](https://adoptopenjdk.net/).

To run the application:
```batch
mvn compile javafx:run
```

To export the executable jar, run in the root folder of the project (the one with the *pom.xml*):
```batch
mvn package
```

The jar, named "pinchgui.jar", will be generated in the root folder.

### Known issue
JavaFX 11 works with [OpenJDK 11](https://openjdk.java.net/) (tested with the [AdoptOpenJDK 11](https://adoptopenjdk.net/) implementation), you will need this JDK to export the *jar* with Maven. You will also need a Java Runtime Environment 11 to use it.

## Explanation on branches

This Git repository contains at least two branches '*master*' and '*legacy-java-8*'.
* '*master*' is the main branch which work with Java 11, it works with [OpenJDK 11](https://openjdk.java.net/).
* '*legacy-java-8*' is a legacy branch which works with [Oracle JDK 8](https://www.oracle.com/fr/java/technologies/javase/javase-jdk8-downloads.html) (be careful with the license update), the Maven configuration is different. It is intended to be used with a Java Runtime Environment 8, a version that is still commonly installed on computers.

We currently maintain both branches to export the two versions of the software.

## Contributors

At one point or another, they were involved in the PinchGUI project:
* Benjamin RÉMY, scientific advisor, professor at ENSEM.
* Thomas PARIS, main developer, research engineer at ENSEM.
* Jérôme DUBOIS, developer of the GUI, intern at ENSEM.
* Brice LAMIL, scientific advisor and tester, end-of-studies project at ENSEM.
* Luigi AZZOPARDI, scientific advisor and tester, end-of-studies project at ENSEM.
